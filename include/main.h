#ifndef MAIN_H
#define MAIN_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

struct ALLEGRO_RESOURCES {
     ALLEGRO_TIMER *fps_timer;
     ALLEGRO_EVENT_QUEUE *event_queue;
     ALLEGRO_DISPLAY *display;
     ALLEGRO_FONT *font;
     ALLEGRO_EVENT_SOURCE event_source;
} ;

extern struct ALLEGRO_RESOURCES *container;

#endif
