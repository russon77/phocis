#ifndef SETTINGS_H
#define SETTINGS_H

#include <allegro5/allegro.h>

extern int SCREEN_W;
extern int SCREEN_H;
extern float FPS;
extern bool FULLSCREEN_TOGGLE;

enum _settings_override {
     WIN_HEIGHT, WIN_WIDTH, CONFIG_FILE, AI_DIFF, SKIP_INTRO, AUDIO_DISABLE
} ;

extern bool settings_override[6];

int settings_load(int, char **);

#endif
