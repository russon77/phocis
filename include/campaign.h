#ifndef CAMPAIGN_H
#define CAMPAIGN_H

#define MAX_CONNECTIONS (2)
#define MAX_LOOT_ITEMS (1)
#define NUM_LOCATIONS (5)
#define NO_LOCATION (-1)

enum _CAMPAIGN_LOCATIONS {
     STARTING_LOC, FIRST_BATTLE_LOC, SECOND_BATTLE_LOC, FIRST_BOSS_LOC, FINAL_BOSS_LOC
} ;

enum _CONNECTION_DIRECTIONS {
     LEFT, RIGHT
} ;

struct LOCATION {
     int id, x, y; /* x and y represent center of location image */
     int connected_location_ids[MAX_CONNECTIONS];

     char *name, *description;

     int loot_id[MAX_LOOT_ITEMS];
} ;

int start_campaign(const char *savefile);

#endif
