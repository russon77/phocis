#ifndef UI_BUILDER_H
#define UI_BUILDER_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

#define UI_NUM_INITIAL_MENUS (2)
#define UI_NUM_INITIAL_ELEMENTS (4)

enum ELEMENT_TYPES {
     EXTERNAL_FUNC, MENU_CHANGE, EDITABLE_TEXT, CONSTANT_TEXT
} ;

enum MENU_TYPES {
     LIST_VERTICAL_TIGHT, LIST_VERTICAL_LOOSE, LIST_HORIZONTAL, GRID, USER_DEFINED
} ;

enum MENU_TYPES_JUSTIFICATION {
     LIST_LEFTED, LIST_CENTERED, LIST_RIGHTED
} ;

/*
  weight is given low to high:
     low means 'bottom' and high
     means printed last, or on 'top'
*/

struct UI_ELEMENT_FUNC {
     void (*run)(void *args);
     void *args;
} ;

struct UI_ELEMENT {
     int id, parent_id;

     bool is_displayed;

     int x, y, width, height;
     int weight;
     int list_pos;

     char text[32];
     int text_x, text_y;
     char hint[64];
     
     bool is_hovered;
     int hover_x, hover_y;

     bool is_focused; /* for editable text fields only */

     int element_type;
     union {
	  int goto_menu_id;
	  struct UI_ELEMENT_FUNC action;
/*	  void (*func)(void *); */
	  int text_pos;
     } purpose;

     /* add in 'associated ids' for combined operations? */
     struct UI_ELEMENT *target_id;

     ALLEGRO_BITMAP *background;
     ALLEGRO_FONT *text_font, *hint_font;
     ALLEGRO_COLOR color;
} ;

struct UI_MENU {
     int id, parent_menu_id;
     int type, justification;

     bool is_displayed;
     
     int x, y, width, height;
     int weight;

     struct UI_ELEMENT *elements_array;
     int num_elements;

     int capacity_elements;
     int element_with_input_focus_id; /* only one element per menu can have input focus */

     ALLEGRO_BITMAP *background;
} ;

struct USER_INTERFACE {
     int id;

     int x, y, width, height;
     int weight;

     struct UI_MENU *menu_array;
     int num_menus;
     int capacity_menus;

     int current_menu_id;
} ;

int ui_init_all(ALLEGRO_DISPLAY *);

struct USER_INTERFACE *ui_create_interface(int x, int y, int width, int height,
					   int weight);

void ui_process_mouse_event(struct USER_INTERFACE *, ALLEGRO_EVENT *);

void ui_display_interface(struct USER_INTERFACE *);

int ui_attach_menu_to_interface(struct UI_MENU *, struct USER_INTERFACE *);

void ui_destroy_user_interface(struct USER_INTERFACE *);

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

#endif
