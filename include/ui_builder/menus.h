#ifndef UI_MENUS_H
#define UI_MENUS_H

#include <allegro5/allegro.h>

#include "ui_builder/ui_builder.h"

struct UI_MENU *ui_create_menu(int x, int y, int width, int height,
			       int weight, bool is_displayed,
			       ALLEGRO_BITMAP *background,
			       int back_button_location,
			       int type, int justification,
			       struct UI_MENU *parent);

void ui_display_menu(struct UI_MENU *);

void ui_destroy_menu(struct UI_MENU *);

bool ui_set_menu_visible(struct UI_MENU *menu_array, int menu_id);
bool ui_set_menu_invisible(struct UI_MENU *menu_array, int menu_id);

bool ui_align_menu(struct UI_MENU *);

#endif
