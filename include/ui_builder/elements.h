#ifndef UI_ELEMENTS_H
#define UI_ELEMENTS_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

#include "ui_builder/ui_builder.h"

int ui_create_element(int x_offset, int y_offset, int width, int height, int type,
		      int weight, bool is_displayed,
		      struct UI_ELEMENT_FUNC *action,
		      struct UI_MENU *goto_menu,
		      char *text, int text_x_offset, int text_y_offset,
		      char *hint, ALLEGRO_BITMAP *bg,
		      int flags,
		      ALLEGRO_FONT *text_font,
		      ALLEGRO_FONT *hint_font,
		      ALLEGRO_COLOR color,
		      struct UI_MENU *parent);

int ui_create_simple_display_element(int x_offset, int y_offset,
				     char *text, ALLEGRO_FONT *text_font,
				     char *hint, ALLEGRO_FONT *hint_font,
				     ALLEGRO_BITMAP *background,
				     ALLEGRO_COLOR color,
				     struct UI_MENU *parent);

int ui_create_editable_text_element(void);

int ui_create_action_element(int x_offset, int y_offset,
			     char *text, ALLEGRO_FONT *text_font,
			     char *hint, ALLEGRO_FONT *hint_font,
			     struct UI_ELEMENT_FUNC *action,
			     ALLEGRO_BITMAP *background,
			     ALLEGRO_COLOR color,
			     struct UI_MENU *parent);

int ui_create_menu_nav_element(int x_offset, int y_offset,
			       char *text, ALLEGRO_FONT *text_font,
			       char *hint, ALLEGRO_FONT *hint_font,
			       struct UI_MENU *goto_menu,
			       ALLEGRO_BITMAP *background,
			       ALLEGRO_COLOR color,
			       struct UI_MENU *parent);

int ui_create_back_button(int location, struct UI_MENU *parent);


void ui_display_element(struct UI_ELEMENT *);
void ui_destroy_element(struct UI_ELEMENT *);


#endif
