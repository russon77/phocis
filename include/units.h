#ifndef UNITS_H
#define UNITS_H

#include <allegro5/allegro.h>

enum DIRECTION {
     DIRECTION_LEFT, DIRECTION_RIGHT
} ;

enum OWNERSHIP {
     PLAYER_OWNED, AI_OWNED
} ;

enum SUB_SPRITE_DEFS {
     SUB_SPRITE_X, SUB_SPRITE_Y, SUB_SPRITE_W, SUB_SPRITE_H
} ;

struct UNIT_RESOURCES {
     /* bitmaps and spritesheet variables */
     int num_sprites_skeleton_walking;
     int num_sprites_skeleton_archer_attack;
     int num_sprites_skeleton_archer_attack_offset;
     int num_sprites_skeleton_soldier_attack;
     int num_sprites_skeleton_soldier_attack_offset;
     int num_sprites_skeleton_wizard_attack;
     int num_sprites_skeleton_wizard_attack_offset;
     int num_sprites_skeleton_total;
     
     int sprites_map_skeleton[35][5];

     ALLEGRO_BITMAP *spritesheet_skeleton;

     int num_sprites_human_walking;
     int num_sprites_human_archer_attack;
     int num_sprites_human_archer_attack_offset;
     int num_sprites_human_soldier_attack;
     int num_sprites_human_soldier_attack_offset;
     int num_sprites_human_wizard_attack;
     int num_sprites_human_wizard_attack_offset;
     int num_sprites_human_total;
     
     int sprites_map_human[35][5];

     ALLEGRO_BITMAP *spritesheet_human;

     /* TODO more units */

     ALLEGRO_BITMAP *unit_healthbar_empty;
     ALLEGRO_BITMAP *unit_healthbar_full;
} ;

enum UNIT_STATE {
     UNIT_STATE_WALKING, UNIT_STATE_ATTACKING, UNIT_STATE_HURT, UNIT_STATE_DEAD
} ;

struct UNIT {
     int unit_type;

     float xpos;
     float ypos;
     
     int direction;
     float movement_modifier;

     float frame_time;
     float unit_timer;
     int num_sprites;
     int spritepos; // which stage of sprite to display
     int spritepos_attack_frame;
     ALLEGRO_BITMAP *spritesheet;
     int (*sprites_mapping_array)[35][5];
     int num_sprites_walking;
     int num_sprites_attack;
     int num_sprites_attack_offset;
     int num_sprites_total;

     float health;
     float health_max;

     bool isplayerowned;
     bool isaggrod;
     bool is_on_attack_cooldown;

     struct UNIT *next;
} ;

struct UNIT_HEAD {
     struct UNIT *head;
} ;

extern struct UNIT_HEAD *uh;

int add_unit(int, int);

int unit_resources_load(void);

void units_draw(void);

void units_move(float);

bool units_enemy_in_range(struct UNIT *);

void units_garbage_collector(void);

void units_remove_all(void);

#endif
