#ifndef WAVES_H
#define WAVES_H

#define MAX_UNITS_PER_WAVE (100)

struct WAVE_MODIFIERS {
     float ai_summon_time_modifier;
     
     float archer_damage_mod;
     float archer_health_mod;
     float archer_defense_mod;

     float soldier_damage_mod;
     float soldier_health_mod;
     float soldier_defense_mod;

     float wizard_damage_mod;
     float wizard_health_mod;
     float wizard_defense_mod;
} ;

struct WAVE {
     int *units_array;
     int units_array_size;
     int units_array_pos;
     int units_array_capacity;
     struct WAVE_MODIFIERS wave_mods;
} ;

struct WAVE *waves_get_next_wave(int);
void waves_reset_count(void);

extern int wave_number;

enum WAVE_SOURCES {
     DEFAULT_WAVE_SOURCE, CUSTOM_FILE_WAVE_SOURCE,
     CAMPAIGN_WAVE_SOURCE
} ;
extern int wave_source;

#endif
