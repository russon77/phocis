#ifndef ENEMY_H
#define ENEMY_H

#define AI_DEFAULT_TIME_BETWEEN_SUMMONS (3.0f)
#define AI_LOWEST_TIME_BETWEEN_SUMMONS_MODIFIER (0.01f)

struct ENEMY_PLAYER {
     float health;
     float health_max;
     float mana;
     float mana_max;
     int direction;

     int bones_to_spend;
     int bones_total_collected;
     int num_units_summoned;

     bool spell_on_cooldown[3];
     float spell_cooldowns[3];

     int player_defined_keys[3];

     float time_since_last_summon;

     int wave;

     ALLEGRO_THREAD *thread;

     ALLEGRO_BITMAP *playersprite;
} ;

enum ENEMY_SOURCES {
     ENEMY_AI, ENEMY_LOCAL_MULT, ENEMY_REMOTE_MULT,
     ENEMY_CAMPAIGN_AI
} ;

extern bool force_new_wave;
extern bool wave_over;
extern bool game_over;
extern bool game_paused;
extern int ENEMY_SOURCE;


int enemy_load(int);
void enemy_summon_unit(int);

void enemy_set_spell_on_cooldown(int);

void enemy_lower_health(int );
void enemy_lower_mana(float);
void enemy_lower_summoned_count(void);
void enemy_lower_units_summoned_to_zero(void);
void enemy_lower_cooldowns(float);

void enemy_increase_units_summoned(void);
void enemy_increase_mana(float);

float enemy_get_current_health(void);
float enemy_get_max_health(void);
float enemy_get_current_mana(void);
float enemy_get_max_mana(void);
bool enemy_is_spell_on_cooldown(int);
float enemy_get_cooldown_remaining(int);
int enemy_get_bones_to_spend(void);
int enemy_get_units_summoned(void);
int enemy_get_max_units_summoned(void);

#include "waves.h"
bool ai_need_new_wave(void);
bool ai_get_new_wave(struct WAVE *);

#include "units.h"

int ai_move(void);

#endif
