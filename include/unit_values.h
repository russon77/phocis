#ifndef UNIT_VALUES_H
#define UNIT_VALUES_H

enum UNIT_TYPE {
     UNIT_SOLDIER, UNIT_WIZARD, UNIT_ARCHER
} ;

/**
   Values for units
**/

struct UNIT_VALUES {
     float DEFAULT_COOLDOWN;
     float USER_COOLDOWN_MOD;
/*     float AI_COOLDOWN_MOD; */

     float DEFAULT_MANA_COST;
     float USER_MANA_COST_MOD;
/*     float AI_MANA_COST_MOD; */

     float DEFAULT_HEALTH;
     float USER_HEALTH_MOD;
     float AI_HEALTH_MOD;

     float DEFAULT_DAMAGE;
     float USER_DAMAGE_MOD;
     float AI_DAMAGE_MOD;

     float DEFAULT_AGGRO_RANGE;
     float USER_AGGRO_RANGE_MOD;
     float AI_AGGRO_RANGE_MOD;

     float MOVEMENT_MODIFIER;

     float DEFAULT_DEFENSE;
     float USER_DEFENSE_MOD;
     float AI_DEFENSE_MOD;

} ;

extern struct UNIT_VALUES unit_values[3];

#include <allegro5/allegro.h>
void unit_values_load(ALLEGRO_CONFIG *);

#endif
