#ifndef AUDIO_H
#define AUDIO_H

void audio_load(void);

#include "units.h"

enum UNIT_SOUNDS {
     UNIT_ATTACK_SOUND, UNIT_HIT_SOUND, UNIT_DEATH_SOUND
} ;

void play_unit_sound(struct UNIT *unit, int sound_type);

enum BACKGROUND_MUSIC {
     NONE,
     BATTLE_BG, BATTLE_HALFWAY_GOOD_BG,
     BATTLE_HALFWAY_BAD_BG,
     BATTLE_VICTORY,
     BATTLE_DEFEAT,

     SHOP_BG,
     INVENTORY_BG,
     MAIN_MENU_BG, MAIN_MENU_WAITING_BG,
     CAMPAIGN_MENU_BG,
     LOADING_BG
} ;

#define NUM_BACKGROUND_MUSIC (10)

void start_background_music(int);
void stop_background_music(void);
int audio_get_current_background_music_id(void);

#endif
