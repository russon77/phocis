#ifndef GAME_H
#define GAME_H

enum EXIT_RESULTS {
     VICTORY, DEFEAT
} ;

enum keys {
     SPELL_ARCHER, SPELL_SOLDIER, SPELL_WIZARD
};

#include "main.h"

int play_game(int);

#endif
