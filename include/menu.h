#ifndef MENU_H
#define MENU_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

/* return value to be switch'd in main */
enum USER_SELECTION {
     NEW_CAMPAIGN, LOAD_CAMPAIGN, WAVES_MODE,
     MULTI_LOCAL, MULTI_REMOTE, SETTINGS, NO_SELECTION
} ;

#include "main.h"
int menu(struct ALLEGRO_RESOURCES *);

#endif
