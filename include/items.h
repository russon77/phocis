#ifndef ITEMS_H
#define ITEMS_H

#include <allegro5/allegro.h>

struct ITEM {
     int id;
     int cost;
     char name[32];
     char description[64];

     char shopkeeper_comment[128];
     
     ALLEGRO_BITMAP *image;
} ;

enum _ITEMS {
     ITEM_EMPTY
} ;

int items_init(void);

struct ITEM *items_get_from_id(int id);
int *items_get_id_list(void);
int items_get_num_items(void);

#endif
