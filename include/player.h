#ifndef PLAYER_H
#define PLAYER_H

#include <allegro5/allegro.h>

#include "campaign.h"

#define MAX_UNITS (20)
#define HEALTH_MAX (100.0f)
#define MANA_MAX (100.0f)

#define MAX_INVENTORY (10)
#define MAX_UPGRADES (3)

/*struct UPGRADE {
     int id;
     char name[32];
     char description[64];
     } ; */

enum _UPGRADES {
     UPGRADE_NONE  
} ;

struct PLAYER {
     float health;
     float health_max;
     float mana;
     float mana_max;
     int direction;

     int num_units_summoned;

     int bones_to_spend;
     int bones_total_collected;

     /* campaign related variables */
     int current_location_id;
     int visited_location_id_pairs[(NUM_LOCATIONS - 1) * 2];
     int inventory[MAX_INVENTORY]; /* hold item ids */
     int num_items;
     int upgrades[MAX_UPGRADES]; /* hold upgrade ids */
     int num_upgrades;

     /* spell related values */
     bool spell_on_cooldown[3];
     float spell_cooldowns[3];

     int player_defined_keys[3]; /* TODO */

     ALLEGRO_BITMAP *sprite; /* TODO */
} ;

#include "overlay.h"
#include "units.h"

int init_player(void);
void reset_player_for_battle(void);
void cast_spell(int);

/* getters for the local player structure */
float player_get_current_health(void);
float player_get_max_health(void);
float player_get_current_mana(void);
float player_get_max_mana(void);
int player_get_units_summoned(void);
int player_get_max_units_summoned(void);
bool player_is_spell_on_cooldown(int);
float player_get_cooldown_remaining(int);
int player_get_bones_to_spend(void);
int player_get_current_location(void);
ALLEGRO_BITMAP *player_get_sprite(void);
int *player_get_visited_location_ids(void);
int player_get_num_items(void);
int *player_get_held_items_id_list(void);

/* setter functions */
void player_set_spell_on_cooldown(int);
void player_set_current_location(int);

void player_add_item(int);

/* increase functions */
void player_increase_mana(float);
void player_increase_health(float);
void player_increase_units_summoned(void);
void player_increase_bones(void);

/* lower functions */
void player_lower_units_summoned(void);
void player_lower_units_summoned_to_zero(void);
void player_lower_health(int);
void player_lower_mana(float);
void player_lower_cooldowns(float);
void player_lower_cooldowns_to_zero(void);
void player_lower_bones(void);
void player_lower_bones_by_amount(int);


#endif
