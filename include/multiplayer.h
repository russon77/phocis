#ifndef MULTIPLAYER_H
#define MULTIPLAYER_H

int mult_set_fd(int);
int start_server(int);
int connect_to_host(char *host, int);

extern int remote_fd;

#endif
