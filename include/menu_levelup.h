#ifndef MENU_LEVELUP
#define MENU_LEVELUP

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

struct MENU_LEVELUP_RESOURCES {

     ALLEGRO_BITMAP *background_texture;

     /*
       Three borders:
       Top left corner
       Top Right corner
       Vertical 'pipe'

       from these basic pieces, desired UI can be created
     */

     // TODO remove top right corner from sprite and sprite map
     ALLEGRO_BITMAP *borders;
     int borders_spritemap[3][4];

     /*
       Six button mappings:
       2 for unpressed, not being hovered over
       2 for pressed
       2 for being hovered over
       2 for being at max level; button is disabled
     */
     ALLEGRO_BITMAP *buttons;
     int buttons_spritemap[2][4][4];
     int buttons_locations[6][4];

     /*
       Check marks:
       1 pair for not being hovered over
       1 pair for being hovered over
       1 pair for being disabled
     */
     ALLEGRO_BITMAP *checkmarks;
     int checkmark_spritemap[2][3][4];
     int checkmark_locations[3][4];

     /*
       These 3 images will represent the
       selection for modifying them

       It will make sense when you see
       the ui in game
     */
     ALLEGRO_BITMAP *archer_spell;
     ALLEGRO_BITMAP *soldier_spell;
     ALLEGRO_BITMAP *wizard_spell;
     int spell_locations[3][4];

     /*
       Continue button
     */
     ALLEGRO_BITMAP *button_continue;
     int button_continue_locations[4];

     /* TODO */
     /*
       Nine specials/abilities/features for units
       3 for archer
       3 for wizard
       3 for soldier
     */
     ALLEGRO_BITMAP *specials;
     int specials_spritemap[9][4];
     
} ;

enum BORDERS_MAP {
     TOP_LEFT_CORNER, TOP_RIGHT_CORNER, FLAT_HORIZONTAL
} ;

enum BUTTONS_MAP {
     BUTTON_DECREASE, BUTTON_INCREASE
} ;

enum BUTTONS_SPECIFIC_MAP {
     BUTTON_NORMAL, BUTTON_SELECTED, BUTTON_HOVERED, BUTTON_INVALID
} ;

enum CHECKMARK_MAP {
     UNCHECKED, CHECKED
} ;

enum CHECKMARK_SPECIFIC_MAP {
     CHECK_NORMAL, CHECK_HOVERED, CHECK_INVALID
} ;

#include "main.h"

int menu_levelup(struct ALLEGRO_RESOURCES*);

#endif
