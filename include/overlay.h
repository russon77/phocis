#ifndef OVERLAY_H
#define OVERLAY_H

#include <allegro5/allegro.h>

struct OVERLAY_RESOURCES {
     ALLEGRO_BITMAP *health_bar_empty;
     ALLEGRO_BITMAP *health_bar_full;
     
     ALLEGRO_BITMAP *mana_bar_empty;
     ALLEGRO_BITMAP *mana_bar_full;
     
     ALLEGRO_BITMAP *archer_spell;

     ALLEGRO_BITMAP *soldier_spell;

     ALLEGRO_BITMAP *wizard_spell;

     /* placeholder for future spells */

     ALLEGRO_BITMAP *background;

} ;

/* TODO */
struct OVERLAY_VALUES {
     float fps;
} ;

#include "player.h"
#include "units.h"
#include "main.h"

void overlay_display(struct ALLEGRO_RESOURCES *,
		     float );

int overlay_load(void);
void overlay_process_mouse_click(int, int);

#endif
