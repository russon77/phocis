#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <allegro5/allegro.h>

struct BACKGROUND {
     int grass_y;
     int grass_height;
     ALLEGRO_BITMAP *grass;

     int road_y;
     int road_height;
     ALLEGRO_BITMAP *road;

     int sky_height;
     ALLEGRO_BITMAP *sky;
} ;

void environment_display(void);
int init_background(ALLEGRO_DISPLAY *);

#endif
