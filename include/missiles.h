#ifndef MISSILES_H
#define MISSILES_H

/*
#define MISSILE_ARROW (1)
#define MISSILE_SPELL (2)
*/
enum MISSILE_TYPE {
     MISSILE_ARROW, MISSILE_SPELL
} ;

#define MISSILE_ARROW_DAMAGE (40.0f)
#define MISSILE_SPELL_DAMAGE (10.0f)

#define MISSILE_SPELL_TIME_TO_LIVE (4.0f)

/*#define GRAVITY (3.0f)*/

#define MISSILE_ARROW_DEFAULT_X_VELOCITY (400.0f)
#define MISSILE_SPELL_DEFAULT_X_VELOCITY (50.0f)

struct MISSILE_RESOURCES {
     ALLEGRO_BITMAP *arrow_spritesheet;
     int arrow_mappings[4];
     ALLEGRO_BITMAP *spellshot_spritesheet_skeleton;
     ALLEGRO_BITMAP *spellshot_spritesheet_human;
} ;

struct MISSILE {
     float xsrc;
     float ysrc;

     float time_since_launch;

     float xdest;
     float ydest;

     float xpos;
     float ypos;

     float xvelocity;
     float yvelocity;

     int type;

     int direction;
     int width;
     int height;

     bool to_be_removed;

     ALLEGRO_BITMAP *spritesheet;
     int (*arrow_mappings)[4];

     struct MISSILE *next;
} ;

struct MISSILE_HEAD {
     struct MISSILE *head;
} ;

int missile_resources_load(void);
void missiles_move(void);
void missiles_draw(void);
void missiles_garbage_collector(void);

int add_missile(int type, int direction,
                float xsrc, float ysrc,
                float xdest, float ydest);

void missiles_remove_all(void);

void missiles_process_collisions();

#endif
