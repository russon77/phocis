IDIR =./include
CC=gcc

CFLAGS=-I$(IDIR) -Wall -g -O0

ODIR=./obj
LDIR =./lib
SDIR=./src

LIBS= -lallegro -lallegro_font -lallegro_ttf -lallegro_image -lallegro_audio -lallegro_acodec -lallegro_primitives

_DEPS = player.h environment.h main_menu.h overlay.h player.h units.h main.h battle.h unit_behavior.h missiles.h enemy.h waves.h menu_levelup.h unit_values.h settings.h usage.h audio.h multiplayer.h ui_builder/elements.h ui_builder/menus.h ui_builder/ui_builder.h campaign.h error.h inventory.h items.h shop.h

DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = main.o main_menu.o overlay.o units.o battle.o environment.o player.o unit_behavior.o missiles.o enemy.o waves.o menu_levelup.o unit_values.o settings.o usage.o audio.o multiplayer.o ui_builder/elements.o ui_builder/menus.o ui_builder/ui_builder.o campaign.o error.o inventory.o items.o shop.o

OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


#$(ODIR)/%.o: %.c $(DEPS)
$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

phocis: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)
	cp phocis bin/

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 
