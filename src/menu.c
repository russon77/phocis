/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

/* FILE NOTES

   TODO : completely re-haul the way objects,
     in this case text objects, are stored so
     that hover status and click results
     can be dynamically found, instead of
     through the repetitive code we have now
*/

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

#include "menu.h"
#include "settings.h"
#include "audio.h"

static int get_selection_from_mouse(ALLEGRO_FONT *font, int xmouse, int ymouse);
static void set_hover_state(ALLEGRO_FONT *font, int xmouse, int ymouse);
static void menu_display_choices(ALLEGRO_FONT *);

enum MENU_STATE {
     MAIN_MENU, SINGLE_PLAYER, MULTIPLAYER, MULTIPLAYER_REMOTE,
     MENU_SETTINGS
} ;

enum SUB_MAIN_MENU_STATE {
     SUB_SINGLE_PLAYER, SUB_MULTIPLAYER, SUB_SETTINGS
} ;

enum SUB_SINGLE_PLAYER_STATE {
     SUB_NEW_CAMPAIGN, SUB_LOAD_CAMPAIGN, SUB_WAVES_MODE
} ;

enum SUB_MULTIPLAYER_MENU_STATE {
     SUB_LOCAL, SUB_REMOTE
} ;

enum SUB_MULTIPLAYER_REMOTE_MENU_STATE {
     SUB_HOST, SUB_JOIN
} ;

enum SUB_SETTINGS_MENU_STATE {
     SUB_DIMENSIONS, SUB_QUALITY
} ;

enum SUB_SETTINGS_SUB_QUALITY_MENU_STATE {
     SUB_HIGH_QUALITY, SUB_LOW_QUALITY
} ;

static bool hover_over_selection[5][4] = {{false}};
static int menu_state = MAIN_MENU;
static char remote_host[256] = {0};
static char portno[6] = {0};

static const char *menu_options[5][4] =
{
     {
	  "Single Player",
	  "Multiplayer",
	  
     }
} ;

/*
  Displays the initial menu with clickable selections of
   NEW GAME, MULTIPLAYER, SETTINGS
*/

int menu(struct ALLEGRO_RESOURCES *container)
{
     bool redraw = false;
     int result;

     start_background_music(MAIN_MENU_BG);

     while (1) {
          ALLEGRO_EVENT ev;
          al_wait_for_event(container->event_queue, &ev);

          if (ev.type == ALLEGRO_EVENT_TIMER)
               redraw = true;

          else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
               return -1;
          }

          else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
                  ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {

	       set_hover_state(container->font, ev.mouse.x, ev.mouse.y);
      
          }

          else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
               // return appropriate code
               // based on where mouse is

               result = get_selection_from_mouse(container->font, ev.mouse.x, ev.mouse.y);
               
               if (result != NO_SELECTION) {
                    return result;
               }
          }

          if (redraw && al_is_event_queue_empty(container->event_queue)) {
               // reset our redraw variable
               redraw = false;

               // clear the screen
               al_clear_to_color(al_map_rgb(0,0,0));

               // now draw our text and bitmaps

               // then draw menu choices
               menu_display_choices(container->font);

               al_flip_display();
          }

     } ;
     
     return -1;
}

static void menu_display_choices(ALLEGRO_FONT *font)
{
     if (menu_state == MAIN_MENU) {
	  if (hover_over_selection[menu_state][SUB_SINGLE_PLAYER]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H/4, ALLEGRO_ALIGN_CENTRE,
			    "Single Player");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H/4, ALLEGRO_ALIGN_CENTRE,
			    "Single Player");
	  }

	  if (hover_over_selection[menu_state][SUB_MULTIPLAYER]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*2/4, ALLEGRO_ALIGN_CENTRE,
			    "Multiplayer");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*2/4, ALLEGRO_ALIGN_CENTRE,
			    "Multiplayer");
	  }
	  if (hover_over_selection[menu_state][SUB_SETTINGS]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*3/4, ALLEGRO_ALIGN_CENTRE,
			    "Settings");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*3/4, ALLEGRO_ALIGN_CENTRE,
			    "Settings");
	  }
     }

     else if (menu_state == SINGLE_PLAYER) {

	  if (hover_over_selection[menu_state][SUB_NEW_CAMPAIGN]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H/4, ALLEGRO_ALIGN_CENTRE,
			    "New Campaign");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H/4, ALLEGRO_ALIGN_CENTRE,
			    "New Campaign"); 
	  }

	  if (hover_over_selection[menu_state][SUB_LOAD_CAMPAIGN]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*2/4, ALLEGRO_ALIGN_CENTRE,
			    "Load Campaign");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*2/4, ALLEGRO_ALIGN_CENTRE,
			    "Load Campaign");
	  }

	  if (hover_over_selection[menu_state][WAVES_MODE]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*3/4, ALLEGRO_ALIGN_CENTRE,
			    "Waves Mode");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*3/4, ALLEGRO_ALIGN_CENTRE,
			    "Waves Mode");
	  }
	  
     }

     else if (menu_state == MULTIPLAYER) {

	  if (hover_over_selection[menu_state][SUB_REMOTE]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*1/3, ALLEGRO_ALIGN_CENTRE,
			    "Remote Multiplayer");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*1/3, ALLEGRO_ALIGN_CENTRE,
			    "Remote Multiplayer"); 
	  }

	  if (hover_over_selection[menu_state][SUB_LOCAL]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*2/3, ALLEGRO_ALIGN_CENTRE,
			    "Local Multiplayer");
	  }

	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*2/3, ALLEGRO_ALIGN_CENTRE,
			    "Local  Multiplayer");
	  }
     }

     else if (menu_state == MULTIPLAYER_REMOTE) {

	  if (hover_over_selection[menu_state][SUB_REMOTE]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*1/3, ALLEGRO_ALIGN_CENTRE,
			    "Remote Multiplayer");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*1/3, ALLEGRO_ALIGN_CENTRE,
			    "Remote Multiplayer"); 
	  }

	  if (hover_over_selection[menu_state][SUB_LOCAL]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*2/3, ALLEGRO_ALIGN_CENTRE,
			    "Local Multiplayer");
	  }

	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*2/3, ALLEGRO_ALIGN_CENTRE,
			    "Local  Multiplayer");
	  }
     }

     else if (menu_state == MENU_SETTINGS) {

	  if (hover_over_selection[menu_state][SUB_DIMENSIONS]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*1/3, ALLEGRO_ALIGN_CENTRE,
			    "Screen Dimensions");
	  }
	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*1/3, ALLEGRO_ALIGN_CENTRE,
			    "Screen Dimensions"); 
	  }

	  if (hover_over_selection[menu_state][SUB_QUALITY]) {
	       al_draw_text(font, al_map_rgb(255,0,0),
			    SCREEN_W/2, SCREEN_H*2/3, ALLEGRO_ALIGN_CENTRE,
			    "Quality");
	  }

	  else {
	       al_draw_text(font, al_map_rgb(255,255,255),
			    SCREEN_W/2, SCREEN_H*2/3, ALLEGRO_ALIGN_CENTRE,
			    "Quality");
	  }
	  
     }

     return;
}

static void set_hover_state(ALLEGRO_FONT *font, int xmouse, int ymouse)
{
     int i, j, text_width;
     int text_height = 72;

     // reset the hover selection state
     for (i = 0; i < 5; i++) {
	  for (j = 0; j < 4; j++)
	       hover_over_selection[i][j] = false;
     }

     if (menu_state == MAIN_MENU) {
	  text_width = al_get_text_width(font, "Single Player");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/4 &&
	      ymouse < (SCREEN_H*1/4 + text_height) ) {

	       hover_over_selection[menu_state][SUB_SINGLE_PLAYER] = true;
	  }

	  text_width = al_get_text_width(font, "Multiplayer");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/4 &&
	      ymouse < (SCREEN_H*2/4 + text_height) ) {

	       hover_over_selection[menu_state][SUB_MULTIPLAYER] = true;
	  }

	  text_width = al_get_text_width(font, "Settings");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*3/4 &&
	      ymouse < (SCREEN_H*3/4 + text_height) ) {

	       hover_over_selection[menu_state][SUB_SETTINGS] = true;
	  }
     }

     else if (menu_state == SINGLE_PLAYER) {

	  text_width = al_get_text_width(font, "New Campaign");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/4 &&
	      ymouse < (SCREEN_H*1/4 + text_height) ) {

	       hover_over_selection[menu_state][SUB_NEW_CAMPAIGN] = true;
	  }

	  text_width = al_get_text_width(font, "Load Campaign");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/4 &&
	      ymouse < (SCREEN_H*2/4 + text_height) ) {

	       hover_over_selection[menu_state][SUB_LOAD_CAMPAIGN] = true;
	  }

	  text_width = al_get_text_width(font, "Waves Mode");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*3/4 &&
	      ymouse < (SCREEN_H*3/4 + text_height) ) {

	       hover_over_selection[menu_state][SUB_WAVES_MODE] = true;
	  }
     }

     else if (menu_state == MULTIPLAYER) {

	  text_width = al_get_text_width(font, "Remote Multiplayer");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/3 &&
	      ymouse < (SCREEN_H*1/3 + text_height) ) {

	       hover_over_selection[menu_state][SUB_REMOTE] = true;
	  }

	  text_width = al_get_text_width(font, "Local Multiplayer");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/3 &&
	      ymouse < (SCREEN_H*2/3 + text_height) ) {

	       hover_over_selection[menu_state][SUB_LOCAL] = true;
	  }
     }

     else if (menu_state == MENU_SETTINGS) {

	  text_width = al_get_text_width(font, "Screen Dimensions");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/3 &&
	      ymouse < (SCREEN_H*1/3 + text_height) ) {

	       hover_over_selection[menu_state][SUB_DIMENSIONS] = true;
	  }

	  text_width = al_get_text_width(font, "Quality");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/3 &&
	      ymouse < (SCREEN_H*2/3 + text_height) ) {

	       hover_over_selection[menu_state][SUB_QUALITY] = true;
	  }
     }

     return;
}

static int get_selection_from_mouse(ALLEGRO_FONT *font, int xmouse, int ymouse)
{
     int text_width;
     int text_height = 72;

     if (menu_state == MAIN_MENU) {
	  text_width = al_get_text_width(font, "Single Player");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/4 &&
	      ymouse < (SCREEN_H*1/4 + text_height) ) {

	       menu_state = SINGLE_PLAYER;

	       return NO_SELECTION;
	  }

	  text_width = al_get_text_width(font, "Multiplayer");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/4 &&
	      ymouse < (SCREEN_H*2/4 + text_height) ) {

	       menu_state = MULTIPLAYER;

	       return NO_SELECTION;
	  }

	  text_width = al_get_text_width(font, "Settings");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*3/4 &&
	      ymouse < (SCREEN_H*3/4 + text_height) ) {

	       menu_state = MENU_SETTINGS;

	       return NO_SELECTION;
	  }
     }

     else if (menu_state == SINGLE_PLAYER) {

	  text_width = al_get_text_width(font, "New Campaign");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/4 &&
	      ymouse < (SCREEN_H*1/4 + text_height) ) {

	       return NEW_CAMPAIGN;
	  }

	  text_width = al_get_text_width(font, "Load Campaign");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/4 &&
	      ymouse < (SCREEN_H*2/4 + text_height) ) {

	       /* TODO FIXME */
	       
	       menu_state = MAIN_MENU;

	       return NO_SELECTION;
	  }

	  text_width = al_get_text_width(font, "Waves Mode");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*3/4 &&
	      ymouse < (SCREEN_H*3/4 + text_height) ) {

	       return WAVES_MODE;
	  }	  
     }

     else if (menu_state == MULTIPLAYER) {

	  text_width = al_get_text_width(font, "Remote Multiplayer");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/3 &&
	      ymouse < (SCREEN_H*1/3 + text_height) ) {

	       /* TODO FIXME */

	       menu_state = MAIN_MENU;

	       return NO_SELECTION;
	  }

	  text_width = al_get_text_width(font, "Local Multiplayer");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/3 &&
	      ymouse < (SCREEN_H*2/3 + text_height) ) {

	       return MULTI_LOCAL;
	  }
     }

     else if (menu_state == MENU_SETTINGS) {

	  text_width = al_get_text_width(font, "Screen Dimensions");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*1/3 &&
	      ymouse < (SCREEN_H*1/3 + text_height) ) {

	       /* TODO FIXME */

	       menu_state = MAIN_MENU;

	       return NO_SELECTION;
	  }

	  text_width = al_get_text_width(font, "Quality");
	  if (xmouse > (SCREEN_W/2 - text_width/2) &&
	      xmouse < (SCREEN_W/2 + text_width/2) &&
	      ymouse > SCREEN_H*2/3 &&
	      ymouse < (SCREEN_H*2/3 + text_height) ) {

	       /* TODO FIXME */
	       menu_state = MAIN_MENU;

	       return NO_SELECTION;
	  }	  
     }
     
     return NO_SELECTION;
}
