#include <stdio.h>
#include <stdlib.h>

#include <allegro5/allegro_font.h>

#include "main_menu.h"

#include "ui_builder/ui_builder.h"
#include "ui_builder/menus.h"
#include "ui_builder/elements.h"

#include "main.h"
#include "battle.h"
#include "settings.h"
#include "audio.h"
#include "enemy.h"
#include "campaign.h"
#include "player.h"

static struct USER_INTERFACE *main_menu_ui = NULL;

static int main_menu_init(void);

int main_menu(void)
{
     bool redraw = false;
     
     if (main_menu_init()) {
	  fprintf(stderr, "fatal: failed to load main menu\n");
	  return 1;
     }

     while (1) {
	  ALLEGRO_EVENT ev;
	  al_wait_for_event(container->event_queue, &ev);

	  switch (ev.type) {
	  case ALLEGRO_EVENT_TIMER:

	       redraw = true;

	       break;
	  case ALLEGRO_EVENT_DISPLAY_CLOSE:

	       ui_destroy_user_interface(main_menu_ui);

	       return 0;

	  case ALLEGRO_EVENT_MOUSE_AXES:
	  case ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY:
	  case ALLEGRO_EVENT_MOUSE_BUTTON_UP:

	       ui_process_mouse_event(main_menu_ui, &ev);

	       break;
	  }

	  if (redraw && al_is_event_queue_empty(
		   container->event_queue)) {

	       /* check and enforce proper background music */
	       if (audio_get_current_background_music_id() !=
		    MAIN_MENU_BG)
		    start_background_music(MAIN_MENU_BG);

	       redraw = false;

	       al_clear_to_color(al_map_rgb(0,0,0));

	       // draw user interface
	       ui_display_interface(main_menu_ui);

	       al_flip_display();
	       
	  }
     } ;
     
     
     return 0;
}

static void helper_start_waves_mode(void *args)
{
     init_player();
     play_game(ENEMY_AI);
}

static void helper_start_local_mult(void *args)
{
     init_player();
     play_game(ENEMY_LOCAL_MULT);
}

static void helper_exit_game(void *args)
{
     ALLEGRO_EVENT ev;
     ev.type = ALLEGRO_EVENT_DISPLAY_CLOSE;
     al_emit_user_event(&(container->event_source),
			&ev, NULL);
}

static void helper_start_new_campaign(void *args)
{
     start_campaign(NULL);
}

static void helper_audio_toggle(void *args)
{
     if (settings_override[AUDIO_DISABLE]) {

	  settings_override[AUDIO_DISABLE] = false;

	  start_background_music(MAIN_MENU_BG);
     }

     else {
	  stop_background_music();

	  settings_override[AUDIO_DISABLE] = true;

     }

     return;
}

static void helper_open_gnu_website(void *args)
{
#ifdef __linux__
     system("xdg-open http://www.gnu.org/licenses/gpl.html");
#endif

#ifdef _WIN32
     system("start http://www.gnu.org/licenses/gpl.html");
#endif

     return;
}

/* TODO FIXME
 need to recreate*/
static void helper_full_screen_toggle(void *args)
{
     static int old_width = 0, old_height = 0;

     /* catch the first run through */
     if (!old_width && !old_height) {
	  old_width = SCREEN_W; old_height = SCREEN_H;
     }

     al_destroy_display(container->display);

     if (old_height == SCREEN_H) {
	  /* not full screen, so turn full screen ON */
	  ALLEGRO_DISPLAY_MODE disp_data;

	  al_get_display_mode(0, &disp_data);

	  al_set_new_display_flags(ALLEGRO_FULLSCREEN);

	  container->display = al_create_display(disp_data.width, disp_data.height);

	  SCREEN_W = disp_data.width;
	  SCREEN_H = disp_data.height;
     }

     else {
	  /* currently full screen, so revert to windowed */
	  al_set_new_display_flags(ALLEGRO_WINDOWED);
	  container->display = al_create_display(old_width, old_height);
	  SCREEN_H = old_height; SCREEN_W = old_width;
     }

     if (!container->display) {
	  fprintf(stderr, "failed to create display!\n");
	  exit(1);
     }

     if (main_menu_init()) {
	  fprintf(stderr, "failed to init menu after dimensions change\n");
	  exit(1);
     }

     return;
}

/*
  main init function for the main menu
    all ui stuff is created/instantiated here
*/
int main_menu_init(void)
{
     ALLEGRO_FONT *text_font, *hint_font;
     /* if user interface structure already exists,
	first destroy old interface before loading new one */
     if (main_menu_ui) {
	  ui_destroy_user_interface(main_menu_ui);
	  main_menu_ui = NULL;
     }

     /* load fonts */
     text_font = al_load_font("./data/fonts/ENDOR.ttf", 24, 0);
     hint_font = al_load_font("./data/fonts/ENDOR.ttf", 18, 0);

     if (!text_font || !hint_font) {
	  fprintf(stderr, "failed to load data/fonts/ENDOR.ttf\n");
	  return 1;
     }

     /* create and initialize menus */
     main_menu_ui = ui_create_interface(0,0,SCREEN_W,SCREEN_H,1);
     if (!main_menu_ui) {
	  fprintf(stderr, "failed to create main menu interface structure\n");
	  return 1;
     }

     struct UI_MENU *menu_main = ui_create_menu(0,0,SCREEN_W,SCREEN_H,1,true,NULL,0,LIST_VERTICAL_LOOSE,
						LIST_CENTERED,NULL);
     if (!menu_main) {
	  fprintf(stderr, "failed to create menu_main menu structure\n");
	  return 1;
     }

     struct UI_MENU *menu_single_player = ui_create_menu(0,0,SCREEN_W,SCREEN_H,1,false,NULL,0,
							 LIST_VERTICAL_LOOSE,
							 LIST_CENTERED,NULL);
     if (!menu_single_player) {
	  fprintf(stderr, "failed to create menu_single_player menu structure\n");
	  return 1;
     }

     struct UI_MENU *menu_load_campaign = ui_create_menu(0,0,SCREEN_W,SCREEN_H,1,false,NULL,0,
							 LIST_VERTICAL_LOOSE,
							 LIST_CENTERED,NULL);
     if (!menu_load_campaign) {
	  fprintf(stderr, "failed to create menu_load_campaign menu structure\n");
	  return 1;
     }


     struct UI_MENU *menu_multi_player = ui_create_menu(0,0,SCREEN_W,SCREEN_H,1,false,NULL,0,
							 LIST_VERTICAL_LOOSE,
							 LIST_CENTERED,NULL);
     if (!menu_multi_player) {
	  fprintf(stderr, "failed to create menu_multi_player menu structure\n");
	  return 1;
     }

     struct UI_MENU *menu_multi_player_remote = ui_create_menu(0,0,SCREEN_W,SCREEN_H,1,false,NULL,0,
							 LIST_VERTICAL_LOOSE,
							 LIST_CENTERED,NULL);
     if (!menu_multi_player_remote) {
	  fprintf(stderr, "failed to create menu_multi_player_remote menu structure\n");
	  return 1;
     }     

     struct UI_MENU *menu_settings = ui_create_menu(0,0,SCREEN_W,SCREEN_H,1,false,NULL,0,
							LIST_VERTICAL_LOOSE,
							LIST_CENTERED,NULL);
     if (!menu_settings) {
	  fprintf(stderr, "failed to create menu_settings menu structure\n");
	  return 1;
     }

     struct UI_MENU *menu_settings_about = ui_create_menu(0,0,SCREEN_W,SCREEN_H,1,false,NULL,0,
							  LIST_VERTICAL_LOOSE,
							  LIST_CENTERED,NULL);
     if (!menu_settings_about) {
	  fprintf(stderr, "failed to create menu_settings_about menu structure\n");
	  return 1;
     }

     /* create and attach elements */

     /* main menu elements */
     ui_create_menu_nav_element(0,0, "Single Player", text_font,
				"Start or load a single player game", hint_font,
				menu_single_player, NULL, al_map_rgb(255,0,0),
				menu_main);

     ui_create_menu_nav_element(0,0, "Multiplayer", text_font,
				"Host or join a multiplayer game", hint_font,
				menu_multi_player, NULL, al_map_rgb(255,0,0),
				menu_main);

     ui_create_menu_nav_element(0,0, "Settings", text_font,
				"Configure the game", hint_font,
				menu_settings, NULL, al_map_rgb(255,0,0),
				menu_main);

     struct UI_ELEMENT_FUNC func = {helper_exit_game, NULL};
     ui_create_action_element(0,0,"Exit", text_font,
			      "Return to desktop", hint_font,
			      &func, NULL, al_map_rgb(255,0,0),
			      menu_main);

     /* single player menu elements */
     func.run = helper_start_new_campaign; func.args = NULL;
     ui_create_action_element(0,0, "New Campaign", text_font,
			      "Start a new adventure!", hint_font,
			      &func,
			      NULL, al_map_rgb(255,0,0),
			      menu_single_player);

     ui_create_menu_nav_element(0,0, "Load Campaign", text_font,
				"Continue a previously saved game", hint_font,
				menu_load_campaign, NULL, al_map_rgb(255,0,0),
				menu_single_player);

     func.run = helper_start_waves_mode; func.args = NULL;
     ui_create_action_element(0,0, "Waves Mode", text_font,
			      "Battle against endless waves!", hint_font,
			      &func,
			      NULL, al_map_rgb(255,0,0),
			      menu_single_player);

     /* multiplayer menu elements */

     ui_create_menu_nav_element(0,0,"Remote Multiplayer",text_font,
			      "Host or join a remote game",hint_font,
			      menu_multi_player_remote, NULL, al_map_rgb(255,0,0),
			      menu_multi_player);

     func.run = helper_start_local_mult; func.args = NULL;
     ui_create_action_element(0,0,"Local Multiplayer",text_font,
			      "Start a local multiplayer game",hint_font,
			      &func,NULL,al_map_rgb(255,0,0),
			      menu_multi_player);

     /* TODO */

     /* load campaign menu elements */

     /* TODO dynamically generate a list of 5 campaign save slots */

     /* settings menu elements */
     func.run = helper_audio_toggle; func.args = NULL;
     ui_create_action_element(0,0, "Audio Toggle", text_font,
			      "Apply directly to the forehead", hint_font,
			      &func, NULL,
			      al_map_rgb(255,0,0),
			      menu_settings);

     func.run = helper_full_screen_toggle;
     ui_create_action_element(0,0, "Full Screen Toggle", text_font,
			      "How do I shot lazer", hint_font,
			      &func, NULL,
			      al_map_rgb(255,0,0),
			      menu_settings);

     ui_create_menu_nav_element(0,0,"About", text_font,
				"Information about the game", hint_font,
				menu_settings_about, NULL, al_map_rgb(255,0,0),
				menu_settings);

     /* about menu elements */

     ui_create_simple_display_element(0,0,"Developer", text_font,
				      "Aka the coolest guy in the world", hint_font,
				      NULL, al_map_rgb(255,0,0),
				      menu_settings_about);

     ui_create_simple_display_element(0,0,"Tristan Kernan", text_font,
				      "Ladies, I am single", hint_font,
				      NULL, al_map_rgb(0,0,255),
				      menu_settings_about);

     ui_create_simple_display_element(0,0,"Concept Arist", text_font,
				      "Aka the man with the plan", hint_font,
				      NULL, al_map_rgb(255,0,0),
				      menu_settings_about);

     ui_create_simple_display_element(0,0,"Noah Mamet", text_font,
				      "Ladies, look no further", hint_font,
				      NULL, al_map_rgb(0,0,255),
				      menu_settings_about);

     ui_create_simple_display_element(0,0,"Composer", text_font,
				      "Aka the next Mozart or Eminem", hint_font,
				      NULL, al_map_rgb(255,0,0),
				      menu_settings_about);

     ui_create_simple_display_element(0,0,"Noah Mamet", text_font,
				      "Ladies, I have nothing else to say", hint_font,
				      NULL, al_map_rgb(0,0,255),
				      menu_settings_about);

     func.run = helper_open_gnu_website; func.args = NULL;
     ui_create_action_element(0,0,"Licensed GPLv3+", text_font,
			      "Visit gnu.org", hint_font,
			      &func,
			      NULL, al_map_rgb(0,255,0),
			      menu_settings_about);


     /* align the menus */
     ui_align_menu(menu_main);
     ui_align_menu(menu_single_player);
     ui_align_menu(menu_multi_player);
     ui_align_menu(menu_load_campaign);
     ui_align_menu(menu_settings);
     ui_align_menu(menu_settings_about);


     /* create back buttons */
     ui_create_menu_nav_element(10,10, "Back", text_font,
				NULL, NULL, menu_main, NULL, al_map_rgb(255,0,0),
				menu_single_player);

     ui_create_menu_nav_element(10,10, "Back", text_font,
				NULL, NULL, menu_main, NULL, al_map_rgb(255,0,0),
				menu_settings);

     ui_create_menu_nav_element(10,10, "Back", text_font,
				NULL, NULL, menu_main, NULL, al_map_rgb(255,0,0),
				menu_multi_player);

     ui_create_menu_nav_element(10,10, "Back", text_font,
				NULL, NULL, menu_settings, NULL, al_map_rgb(255,0,0),
				menu_settings_about);

     /* attach the menus to the main interface */
     ui_attach_menu_to_interface(menu_main, main_menu_ui);
     ui_attach_menu_to_interface(menu_single_player, main_menu_ui);
     ui_attach_menu_to_interface(menu_multi_player, main_menu_ui);
     ui_attach_menu_to_interface(menu_multi_player_remote, main_menu_ui);
     ui_attach_menu_to_interface(menu_load_campaign, main_menu_ui);     
     ui_attach_menu_to_interface(menu_settings, main_menu_ui);
     ui_attach_menu_to_interface(menu_settings_about, main_menu_ui);     
     
     return 0;
}
