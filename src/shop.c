#include <stdio.h>
#include <allegro5/allegro.h>

#include "shop.h"
#include "main.h"
#include "audio.h"
#include "items.h"
#include "player.h"
#include "settings.h"

#include "ui_builder/ui_builder.h"
#include "ui_builder/menus.h"
#include "ui_builder/elements.h"

static int shop_init(void);

static void shop_draw_background(void);
static void shop_draw_shopkeeper(void);
static void shop_draw_selected_item_arrow(void);
static void shop_draw_information(void);

enum _LOCATION {
     CENTER_X, CENTER_Y
} ;

/* the user interface for the shop */
static struct USER_INTERFACE *shop_ui = NULL;

/* info for managing the display of items on screen */
static int selected_item_id = ITEM_EMPTY;
static int selected_item_index = (-1);

static int **on_screen_item_locations; /* for use with drawing selection arrow */
static int num_on_screen_items;

/* images and fonts are stored here statically so that
   they can be loaded and destroyed by separate init and exit functions */
static ALLEGRO_BITMAP *shopkeeper_bmp = NULL, *shopkeeper_dialog_bubble_bmp = NULL,
     *desk_bmp = NULL, *background_bmp = NULL, *selected_arrow_bmp = NULL;

static ALLEGRO_FONT *shopkeeper_dialog_font = NULL, *text_font = NULL,
     *hint_font = NULL;

int shop_run(void)
{
     bool redraw = false;

     if (shop_init()) {
	  fprintf(stderr, "inventory init failed!\n");
	  return 1;
     }

     while (1) {
	  ALLEGRO_EVENT ev;
	  al_wait_for_event(container->event_queue, &ev);

	  switch (ev.type) {
	  case ALLEGRO_EVENT_TIMER:

	       redraw = true;

	       break;
	  case ALLEGRO_EVENT_DISPLAY_CLOSE:

	       ui_destroy_user_interface(shop_ui);

	       return 0;

	  case ALLEGRO_EVENT_MOUSE_AXES:
	  case ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY:
	  case ALLEGRO_EVENT_MOUSE_BUTTON_UP:

	       ui_process_mouse_event(shop_ui, &ev);

	       break;
	  }

	  if (redraw && al_is_event_queue_empty(
		   container->event_queue)) {

	       /* check and enforce proper background music */
	       if (audio_get_current_background_music_id() !=
		   SHOP_BG)
		    start_background_music(SHOP_BG);

	       redraw = false;

	       al_clear_to_color(al_map_rgb(0,0,0));

	       /* draw user interface */
	       shop_draw_background();

	       shop_draw_shopkeeper();

	       shop_draw_information();

	       ui_display_interface(shop_ui);

	       /* has highest weight, draw last */
	       shop_draw_selected_item_arrow();

	       al_flip_display();
	  }
     } ;
     
     return 0;
}

/*
  args should point to an integer array,
  index 0 being the id of the item and
  index 1 being the mappings index of the item
*/
static void helper_set_selected_item_id(void *args)
{
     int *helper = (int *) args;

     /**** DEBUG ***/
     fprintf(stdout, "set selected item id : %d\n",
	     helper[0]);
     fprintf(stdout, "set selected item index : %d\n",
	     helper[1]);
     fprintf(stdout, "selected item x: %d\ty: %d\n",
	     on_screen_item_locations[helper[1]][CENTER_X],
	     on_screen_item_locations[helper[1]][CENTER_Y]);


     selected_item_id = helper[0];
     selected_item_index = helper[1];
}

/*
  purchase an item: check if player has enough
  bones to purchase item, and if so,
  add item to player inventory and subtract cost
*/
static void helper_purchase_item(void *args)
{
     if (selected_item_id == ITEM_EMPTY)
	  return;
     
     struct ITEM *item = items_get_from_id(selected_item_id);

     if (player_get_bones_to_spend() >= item->cost) {
	  /* player has enough to purchase */
	  player_lower_bones_by_amount(item->cost);
	  player_add_item(selected_item_id);

	  /* reset the current shop inventory */
	  shop_init();
     }

     return;
}

static void helper_return(void *args)
{
     ALLEGRO_EVENT ev;
     ev.type = ALLEGRO_EVENT_DISPLAY_CLOSE;
     al_emit_user_event(&(container->event_source),
			&ev, NULL);
}

int shop_init(void)
{
     static int **mappings_item_runnable_args = NULL;
     int ctr;

     /* catch memory leaks */
     if (shop_ui) {
	  ui_destroy_user_interface(shop_ui);

	  /* free on_screen_locations */
	  for (ctr = 0; ctr < num_on_screen_items; ctr++)
	       free(on_screen_item_locations[ctr]);
	  free(on_screen_item_locations);

	  if (mappings_item_runnable_args) {
	       for (ctr = 0; ctr < num_on_screen_items; ctr++) {
		    free(mappings_item_runnable_args[ctr]);
	       }
	       free (mappings_item_runnable_args);

	       mappings_item_runnable_args = NULL;
	  }
	  
	  selected_item_id = ITEM_EMPTY;
	  selected_item_index = (-1);
     }

     static ALLEGRO_BITMAP *shelf_background = NULL;

     /* load fonts */
     if (!text_font)
	  text_font = al_load_font("./data/fonts/ENDOR.ttf", 24, 0);
     if (!hint_font)
	  hint_font = al_load_font("./data/fonts/ENDOR.ttf", 18, 0);
     if (!shopkeeper_dialog_font)
	  shopkeeper_dialog_font = al_load_font("./data/fonts/ENDOR.ttf", 24, 0);

     /* test if fonts were loaded correctly, fail otherwise */
     if (!text_font || !hint_font || !shopkeeper_dialog_font) {
	  fprintf(stderr, "failed to load data/fonts/ENDOR.ttf\n");
	  return 1;
     }

     /* create the shelf background image */
     if (!shelf_background) {
	  shelf_background = al_create_bitmap(SCREEN_W/3,80);
	  al_set_target_bitmap(shelf_background);
	  al_clear_to_color(al_map_rgb(160,82,45));
	  al_set_target_bitmap(al_get_backbuffer(container->display));
     }

     shop_ui = ui_create_interface(0,0,SCREEN_W,SCREEN_H,1);
     if (!shop_ui) {
	  fprintf(stderr, "failed to create shop ui interface structure\n");
	  return 1;
     }

     struct UI_MENU *menu_operations = ui_create_menu(10,10,SCREEN_W/4,200,1,true,
						      NULL,LIST_VERTICAL_TIGHT,0,0,NULL);
     if (!menu_operations) {
	  fprintf(stderr, "failed to create menu operations menu structure\n");
	  return 1;
     }

     int shelf_width = SCREEN_W/3, shelf_height = 80, top_shelf_x_begin = SCREEN_W/2,
	  top_shelf_y_begin = SCREEN_H/5;
     int bot_shelf_x_begin = SCREEN_W/2, bot_shelf_y_begin = SCREEN_H*2/5;

     struct UI_MENU *menu_top_shelf = ui_create_menu(top_shelf_x_begin, top_shelf_y_begin,
						     shelf_width,shelf_height,1,true,
						     shelf_background,0,
						     LIST_HORIZONTAL,0,NULL);
     if (!menu_top_shelf) {
	  fprintf(stderr, "failed to create menu top shelf menu structure\n");
	  return 1;
     }

     struct UI_MENU *menu_bottom_shelf = ui_create_menu(bot_shelf_x_begin, bot_shelf_y_begin,
							shelf_width,shelf_height,1,true,
							shelf_background,0,
							LIST_HORIZONTAL,0,NULL);
     if (!menu_bottom_shelf) {
	  fprintf(stderr, "failed to create menu bottom shelf menu structure\n");
	  return 1;
     }


     /* create elements for bottom and top shelf */
     int i, j, k;
     bool collision_found;
     struct ITEM *item;
     int *items_id_list = items_get_id_list();
     int num_items = items_get_num_items();
     int *player_held_items_id_list = player_get_held_items_id_list();
     int num_player_items = player_get_num_items();

     /* create a new array minus all collisions */
     int num_result_items = num_items - num_player_items;
     int result_list[num_result_items];

     /* instantiate the item location array */
     num_on_screen_items = num_result_items;
     on_screen_item_locations =
	  malloc(sizeof(int *) * num_result_items);
     if (!on_screen_item_locations) {
	  fprintf(stderr, "malloc failed to allocate on screen item locations master\n");
	  return 1;
     }

     for (i = 0; i < num_on_screen_items; i++) {
	  on_screen_item_locations[i] =
	       malloc(sizeof(int) * 2);
	  if (!on_screen_item_locations[i]) {
	       fprintf(stderr, "malloc failed to allocate on screen item locations slave\n");
	       return 1;
	  }
     }

     for (i = 0, k = 0; i < num_items; i++) {
	  for (j = 0, collision_found = false; j < num_player_items; j++) {
	       if (items_id_list[i] == player_held_items_id_list[j])
		    collision_found = true;
	  }

	  if (!collision_found) 
	       result_list[k++] = items_id_list[i];
     }

     struct UI_ELEMENT_FUNC runnable = {helper_set_selected_item_id, NULL};

     /* need to map the arguments of each element to
	the appropriate item id */

     mappings_item_runnable_args = malloc(sizeof(int *) * num_on_screen_items);

     /* create elements for bottom and top rows */
     for (i = 0; i < num_result_items; i++) {
	  /* get the item to be displayed */
	  item = items_get_from_id(result_list[i]);
	  if (!item) {
	       /* shouldn't happen */
	       continue;
	  }

	  /* store the appropriate args in a static variable
	     and store the pointer in the element */
	  mappings_item_runnable_args[i] = malloc(sizeof(int) * 2);
	  mappings_item_runnable_args[i][0] = result_list[i]; /* store id */
	  mappings_item_runnable_args[i][1] = i;

/*	  mappings_item_runnable_args[i] = result_list[i]; */
	  runnable.args = mappings_item_runnable_args[i];

	  if (i > (num_items / 2)) {
	       /* draw on bottom shelf */

	       on_screen_item_locations[i][CENTER_X] =
		    bot_shelf_x_begin + (i * shelf_width / num_result_items);
	       on_screen_item_locations[i][CENTER_Y] =
		    bot_shelf_y_begin;
		    
	       ui_create_action_element(i * shelf_width / num_result_items, 0,
					NULL,NULL,
					item->description, hint_font,
					&runnable,
					item->image, al_map_rgb(255,0,0),
					menu_bottom_shelf);

	  }

	  else {
	       /* draw on top shelf */

	       on_screen_item_locations[i][CENTER_X] =
		    top_shelf_x_begin + (i * shelf_width / num_result_items);
	       on_screen_item_locations[i][CENTER_Y] =
		    top_shelf_y_begin;

	       ui_create_action_element(i * shelf_width / num_result_items, 0,
					NULL,NULL,
					item->description, hint_font,
					&runnable,
					item->image, al_map_rgb(255,0,0),
					menu_top_shelf);

	  }
     }

     runnable.run = helper_return; runnable.args = NULL;

     /* create operations elements : back button and purchase */
     ui_create_action_element(0,0,"Back",text_font,
			      "Return to the campaign map", hint_font,
			      &runnable, NULL, al_map_rgb(128,128,128),
			      menu_operations);

     runnable.run = helper_purchase_item; runnable.args = NULL;
     ui_create_action_element(0,0,"Purchase",text_font,
			      "Purchase this item!", hint_font,
			      &runnable, NULL, al_map_rgb(128,128,128),
			      menu_operations);

     
     
     ui_align_menu(menu_operations);

     ui_attach_menu_to_interface(menu_top_shelf, shop_ui);
     ui_attach_menu_to_interface(menu_bottom_shelf, shop_ui);
     ui_attach_menu_to_interface(menu_operations, shop_ui);

     /***** LOAD GRAPHICS FOR SHOP ****/
     if (!shopkeeper_bmp) {
	  shopkeeper_bmp = al_create_bitmap(SCREEN_W/8,SCREEN_H/2);
	  al_set_target_bitmap(shopkeeper_bmp);
	  al_clear_to_color(al_map_rgb(244,164,96));
	  al_set_target_bitmap(al_get_backbuffer(container->display));
     }

     if (!shopkeeper_dialog_bubble_bmp) {
	  shopkeeper_dialog_bubble_bmp = al_create_bitmap(SCREEN_W/3,SCREEN_H/10);
	  al_set_target_bitmap(shopkeeper_dialog_bubble_bmp);
	  al_clear_to_color(al_map_rgb(255,0,0));
	  al_set_target_bitmap(al_get_backbuffer(container->display));
     }

     if (!desk_bmp) {
	  desk_bmp = al_create_bitmap(SCREEN_W*4/5,SCREEN_H*2/5);
	  al_set_target_bitmap(desk_bmp);
	  al_clear_to_color(al_map_rgb(49,79,79));
	  al_set_target_bitmap(al_get_backbuffer(container->display));
     }

     if (!selected_arrow_bmp) {
	  selected_arrow_bmp = al_create_bitmap(50,100);
	  al_set_target_bitmap(selected_arrow_bmp);
	  al_clear_to_color(al_map_rgb(255,0,0));
	  al_set_target_bitmap(al_get_backbuffer(container->display));
     }


     return 0;
}

/*
  draw textured background for shop

  FUTURE make it cute and pretty
*/
void shop_draw_background(void)
{
     if (!background_bmp) {
	  /* initialize background */
     }

     return;
}

/*
  draw the shopkeeper and the desk

  TODO real graphics
*/
void shop_draw_shopkeeper(void)
{
     /* get the currently selected item in order to
	write appropriate dialog message on screen */
     struct ITEM *item = items_get_from_id(selected_item_id);

     al_draw_bitmap(shopkeeper_bmp,SCREEN_W/8,SCREEN_H/2,0);

     al_draw_bitmap(desk_bmp,SCREEN_W*1/10,SCREEN_H*4/5,0);

     /* if item is selected, draw shopkeeper text */
     if (item) {
	  al_draw_bitmap(shopkeeper_dialog_bubble_bmp,SCREEN_W*5/16,SCREEN_H*5/8,0);

	  al_draw_text(shopkeeper_dialog_font, al_map_rgb(0,0,255),
		       SCREEN_W*5/16 + 18,
		       SCREEN_H*5/8 + 18,
		       0, item->shopkeeper_comment);
     }

     return;

}

/*
  map the selected_item_id to its center location
  on the screen and draw an arrow above it

  TODO
*/
static void shop_draw_selected_item_arrow(void)
{
     /* to be modified manually to print the arrow nicely */
     static int x_offset = 20, y_offset = 80;

     if (selected_arrow_bmp && selected_item_index >= 0) {
	  al_draw_bitmap(selected_arrow_bmp,
			 on_screen_item_locations[selected_item_index][CENTER_X] + x_offset,
	       		 on_screen_item_locations[selected_item_index][CENTER_Y] - y_offset,
			 0);
     }

     return;
}

/*
  write to screen player bones count,
  and if an item is selected, the cost
  of that item
*/
static void shop_draw_information(void)
{
     al_draw_textf(shopkeeper_dialog_font, al_map_rgb(255,0,0),
		   SCREEN_W*2/10,SCREEN_H*8/10,
		   0, "Player bones %d",
		   player_get_bones_to_spend());

     /* if item is selected, draw its cost */
     if (selected_item_id != ITEM_EMPTY) {
	  struct ITEM *item = items_get_from_id(selected_item_id);
	  al_draw_textf(shopkeeper_dialog_font, al_map_rgb(255,0,0),
			SCREEN_W*2/10,SCREEN_H*8/10 +
			      al_get_font_line_height(shopkeeper_dialog_font),
			0, "Item cost %d", item->cost);
     }

     return;
}
