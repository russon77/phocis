#include <stdio.h>
#include <stdlib.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

#include "ui_builder/ui_builder.h"
#include "ui_builder/menus.h"
#include "ui_builder/elements.h"

static unsigned int current_uid = 1001;

/* local prototypes */
/*static ui_align_menu_vertically*/

/*
  construct and return a new menu structure,
    initialized to parameters and default values,

    returning new menu on success

  returns NULL on failure
*/
struct UI_MENU *ui_create_menu(int x, int y, int width, int height,
			       int weight, bool is_displayed,
			       ALLEGRO_BITMAP *background,
			       int back_button_location,
			       int type, int justification,
			       struct UI_MENU *parent)
{
     struct UI_MENU *tmp = malloc(sizeof(struct UI_MENU));
     if (!tmp) {
	  fprintf(stderr, "malloc failed to allocate ui menu\n");
	  return NULL;
     }

     /* set up elements array */
     tmp->num_elements = 0;
     tmp->capacity_elements = UI_NUM_INITIAL_ELEMENTS;
     tmp->elements_array = malloc(sizeof(struct UI_ELEMENT) *
				  UI_NUM_INITIAL_ELEMENTS);
     if (!tmp->elements_array) {
	  fprintf(stderr, "malloc failed to allocate elements array for given menu\n");
	  return NULL;
     }

     /* set up general parameters */
     tmp->id = current_uid;
     current_uid++;
     if (parent)
	  tmp->parent_menu_id = parent->id;
     else
	  tmp->parent_menu_id = (-1);

     tmp->x = x; tmp->y = y;
     tmp->width = width; tmp->height = height;
     tmp->weight = weight;

     tmp->background = background;

     tmp->is_displayed = is_displayed;

     tmp->type = type;
     tmp->justification = justification;

     /* set focus element id to non-existent/null */
     tmp->element_with_input_focus_id = (-1);

     return tmp;
}

/*
  helper function to display a single given menu,
    cycling through each element and displaying based on weight

  TODO Implement weight
*/
void ui_display_menu(struct UI_MENU *menu)
{
     int i;

     if (menu->background)
	  al_draw_bitmap(menu->background, menu->x, menu->y, 0);

     for (i = 0; i < menu->num_elements; i++) {
	  if (menu->elements_array[i].is_displayed)
	       ui_display_element(&(menu->elements_array[i]));
     }

     return;
}

/*
  setters
*/

/*
  returns true on success,
     false if not found
*/
bool ui_set_menu_visible(struct UI_MENU *menu_array, int menu_id)
{
     while (menu_array) {
	  if (menu_array->id == menu_id) {
	       menu_array->is_displayed = true;
	       return true;
	  }
	  menu_array++;
     } ;

     return false;
}

/*
  returns true on success
    false if not found
*/
bool ui_set_menu_invisible(struct UI_MENU *menu_array, int menu_id)
{
     while (menu_array) {
	  if (menu_array->id == menu_id) {
	       menu_array->is_displayed = false;
	       return true;
	  }
	  menu_array++;
     } ;

     return false;
}

/*
  align a menu's elements according to its type

  set each element's xpos and ypos

  returns true on success,
     false on failure

  TODO Implemenet element->list_position,
     error conditions, horizontal and grid alignment
*/
bool ui_align_menu(struct UI_MENU *target)
{
     int i, vert_space;

     if (!target->num_elements) {
	  return false;
     }
     
     if (target->type == LIST_VERTICAL_LOOSE) {

	  /* test for enough room */
	  int total_height = 0;
	  for (i = 0; i < target->num_elements; i++) {
	       total_height += target->elements_array[i].height;
	  }

	  if (total_height > target->height) {
	       fprintf(stderr, "ERROR: not enough room to align menu properly\n");
	       return false;
	  }

	  vert_space = target->height /
	       target->num_elements;

	  for (i = 0; i < target->num_elements; i++) {

	       switch (target->justification) {
	       case LIST_LEFTED:
		    target->elements_array[i].x = 0;
		    break;
	       case LIST_CENTERED:
		    target->elements_array[i].x =
			 (target->width -
			  al_get_text_width(target->elements_array[i].text_font,
					    target->elements_array[i].text));

		    target->elements_array[i].x /= 2;
		    break;
	       case LIST_RIGHTED:
		    break;
	       }

	       target->elements_array[i].y =
		    i * vert_space;
	  }
	  
     }

     else if (target->type == LIST_VERTICAL_TIGHT) {

	  /* test for enough room */
	  int total_height = 0;
	  for (i = 0; i < target->num_elements; i++) {
	       total_height += target->elements_array[i].height;
	  }

	  if (total_height > target->height) {
	       fprintf(stderr, "ERROR: not enough room to align menu properly\n");
	       return false;
	  }

	  vert_space = 10;

	  target->elements_array[0].x =
	       target->elements_array[0].y = 0;

	  for (i = 1; i < target->num_elements; i++) {

	       /* set the appropriate x value */
	       switch (target->justification) {
	       case LIST_LEFTED:
		    target->elements_array[i].x = 0;
		    break;
	       case LIST_CENTERED:
		    target->elements_array[i].x =
			 (target->width -
			  al_get_text_width(target->elements_array[i].text_font,
					    target->elements_array[i].text));

		    target->elements_array[i].x /= 2;
		    break;
	       case LIST_RIGHTED:
		    break;
	       }

	       /* and set the appropriate y value */

	       vert_space += target->elements_array[i - 1].height;

	       target->elements_array[i].y =
		    vert_space;
		    
	  }
     }

     else if (target->type == LIST_HORIZONTAL) {

	  /* first check for enough room */
	  int total_width = 0;
	  for (i = 0; i < target->num_elements; i++) {
	       total_width += target->elements_array[i].width;
	  }

	  if (total_width > target->width) {
	       fprintf(stderr, "ERROR: not enough room to align menu properly\n");
	       return false;
	  }

	  vert_space = 10;

	  for (i = 0; i < target->num_elements; i++) {

	       /* set the y value */
	       target->elements_array[i].y = vert_space;

	       /* set the appropriate x value */
	       target->elements_array[i].x =
		    (target->width / (target->num_elements + 1)) * (i+1) -
		    (target->elements_array[i].width / 2);
		    
	  }

	  
     }

     return true;
}

/*
  destroy this menu, cycling through each element
*/
void ui_destroy_menu(struct UI_MENU *target)
{
     int i;
     for (i = 0; i < target->num_elements; i++) {
	  ui_destroy_element(&(target->elements_array[i]));
     }

     free(target->elements_array);

     return;
}
