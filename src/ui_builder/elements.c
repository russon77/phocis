#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

#include "ui_builder/ui_builder.h"
#include "ui_builder/elements.h"
#include "ui_builder/menus.h"

/* function prototypes */

static double max_of_three(double a, double b, double c);
static void draw_hit_box(struct UI_ELEMENT *elem);
static void ui_display_hint_dialog(struct UI_ELEMENT *elem);

/* beginning id number for elements
   idk should this be random or something?
*/

static unsigned int current_uid = 10001;

/*
  returns 0 upon success, 1 upon failure

  instantiate an element with given constructor
    values and attach it to given menu

  if goto menu is supplied, then upon mouse click
    that menu will be changed

  else if function is supplied, then that function
    will be called upon click

  otherwise this element is for text, possibly
    text-display or text-edit
*/

int ui_create_element(int x_offset, int y_offset, int width, int height, int type,
		      int weight, bool is_displayed,
		      struct UI_ELEMENT_FUNC *action,
		      struct UI_MENU *goto_menu,
		      char *text, int text_x_offset, int text_y_offset,
		      char *hint, ALLEGRO_BITMAP *bg,
		      int flags,
		      ALLEGRO_FONT *text_font,
		      ALLEGRO_FONT *hint_font,
		      ALLEGRO_COLOR color,
		      struct UI_MENU *parent)
{
     if (!parent) {
	  fprintf(stderr, "error ui menu parent does not exist\n");
	  return 1;
     }

     /* implement array list resizing */

     struct UI_ELEMENT *tmp;

	  
     if (parent->num_elements == parent->capacity_elements) {

	  parent->capacity_elements *= 2;
	  tmp = realloc(parent->elements_array,
			sizeof(struct UI_ELEMENT) * parent->capacity_elements);
	  if (!tmp) {
	       fprintf(stderr, "fatal error realloc failed to increase size of \
elements array for menu when needed\n");
	       return 1;
	  }

	  parent->elements_array = tmp;
     }

     tmp = &(parent->elements_array[parent->num_elements]);

     /****
	  construct element
     ****/

     tmp->is_displayed = is_displayed;

     tmp->id = current_uid;
     current_uid++;
     tmp->parent_id = parent->id;

     tmp->x = x_offset +
	  parent->x;
     tmp->y = y_offset +
	  parent->y;

     /* 
	set width and height
          to the greatest of given width/height,
	  text width/height, or image width/height
     */
     if (text && text_font && bg) {
	  tmp->width = max_of_three(width + text_x_offset, al_get_text_width(text_font, text),
				    al_get_bitmap_width(bg));
	  tmp->height = max_of_three(width + text_y_offset, al_get_font_line_height(text_font),
				    al_get_bitmap_height(bg));
     }
     else if (!text && bg) {
	  tmp->width = al_get_bitmap_width(bg);
	  tmp->height = al_get_bitmap_height(bg);
     }
     else if (text && text_font && !bg) {
	  tmp->width = text_x_offset + al_get_text_width(text_font, text);
	  tmp->height = al_get_font_line_height(text_font) + text_y_offset;
     }

/*
     printf("new element:\n");
     printf("x: %d\ty: %d\n", tmp->x, tmp->y);
     printf("width: %d\t height: %d\n", tmp->width, tmp->height);
*/

     tmp->weight = weight;

     /*
       tricky part, dealing with how a button actually
         acts upon events
     */
     tmp->element_type = type;
     switch (type) {
     case MENU_CHANGE:
	  tmp->purpose.goto_menu_id = goto_menu->id;
	  break;
     case EXTERNAL_FUNC:
	  tmp->purpose.action = *action;
	  break;
     case EDITABLE_TEXT:
	  tmp->purpose.text_pos = 0;
	  break;
     }
     
     /* string/text stuff
	 minus one for guaranteed null termination */
     size_t size;
     if (text) {
	  tmp->text_x = text_x_offset;
	  tmp->text_y = text_y_offset;
     
	  size = (sizeof(tmp->text) * sizeof(char));

	  strncpy(tmp->text, text,
		  size - 1);

	  tmp->text[size - 1] = '\0';
     }
     else {
	  memset(tmp->text, '\0', sizeof(tmp->text) * sizeof(char));
     }

     if (hint) {
	  size = (sizeof(tmp->hint) * sizeof(char));
	  strncpy(tmp->hint, hint,
		  size - 1);

	  tmp->hint[
	       size - 1] = '\0';
     }
     else {
	  memset(tmp->hint, '\0', sizeof(tmp->hint) * sizeof(char));
     }

     /* end of text stuff,
         onto general stuff */
     tmp->is_hovered =
	  tmp->is_focused = false;

     tmp->background = bg;
     tmp->text_font = text_font;
     tmp->hint_font = hint_font;
     tmp->color = color;

     /* lastly and very importantly so that elements don't get over written */
     parent->num_elements++;
     
     return 0;
}

/*
  display given ui element given appropriate state
*/
void ui_display_element(struct UI_ELEMENT *elem)
{
     ALLEGRO_COLOR to_draw_color;

     if (elem->is_hovered) {
	  unsigned char r, g, b;
	  al_unmap_rgb(elem->color, &r, &b, &g);
	  unsigned char m = max_of_three(r,b,g);
	  if (m == r)
	       to_draw_color = al_map_rgb(0,255,0);
	  else if (m == b)
	       to_draw_color = al_map_rgb(0,0,255);
	  else
	       to_draw_color = al_map_rgb(255,0,0);

     }
     else {
	  to_draw_color = elem->color;
     }

     if (elem->background) {
	  al_draw_bitmap(elem->background,
			 elem->x,
			 elem->y,
			 0);
     }

     if (elem->text_font && elem->text) {
	  al_draw_text(elem->text_font,
		       to_draw_color,
		       elem->x + elem->text_x,
		       elem->y + elem->text_y,
		       ALLEGRO_ALIGN_LEFT,
		       elem->text);
     }

     if (elem->is_hovered && elem->hint && elem->hint_font) {
	  ui_display_hint_dialog(elem);
     }

     /* for debug purposes
	draw_hit_box(elem); */

     return;
}

/*
  has highest 'weight'

  TODO 
    
*/
void ui_display_hint_dialog(struct UI_ELEMENT *elem)
{
     int room_x, room_y;
     int text_w, text_h;
     int direction_y = 0; /* 0 means down, 1 means up */
     int direction_x = 0; /* 0 means right, 1 means left */
     int y_offset = 18; /* offset to put dialog below mouse cursor */
     ALLEGRO_COLOR to_draw_color; /* color to draw text with */

     /* find color to draw text with */
     unsigned char r, g, b;
     al_unmap_rgb(elem->color, &r, &b, &g);
     r = ~r; g = ~g; b = ~b;
     to_draw_color = al_map_rgb(r,g,b);
     
     /* find dimensions of hint dialog to display */

     room_y = SCREEN_HEIGHT - elem->hover_y - y_offset;
     text_h = al_get_font_line_height(elem->hint_font);

     if (text_h > room_y) {
	  /* display going upwards instead of downwards */
	  direction_y = 1;
     }

     room_x = SCREEN_WIDTH - elem->hover_x;
     text_w = al_get_text_width(elem->hint_font, elem->hint);

     if (text_w > room_x) {
	  /* try to display hint on page anyway */
	  direction_x = 1;
     }

     // draw text in box
     al_draw_text(elem->hint_font, to_draw_color,
		  elem->hover_x - (text_w * direction_x),
		  elem->hover_y + y_offset - (text_h * direction_y),
		  0,
		  elem->hint); 



     return;
}

/* small helper function */
static double max_of_three(double a, double b, double c)
{
     if (a > b) {
	  if (a > c)
	       return a;
	  else
	       return c;
     }

     else {
	  if (b > c)
	       return b;
	  else
	       return c;
     }
     
}

/*
  helper function to draw a hitbox without using allegro primitives,
  although that means that this is very inefficient and should only
  be used for small testing purposes
*/
static void draw_hit_box(struct UI_ELEMENT *elem)
{
     int i, j;
     for (i = elem->x; i < (elem->x + elem->width); i++)

	  for (j = elem->y; j < (elem->y + elem->height); j++)

	       al_put_pixel(i, j, al_map_rgb(255,255,255));

     return;
}

/*
  external interfaces to ui_create_element

  meant to smooth the creation of elements
*/
int ui_create_simple_display_element(int x_offset, int y_offset,
				     char *text, ALLEGRO_FONT *text_font,
				     char *hint, ALLEGRO_FONT *hint_font,
				     ALLEGRO_BITMAP *background,
				     ALLEGRO_COLOR color,
				     struct UI_MENU *parent)
{
     return ui_create_element(x_offset, y_offset, 0, 0, CONSTANT_TEXT,
			      1, true, NULL, NULL, text, 0, 0,
			      hint, background, 0, text_font, hint_font,
			      color, parent);
}

int ui_create_action_element(int x_offset, int y_offset,
			     char *text, ALLEGRO_FONT *text_font,
			     char *hint, ALLEGRO_FONT *hint_font,
			     struct UI_ELEMENT_FUNC *action,
			     ALLEGRO_BITMAP *background,
			     ALLEGRO_COLOR color,
			     struct UI_MENU *parent)
{
     return ui_create_element(x_offset, y_offset, 0, 0, EXTERNAL_FUNC,
			      1, true, action, NULL, text, 0, 0,
			      hint, background, 0, text_font, hint_font,
			      color, parent);
     
}

int ui_create_menu_nav_element(int x_offset, int y_offset,
			       char *text, ALLEGRO_FONT *text_font,
			       char *hint, ALLEGRO_FONT *hint_font,
			       struct UI_MENU *goto_menu,
			       ALLEGRO_BITMAP *background,
			       ALLEGRO_COLOR color,
			       struct UI_MENU *parent)
{

     return ui_create_element(x_offset, y_offset, 0, 0, MENU_CHANGE,
			      1, true, NULL, goto_menu, text, 0, 0,
			      hint, background, 0, text_font, hint_font,
			      color, parent);

}

/*
  free the dynamically allocated memory

  TODO FIXME i dont know whats happening
*/
void ui_destroy_element(struct UI_ELEMENT *target)
{
/*     if (target->background)
       al_destroy_bitmap(target->background); */

     /* something something i am not sure why fonts work at all... */
/*     if (target->text_font)
	  al_destroy_font(target->text_font);

     if (target->hint_font)
     al_destroy_font(target->hint_font); */

     return;
}
