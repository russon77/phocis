#include <stdio.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>

#include "ui_builder/ui_builder.h"
#include "ui_builder/elements.h"
#include "ui_builder/menus.h"

void my_test_function(void *args)
{
     printf("i got called by the nicest button!\n");

     return;
}

int main(int argc, char *argv[])
{
     ALLEGRO_DISPLAY *display;
     ALLEGRO_TIMER *timer;
     ALLEGRO_EVENT_QUEUE *event_queue;
     bool redraw = false;
     
     al_init();
     al_init_image_addon();
     al_init_font_addon();
     al_init_ttf_addon();

     al_install_mouse();
	  
     display = al_create_display(640, 480);

     timer = al_create_timer(1.0 / 30.0);
     event_queue = al_create_event_queue();
     al_register_event_source(event_queue, al_get_timer_event_source(timer));
     al_register_event_source(event_queue, al_get_mouse_event_source());

     al_start_timer(timer);

     ALLEGRO_FONT *font = al_load_font("unispace.ttf", 24, 0);
     if (!font) {
	  fprintf(stderr, "failed to load font\n");

	  return 1;
     }

     ALLEGRO_FONT *hint_font = al_load_font("unispace.ttf", 10, 0);

     ui_init_all(display);

     /*********


	TESTING GROUNDS


     *********/

     struct USER_INTERFACE *my_ui =
	  ui_create_interface(0, 0, 640, 480, 1);

     struct UI_MENU *my_top_menu = ui_create_menu(0, 0, 640, 480, 1, true,
						  NULL, 
						  NOWHERE,
						  NULL);
     struct UI_MENU *my_bottom_menu = ui_create_menu(100, 100, 100, 100, 1, false,
						     NULL, 
						     NOWHERE,
						     my_top_menu);


     ui_create_simple_display_element(100,100, "hello world", font,
				      "goodbye world", hint_font,
				      NULL, al_map_rgb(0,0,255),
				      my_top_menu);

     ui_create_element(400, 450, 0, 0, MENU_CHANGE,
		       1, true,
		       NULL,
		       my_bottom_menu,
		       "hello world",
		       0, 0,
		       "love",
		       NULL,
		       0,
		       font, hint_font,
		       al_map_rgb(255, 0, 0),
		       my_top_menu);

     ui_create_element(0, 100, 0,0, EXTERNAL_FUNC,
		       1, true,
		       my_test_function, NULL,
		       "press me", 0, 0, "love me", NULL, 0, font, hint_font, al_map_rgb(0,255,0),
		       my_top_menu);
     
     
     ui_create_element(10, 10, 0, 0, MENU_CHANGE,
		       2, true,
		       NULL, my_top_menu,
		       "i love phocis",
		       0,0,
		       NULL, NULL,0,font, hint_font,al_map_rgb(0,0,255),
		       my_bottom_menu);
     
     ui_attach_menu_to_interface(my_top_menu, my_ui);
     ui_attach_menu_to_interface(my_bottom_menu, my_ui);

     /* main loop */

     while (1) {
	  ALLEGRO_EVENT ev;
	  al_wait_for_event(event_queue, &ev);

	  if (ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
	       ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
	       ui_process_mouse_event(my_ui, &ev);
	  }

	  if (ev.type == ALLEGRO_EVENT_TIMER) {
	       redraw = true;
	  }

	  if (redraw && al_is_event_queue_empty(event_queue)) {

	       al_clear_to_color(al_map_rgb(0,0,0));

	       ui_display_interface(my_ui);

	       al_flip_display();
	  }
     } ;

     return;
}
