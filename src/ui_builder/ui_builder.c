#include <stdio.h>
#include <allegro5/allegro.h>

#include "ui_builder/ui_builder.h"
#include "ui_builder/menus.h"
#include "ui_builder/elements.h"

/* external variables */
int SCREEN_WIDTH;
int SCREEN_HEIGHT;

/* static variables */
static unsigned int current_uid = 1;

/*
  initialization routine

  needed for grabbing screen dimensions

  returns 1 upon failure; 0 upon success
*/
int ui_init_all(ALLEGRO_DISPLAY *display)
{
     SCREEN_WIDTH = al_get_display_width(display);
     SCREEN_HEIGHT = al_get_display_height(display);

     return 0;
}


/*
  allocate memory and instantiate an empty
  user interface

  remember to check for return values
*/
struct USER_INTERFACE *ui_create_interface(int x, int y, int width, int height,
					   int weight)
{
     struct USER_INTERFACE *tmp =
	  malloc(sizeof(struct USER_INTERFACE));

     if (!tmp) {
	  fprintf(stderr, "failed to allocate user interface struct\n");
	  return NULL;
     }

     tmp->x = x; tmp->y = y;
     tmp->width = width; tmp->height = height;
     tmp->weight = weight;

     tmp->menu_array = malloc(sizeof(struct UI_MENU) * UI_NUM_INITIAL_MENUS);
     if (!tmp->menu_array) {
	  fprintf(stderr, "malloc failed to allocate struct UI MENU\n");
	  return NULL;
     }

     tmp->num_menus = 0;
     tmp->capacity_menus = UI_NUM_INITIAL_MENUS;
     
     tmp->current_menu_id = current_uid;
     current_uid++;

     return tmp;
}

/*
  take a given interface and process given mouse event by
    cycling through each menu, cycling through each element
    until a match is found and then set proper state
*/
void ui_process_mouse_event(struct USER_INTERFACE *ui, ALLEGRO_EVENT *ev)
{
     int i, j;
     int start_y, end_y;
     int start_x, end_x;

     struct UI_MENU *current_menu;
     struct UI_ELEMENT *current_element;

     /* loop through each menu */
     for (i = 0; i < ui->num_menus; i++) {
	  
	  current_menu = &(ui->menu_array[i]);

	  if (!current_menu->is_displayed)
	       continue;

	  /* loop through each element */
	  for (j = 0; j < current_menu->num_elements; j++) {

	       current_element = &(current_menu->elements_array[j]);

	       /* reset hover state for current element */
	       current_element->is_hovered = false;

	       if (!current_element->is_displayed)
		    continue;

	       /* calculate hit box positions */
	       start_y = ui->menu_array[i].elements_array[j].y;
	       end_y = start_y + ui->menu_array[i].elements_array[j].height;

	       start_x = ui->menu_array[i].elements_array[j].x;
	       end_x = start_x + ui->menu_array[i].elements_array[j].width;

	       /* test hit box positions versus mouse */
	       if (ev->mouse.y >= start_y && ev->mouse.y <= end_y &&
		   ev->mouse.x >= start_x && ev->mouse.x <= end_x) {

		    /*
		    printf("mouse event with element id %d\n",
			   ui->menu_array[i].elements_array[j].id);

		    printf("mouse x: %d\tmouse y: %d\n", ev->mouse.x, ev->mouse.y);
		    printf("start_y: %d\tend_y: %d\nstart_x: %d\tend_x: %d\n",
			   start_y, end_y, start_x, end_x);
		    */

		    if (ev->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {

			 switch (ui->menu_array[i].elements_array[j].element_type) {
			 case EXTERNAL_FUNC:

			      /* call external function stored in element */
			      ui->menu_array[i].elements_array[j].purpose.action.run(
				   ui->menu_array[i].elements_array[j].purpose.action.args);

			      break;
			 case MENU_CHANGE:

			      /* set appropriate visibility for menus */
			      ui_set_menu_visible(ui->menu_array,
				   ui->menu_array[i].elements_array[j].purpose.goto_menu_id);
			      ui_set_menu_invisible(ui->menu_array,
				   ui->menu_array[i].id);
			      break;

			 }
		    }

		    else if (ev->type == ALLEGRO_EVENT_MOUSE_AXES ||
			     ev->type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {

			 current_element->is_hovered = true;
			 current_element->hover_x = ev->mouse.x;
			 current_element->hover_y = ev->mouse.y;
			 
		    }

		    /* only one element at a time should be affected
		       by a single mouse event */
		    return;
	       }
	  }
     }
}

/*
  cycle through the menus stored in given user interface structure,
    in turn displaying them

  TODO implement weight
*/
void ui_display_interface(struct USER_INTERFACE *ui)
{
     int i;
     for (i = 0; i < ui->num_menus; i++) {

	  if (ui->menu_array[i].is_displayed)
	       ui_display_menu(&(ui->menu_array[i]));
     }

     return;
}

/*
  returns 1 upon failure,
    returns 0 upon success
*/
int ui_attach_menu_to_interface(struct UI_MENU *menu, struct USER_INTERFACE *ui)
{
     struct UI_MENU *tmp = NULL;

     /* implement array list functionality */
     if (ui->num_menus == ui->capacity_menus) {
	  ui->capacity_menus *= 2;
	  tmp = realloc(ui->menu_array,
					sizeof(struct UI_MENU) * ui->capacity_menus);

	  if (!tmp) {
	       fprintf(stderr, "failed to realloc menu array for given interface\n");
	       return 1;
	  }

	  ui->menu_array = tmp;
     }

     ui->menu_array[ui->num_menus] =
	  *menu;

     free(menu);

     ui->num_menus++;

     return 0;
}

/*
  cycle through each menu, destroying each
*/
void ui_destroy_user_interface(struct USER_INTERFACE *target)
{
     int i;
     for (i = 0; i < target->num_menus; i++) {
	  ui_destroy_menu(&(target->menu_array[i]));
	  /* free this menu??? */
     }

     free(target->menu_array);

     free(target);

     return;
}
