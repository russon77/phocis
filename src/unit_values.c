/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <allegro5/allegro.h>

#include "unit_values.h"
#include "settings.h"

/*
  Define our external variables here
*/
struct UNIT_VALUES unit_values[3];

/*
  Function prototypes
*/
void unit_values_load_internal_settings(void);
/*
  Function definitions
*/

/*
  write default/initial values for each unit structure

  long and tedious much, but well worth it when you consider
   that now every variable is no longer a static DEFINE but
   a modifiable value... which we needed for our 'levelup'
   feature

   // TODO make this into a configuration file
   // TODO split into two functions,
         load from config file or load from internal defaults
*/
void unit_values_load(ALLEGRO_CONFIG *cfg)
{
     // if cfg is NULL, then load internal values
     if (!cfg) {
	  unit_values_load_internal_settings();
     }
     
     /*********************

      SOLDIER VALUES

     ********************/
     
     unit_values[UNIT_SOLDIER].DEFAULT_COOLDOWN =
          atof(al_get_config_value(cfg, "unit soldier", "cooldown default"));
     unit_values[UNIT_SOLDIER].USER_COOLDOWN_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "cooldown user mod"));
//     soldier_values.AI_COOLDOWN_MOD = 1.0f;

     unit_values[UNIT_SOLDIER].DEFAULT_MANA_COST =
          atof(al_get_config_value(cfg, "unit soldier", "mana default"));
     unit_values[UNIT_SOLDIER].USER_MANA_COST_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "mana user mod"));
//     soldier_values.AI_MANA_COST_MOD = 1.0f;

     unit_values[UNIT_SOLDIER].DEFAULT_HEALTH =
          atof(al_get_config_value(cfg, "unit soldier", "health default"));          
     unit_values[UNIT_SOLDIER].USER_HEALTH_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "health user mod"));          
     unit_values[UNIT_SOLDIER].AI_HEALTH_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "health ai mod"));

     unit_values[UNIT_SOLDIER].DEFAULT_DAMAGE =
          atof(al_get_config_value(cfg, "unit soldier", "damage default"));
     unit_values[UNIT_SOLDIER].USER_DAMAGE_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "damage user mod"));
     unit_values[UNIT_SOLDIER].AI_DAMAGE_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "damage ai mod"));

     unit_values[UNIT_SOLDIER].DEFAULT_AGGRO_RANGE =
          atof(al_get_config_value(cfg, "unit soldier", "aggro range default"));
     unit_values[UNIT_SOLDIER].USER_AGGRO_RANGE_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "aggro range user mod"));
     unit_values[UNIT_SOLDIER].AI_AGGRO_RANGE_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "aggro range ai mod"));

     unit_values[UNIT_SOLDIER].MOVEMENT_MODIFIER =
          atof(al_get_config_value(cfg, "unit soldier", "movement mod"));

     // TODO this isn't used in game
     unit_values[UNIT_SOLDIER].DEFAULT_DEFENSE =
          atof(al_get_config_value(cfg, "unit soldier", "defense default"));          
     unit_values[UNIT_SOLDIER].USER_DEFENSE_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "defense user mod"));          
     unit_values[UNIT_SOLDIER].AI_DEFENSE_MOD =
          atof(al_get_config_value(cfg, "unit soldier", "defense ai mod"));          
     
     /*********************

      ARCHER VALUES

     ********************/

     unit_values[UNIT_ARCHER].DEFAULT_COOLDOWN =
	  atof(al_get_config_value(cfg, "unit archer", "cooldown default"));
     unit_values[UNIT_ARCHER].USER_COOLDOWN_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "cooldown user mod"));
/*     soldier_values.AI_COOLDOWN_MOD = 1.0f; */

     unit_values[UNIT_ARCHER].DEFAULT_MANA_COST =
	  atof(al_get_config_value(cfg, "unit archer", "mana default"));
     unit_values[UNIT_ARCHER].USER_MANA_COST_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "mana user mod"));
/*     soldier_values.AI_MANA_COST_MOD = 1.0f; */

     unit_values[UNIT_ARCHER].DEFAULT_HEALTH =
	  atof(al_get_config_value(cfg, "unit archer", "health default"));
     unit_values[UNIT_ARCHER].USER_HEALTH_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "health user mod"));
     unit_values[UNIT_ARCHER].AI_HEALTH_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "health ai mod"));

     unit_values[UNIT_ARCHER].DEFAULT_DAMAGE =
	  atof(al_get_config_value(cfg, "unit archer", "damage default"));
     unit_values[UNIT_ARCHER].USER_DAMAGE_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "damage user mod"));
     unit_values[UNIT_ARCHER].AI_DAMAGE_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "damage ai mod"));

     unit_values[UNIT_ARCHER].DEFAULT_AGGRO_RANGE = (int)( (float)SCREEN_W *
	  atof(al_get_config_value(cfg, "unit archer", "aggro range default")));
     unit_values[UNIT_ARCHER].USER_AGGRO_RANGE_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "aggro range user mod"));
     unit_values[UNIT_ARCHER].AI_AGGRO_RANGE_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "aggro range ai mod"));

     unit_values[UNIT_ARCHER].MOVEMENT_MODIFIER =
	  atof(al_get_config_value(cfg, "unit archer", "movement mod"));

     // TODO this isn't used in game
     unit_values[UNIT_ARCHER].DEFAULT_DEFENSE =
	  atof(al_get_config_value(cfg, "unit archer", "defense default"));
     unit_values[UNIT_ARCHER].USER_DEFENSE_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "defense user mod"));
     unit_values[UNIT_ARCHER].AI_DEFENSE_MOD =
	  atof(al_get_config_value(cfg, "unit archer", "defense ai mod"));


     /*********************

      WIZARD VALUES

     ********************/

     unit_values[UNIT_WIZARD].DEFAULT_COOLDOWN =
	  atof(al_get_config_value(cfg, "unit wizard", "cooldown default"));
     unit_values[UNIT_WIZARD].USER_COOLDOWN_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "cooldown user mod"));
/*     soldier_values.AI_COOLDOWN_MOD = 1.0f; */

     unit_values[UNIT_WIZARD].DEFAULT_MANA_COST =
	  atof(al_get_config_value(cfg, "unit wizard", "mana default"));
     unit_values[UNIT_WIZARD].USER_MANA_COST_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "mana user mod"));
/*     soldier_values.AI_MANA_COST_MOD = 1.0f; */

     unit_values[UNIT_WIZARD].DEFAULT_HEALTH =
	  atof(al_get_config_value(cfg, "unit wizard", "health default"));
     unit_values[UNIT_WIZARD].USER_HEALTH_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "health user mod"));
     unit_values[UNIT_WIZARD].AI_HEALTH_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "health ai mod"));

     unit_values[UNIT_WIZARD].DEFAULT_DAMAGE =
	  atof(al_get_config_value(cfg, "unit wizard", "health default"));
     unit_values[UNIT_WIZARD].USER_DAMAGE_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "health user mod"));
     unit_values[UNIT_WIZARD].AI_DAMAGE_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "health ai mod"));

     unit_values[UNIT_WIZARD].DEFAULT_AGGRO_RANGE = (int)((float)SCREEN_W *
	  atof(al_get_config_value(cfg, "unit wizard", "aggro range default")));
     unit_values[UNIT_WIZARD].USER_AGGRO_RANGE_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "aggro range user mod"));
     unit_values[UNIT_WIZARD].AI_AGGRO_RANGE_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "aggro range ai mod"));

     unit_values[UNIT_WIZARD].MOVEMENT_MODIFIER =
	  atof(al_get_config_value(cfg, "unit wizard", "movement mod"));

     // TODO this isn't used in game
     unit_values[UNIT_WIZARD].DEFAULT_DEFENSE =
	  atof(al_get_config_value(cfg, "unit wizard", "defense default"));
     unit_values[UNIT_WIZARD].USER_DEFENSE_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "defense user mod"));
     unit_values[UNIT_WIZARD].AI_DEFENSE_MOD =
	  atof(al_get_config_value(cfg, "unit wizard", "defense ai mod"));

     return;
}

/*
  Internal values stored for game,

  ONLY TO BE CALLED if configuration file
  EXISTS but is misconfigured
*/
void unit_values_load_internal_settings(void)
{
     /*********************

      SOLDIER VALUES

     ********************/

     unit_values[UNIT_SOLDIER].DEFAULT_COOLDOWN = 4.0f;
     unit_values[UNIT_SOLDIER].USER_COOLDOWN_MOD = 1.0f;
/*     soldier_values.AI_COOLDOWN_MOD = 1.0f; */

     unit_values[UNIT_SOLDIER].DEFAULT_MANA_COST = 4.0f;
     unit_values[UNIT_SOLDIER].USER_MANA_COST_MOD = 1.0f;
/*     soldier_values.AI_MANA_COST_MOD = 1.0f; */

     unit_values[UNIT_SOLDIER].DEFAULT_HEALTH = 1000.0f;
     unit_values[UNIT_SOLDIER].USER_HEALTH_MOD = 1.0f;
     unit_values[UNIT_SOLDIER].AI_HEALTH_MOD = 1.0f;

     unit_values[UNIT_SOLDIER].DEFAULT_DAMAGE = 100.0f;
     unit_values[UNIT_SOLDIER].USER_DAMAGE_MOD = 1.0f;
     unit_values[UNIT_SOLDIER].AI_DAMAGE_MOD = 1.0f;

     unit_values[UNIT_SOLDIER].DEFAULT_AGGRO_RANGE = 20.0f;
     unit_values[UNIT_SOLDIER].USER_AGGRO_RANGE_MOD = 1.0f; 
     unit_values[UNIT_SOLDIER].AI_AGGRO_RANGE_MOD = 1.0f;

     unit_values[UNIT_SOLDIER].MOVEMENT_MODIFIER = 1.0f;

     // TODO this isn't used in game
     unit_values[UNIT_SOLDIER].DEFAULT_DEFENSE = 25.0f;
     unit_values[UNIT_SOLDIER].USER_DEFENSE_MOD = 1.0f;
     unit_values[UNIT_SOLDIER].AI_DEFENSE_MOD = 1.0f;

     /*********************

      ARCHER VALUES

     ********************/

     unit_values[UNIT_ARCHER].DEFAULT_COOLDOWN = 7.0f;
     unit_values[UNIT_ARCHER].USER_COOLDOWN_MOD = 1.0f;
/*     soldier_values.AI_COOLDOWN_MOD = 1.0f; */

     unit_values[UNIT_ARCHER].DEFAULT_MANA_COST = 7.0f;
     unit_values[UNIT_ARCHER].USER_MANA_COST_MOD = 1.0f;
/*     soldier_values.AI_MANA_COST_MOD = 1.0f; */

     unit_values[UNIT_ARCHER].DEFAULT_HEALTH = 400.0f;
     unit_values[UNIT_ARCHER].USER_HEALTH_MOD = 1.0f;
     unit_values[UNIT_ARCHER].AI_HEALTH_MOD = 1.0f;

     unit_values[UNIT_ARCHER].DEFAULT_DAMAGE = 80.0f;
     unit_values[UNIT_ARCHER].USER_DAMAGE_MOD = 1.0f;
     unit_values[UNIT_ARCHER].AI_DAMAGE_MOD = 1.0f;

     unit_values[UNIT_ARCHER].DEFAULT_AGGRO_RANGE = SCREEN_W / 2;
     unit_values[UNIT_ARCHER].USER_AGGRO_RANGE_MOD = 1.0f; 
     unit_values[UNIT_ARCHER].AI_AGGRO_RANGE_MOD = 1.0f;

     unit_values[UNIT_ARCHER].MOVEMENT_MODIFIER = 1.4f;

     // TODO this isn't used in game
     unit_values[UNIT_ARCHER].DEFAULT_DEFENSE = 5.0f;
     unit_values[UNIT_ARCHER].USER_DEFENSE_MOD = 1.0f;
     unit_values[UNIT_ARCHER].AI_DEFENSE_MOD = 1.0f;


     /*********************

      WIZARD VALUES

     ********************/

     unit_values[UNIT_WIZARD].DEFAULT_COOLDOWN = 10.0f;
     unit_values[UNIT_WIZARD].USER_COOLDOWN_MOD = 1.0f;
/*     soldier_values.AI_COOLDOWN_MOD = 1.0f; */

     unit_values[UNIT_WIZARD].DEFAULT_MANA_COST = 10.0f;
     unit_values[UNIT_WIZARD].USER_MANA_COST_MOD = 1.0f;
/*     soldier_values.AI_MANA_COST_MOD = 1.0f; */

     unit_values[UNIT_WIZARD].DEFAULT_HEALTH = 250.0f;
     unit_values[UNIT_WIZARD].USER_HEALTH_MOD = 1.0f;
     unit_values[UNIT_WIZARD].AI_HEALTH_MOD = 1.0f;

     unit_values[UNIT_WIZARD].DEFAULT_DAMAGE = 10.0f;
     unit_values[UNIT_WIZARD].USER_DAMAGE_MOD = 1.0f;
     unit_values[UNIT_WIZARD].AI_DAMAGE_MOD = 1.0f;

     unit_values[UNIT_WIZARD].DEFAULT_AGGRO_RANGE = SCREEN_W / 4;
     unit_values[UNIT_WIZARD].USER_AGGRO_RANGE_MOD = 1.0f;
     unit_values[UNIT_WIZARD].AI_AGGRO_RANGE_MOD = 1.0f;

     unit_values[UNIT_WIZARD].MOVEMENT_MODIFIER = 0.95f;

     // TODO this isn't used in game
     unit_values[UNIT_WIZARD].DEFAULT_DEFENSE = 2.0f;
     unit_values[UNIT_WIZARD].USER_DEFENSE_MOD = 1.0f;
     unit_values[UNIT_WIZARD].AI_DEFENSE_MOD = 1.0f;

     return;
     
}
