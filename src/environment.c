/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "environment.h"
#include "settings.h"

static struct BACKGROUND *bg = NULL;

/*
  initialize background with simple template

  TODO FIXME
*/

int init_background(ALLEGRO_DISPLAY *display)
{
     bg = malloc(sizeof(struct BACKGROUND));
     if (!bg) {
	  fprintf(stderr, "failed to allocate struct BACKGROUND\n");
	  return 1;
     }

     // height represents pixels DOWNWARDS from TOP
     
     bg->grass_y = SCREEN_H - SCREEN_H * 20 / 100;
     bg->grass_height = SCREEN_H * 20 / 100;
     bg->grass = al_create_bitmap(SCREEN_W, bg->grass_height);
     al_set_target_bitmap(bg->grass);

     al_clear_to_color(al_map_rgb(0,255,0));

     bg->road_y = SCREEN_H - SCREEN_H * 50 / 100;
     bg->road_height = SCREEN_H * 30 / 100;
     bg->road = al_create_bitmap(SCREEN_W, bg->road_height);
     al_set_target_bitmap(bg->road);

     al_clear_to_color(al_map_rgb(51,25,0));
     
     bg->sky_height = SCREEN_H * 50 / 100;
     bg->sky = al_create_bitmap(SCREEN_W, bg->sky_height);
     al_set_target_bitmap(bg->sky);

     al_clear_to_color(al_map_rgb(153,255,255));


     al_set_target_bitmap(al_get_backbuffer(display));


     return 0;
}

/*
  Write background to screen buffer
*/

void environment_display(void)
{

     
     // draw sky
     al_draw_bitmap(bg->sky, 0, 0, 0);

     // draw road
     al_draw_bitmap(bg->road, 0, bg->road_y, 0);

     // draw grass
     al_draw_bitmap(bg->grass, 0, bg->grass_y, 0);

     return;
}
