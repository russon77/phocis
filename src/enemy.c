/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <stdio.h>
#include <allegro5/allegro.h>

#ifdef __linux__
#include <sys/types.h>
#include <sys/socket.h>
#endif

#ifdef _WIN32
#include <winsock.h>
#endif

#include "enemy.h"
#include "player.h"
#include "unit_values.h"
#include "units.h"
#include "waves.h"
#include "multiplayer.h"

static void enemy_parse_remote_input(const char *, int);

struct ENEMY_PLAYER *enemy_pl = NULL;
struct WAVE *current_wave = NULL;

bool force_new_wave = true;
bool wave_over = false;
bool game_over = false;
bool game_paused = false;

int ENEMY_SOURCE = ENEMY_AI;

void *enemy_thread(ALLEGRO_THREAD *thr, void *args)
{
     char buffer[256];

     waves_reset_count();
     
     while (1) {
	  // pause the game for a little bit,
	  // to free up them lovely cpu cycles
	  if (game_paused) {
	       al_rest(10.0/60.0);
	       continue;
	  }
	  
	  if (ENEMY_SOURCE == ENEMY_AI) {

	       al_rest(1.0/60.0);

	       // if ai needs new wave, get new wave
	       if (ai_need_new_wave()) {
		    // ai needs next wave,
		    // attempt to get next wave
		    wave_over = true;

		    if (!ai_get_new_wave(waves_get_next_wave(DEFAULT_WAVE_SOURCE))) {
			 // victory!
			 game_over = true;
		    }

	       }
	       // otherwise summon a unit
	       else {
		    ai_move();
	       }
	  }

	  else if (ENEMY_SOURCE == ENEMY_REMOTE_MULT) {
	       // simply wait for input from the socket
	       // TODO
	       // read(blah blah blah)

	       memset(buffer, '\0', sizeof(buffer));

	       if (recv(remote_fd, buffer, sizeof(buffer), 0) > 0) {
		    // read was successful

		    enemy_parse_remote_input(buffer, sizeof(buffer));
	       }

	       else {
		    fprintf(stderr,
			    "read failed on remote port\nexiting...\n");

		    break;
	       }
	  }

	  /* if the 'enemy' is from the campaign, get the next
	        campaign wave */
	  else if (ENEMY_SOURCE == ENEMY_CAMPAIGN_AI) {

	       al_rest(1.0f/60.0f);

	       if (ai_need_new_wave()) {

		    wave_over = true;

		    if (!ai_get_new_wave(waves_get_next_wave(CAMPAIGN_WAVE_SOURCE)))
			 game_over = true;
	       }
	       else {
		    ai_move();
	       }
	       
	       
	  }
     }

     return NULL;
}

/*
  attempt to parse remote input, following protocol defined:

  "summon x" : attempt to summon unit of that type
  "message x" : write message to local player NOT IMPLEMENTED
  "quit" : remote disconnect

  other inputs to be added later
*/
static void enemy_parse_remote_input(const char *input, int size)
{
     printf("Got enemy text : %s\n", input);

     if (!strncmp(input, "summon", 6)) {

	  printf("Got enemy summon : %s\n", input);

	  enemy_summon_unit(input[7] - '0');

	  
/*	  if (input[strlen("summon") + 2]) {
	  enemy_summon_unit(input[8] - '0'); 
	  }*/
     }
}

/*
  ENEMY SUMMONS A UNIT
  for use with multiplayer, when enemy
  is player controlled
*/
void enemy_summon_unit(int unit_type)
{
     if (ENEMY_SOURCE == ENEMY_REMOTE_MULT) {
	  // mana and cooldowns are controlled remotely,
	  // don't need to check or manage here

	  if (add_unit(AI_OWNED, unit_type)) {
	       fprintf(stderr, "failed to add enemy unit\n");
	  }

	  return;
     }


     float unit_mana_cost = unit_values[unit_type].DEFAULT_MANA_COST;
     // *  unit_values[unit_type].AI_MANA_COST_MOD;

     if (!enemy_is_spell_on_cooldown(unit_type) &&
	 enemy_get_current_mana() > (unit_mana_cost) &&
	 enemy_get_units_summoned() < enemy_get_max_units_summoned()) {

	  enemy_increase_units_summoned();
	  enemy_lower_mana(unit_mana_cost);

	  enemy_set_spell_on_cooldown(unit_type);

	  if (add_unit(AI_OWNED, unit_type)) {
	       fprintf(stderr, "failed to add enemy unit\n");
	       return;
	  }
     }

     return;
}

/*
  allocate and initialize enemy player structure
*/
int enemy_load(int enemy_src)
{
     /* if enemy_pl structure already exists, do not reallocate --
        just reset all values to defaults
     */
        
     if (!enemy_pl) {

          enemy_pl = 
               malloc(sizeof(struct ENEMY_PLAYER));
          if (!enemy_pl) {
               fprintf(stderr, "malloc failed allocating struct enemy player\n");
               return 1;
          }
     }

     // SET THE LOCAL STATE VARIABLE ENEMY SOURCE,
     // only if it is different
     if (enemy_src != ENEMY_AI)
	  ENEMY_SOURCE = enemy_src;

     enemy_pl->health = 
	  enemy_pl->health_max = HEALTH_MAX;
     enemy_pl->mana = 
	  enemy_pl->mana_max = MANA_MAX;
     enemy_pl->direction = DIRECTION_LEFT;

     enemy_pl->time_since_last_summon = 0.0f;
     enemy_pl->wave = 0;

     enemy_pl->num_units_summoned = 0;

     enemy_pl->spell_on_cooldown[UNIT_SOLDIER] =
	  enemy_pl->spell_on_cooldown[UNIT_WIZARD] =
	  enemy_pl->spell_on_cooldown[UNIT_ARCHER] = false;

     enemy_pl->spell_cooldowns[UNIT_SOLDIER] =
	  unit_values[UNIT_SOLDIER].DEFAULT_COOLDOWN;
     enemy_pl->spell_cooldowns[UNIT_ARCHER] =
	  unit_values[UNIT_ARCHER].DEFAULT_COOLDOWN;
     enemy_pl->spell_cooldowns[UNIT_WIZARD] =
	  unit_values[UNIT_WIZARD].DEFAULT_COOLDOWN;

     enemy_pl->bones_to_spend = 5;

     // don't need threads if it's local multiplayer
     if (ENEMY_SOURCE == ENEMY_LOCAL_MULT) {
	  // printf("enemy is local multiplayer\n");
	  return 0;
     }

     // otherwise enemy is either ai or
     // remote player;

     // create the enemy thread
     enemy_pl->thread = al_create_thread(enemy_thread, NULL);

     // and start the enemy thread
     al_start_thread(enemy_pl->thread);

     return 0;
}

/*
  if local AI is enemy, then this is called
   to process next 'turn'
*/
int ai_move(void)
{
     if (!enemy_pl)
          enemy_load(ENEMY_AI);

     if (!current_wave)
	  return 1;

     // increase our ai summon timer
     enemy_pl->time_since_last_summon += (1.0f)/(60.0f);

     if (enemy_pl->time_since_last_summon > AI_DEFAULT_TIME_BETWEEN_SUMMONS &&
	  current_wave->units_array_pos < current_wave->units_array_size) {
	  // summon the next unit
	  // NOTE might break, need to test 'next unit' retrieval

	  if (add_unit(AI_OWNED, current_wave->units_array[current_wave->units_array_pos++])) {
	       fprintf(stderr, "failed to add unit\n");
	       return 1;
	  }

	  // reset the ai summon timer
	  enemy_pl->time_since_last_summon = 0.0f;

	  // increase ai number of units summoned
	  enemy_pl->num_units_summoned++;
     }
     
     return 0;
}

/*
  Decrease functions
*/

void enemy_lower_health(int type)
{
     enemy_pl->health -= type * 3;

     // TODO victory condition

     return;
}

void enemy_lower_summoned_count(void)
{
     enemy_pl->num_units_summoned--;

     return;
}

void enemy_lower_units_summoned_to_zero(void)
{
     enemy_pl->num_units_summoned = 0;

     return;
}

void enemy_lower_mana(float amount)
{
     if ((enemy_pl->mana - amount) >= 0.0f)
	  enemy_pl->mana -= amount;

     return;
}

/*
  Increase Functions
*/

void enemy_increase_units_summoned(void)
{
     enemy_pl->num_units_summoned++;

     return;
}

void enemy_increase_mana(float amount)
{
     if ((enemy_pl->mana + amount) <= 100.0f)
	  enemy_pl->mana += amount;

     return;
}

void enemy_lower_cooldowns(float amount)
{
          if (enemy_is_spell_on_cooldown(UNIT_SOLDIER)) {
	  if ((enemy_pl->spell_cooldowns[UNIT_SOLDIER] - amount) > 0.0f)
	       enemy_pl->spell_cooldowns[UNIT_SOLDIER] -= amount;
	  else {
	       enemy_pl->spell_cooldowns[UNIT_SOLDIER] = 0.0f;
	       enemy_pl->spell_on_cooldown[UNIT_SOLDIER] = false;
	  }
     }

     if (enemy_is_spell_on_cooldown(UNIT_ARCHER)) {
	  if ((enemy_pl->spell_cooldowns[UNIT_ARCHER] - amount) > 0.0f)
	       enemy_pl->spell_cooldowns[UNIT_ARCHER] -= amount;
	  else {
	       enemy_pl->spell_cooldowns[UNIT_ARCHER] = 0.0f;
	       enemy_pl->spell_on_cooldown[UNIT_ARCHER] = false;
	  }
     }


     if (enemy_is_spell_on_cooldown(UNIT_WIZARD)) {
	  if ((enemy_pl->spell_cooldowns[UNIT_WIZARD] - amount) > 0.0f)
	       enemy_pl->spell_cooldowns[UNIT_WIZARD] -= amount;
	  else {
	       enemy_pl->spell_cooldowns[UNIT_WIZARD] = 0.0f;
	       enemy_pl->spell_on_cooldown[UNIT_WIZARD] = false;
	  }
     }

}

/*
  Getters
*/

float enemy_get_current_health(void)
{
     return enemy_pl->health;
}

float enemy_get_max_health(void)
{
     return enemy_pl->health_max;
}

float enemy_get_current_mana(void)
{
     return enemy_pl->mana;
}

float enemy_get_max_mana(void)
{
     return enemy_pl->mana_max;
}

int enemy_get_units_summoned(void)
{
     return enemy_pl->num_units_summoned;
}

int enemy_get_max_units_summoned(void)
{
     return MAX_UNITS;
}

bool enemy_is_spell_on_cooldown(int unit)
{
     return enemy_pl->spell_on_cooldown[unit];
}

float enemy_get_cooldown_remaining(int unit)
{
     return enemy_pl->spell_cooldowns[unit];
}

int enemy_get_bones_to_spend(void)
{
     return enemy_pl->bones_to_spend;
}

/*
  returns TRUE if there is another wave,
  returns FALSE if there are no more waves
*/
bool ai_get_new_wave(struct WAVE *wave)
{
     if (!wave) {
	  // UNSUCCESSFUL reading of next wave
	  return false;
     }
     
     // free the current wave (IF IT EXISTS)
     // and set the next wave
     if (current_wave) {
	  free(current_wave->units_array);
	  free(current_wave);
     }
     
     current_wave = wave;

     return true;
}

/*
  Setters
*/

void enemy_set_spell_on_cooldown(int type)
{
     enemy_pl->spell_on_cooldown[type] = true;
     enemy_pl->spell_cooldowns[type] = unit_values[type].DEFAULT_COOLDOWN;
     // / unit_values[type].AI_COOLDOWN_MOD;

     return;
}

/*
  Utility Functions
*/

/*
  return TRUE if ai needs a new wave
  otherwise return FALSE
*/
bool ai_need_new_wave(void)
{
     if (ENEMY_SOURCE != ENEMY_AI) {
	  return false;
     }
     
     if (force_new_wave) {
	  force_new_wave = false;
	  return true;
     }

     if (current_wave == NULL) {
	  return true;
     }

     // if the ai has any units summoned, do NOT start a new wave
     if (enemy_pl->num_units_summoned) {
	  return false;
     }

     // however if the array position holder is at the end of the array,
     // we DO start a new wave
     if (current_wave->units_array_pos == current_wave->units_array_size) {
	  return true;
     }

     return false;
}
