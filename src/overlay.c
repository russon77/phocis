/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

#include "overlay.h"
#include "main.h"
#include "settings.h"
#include "player.h"
#include "waves.h"
#include "units.h"
#include "unit_values.h"
#include "enemy.h"

/*
  FIXME
  TODO remove all magic numbers
*/

struct OVERLAY_RESOURCES *overlay = NULL;

/*
  Prototypes
*/
static void overlay_display_local_enemy(struct ALLEGRO_RESOURCES *);

/*
  process a mouse click within the ui

  NOTE this may have to be majorly redone
   if we decide to make individual units
   selectable / managed

*/
void overlay_process_mouse_click(int x, int y)
{
     if (x > 240 && x < (240 + al_get_bitmap_width(overlay->archer_spell)) &&
	 y > 10 && y < (10 + al_get_bitmap_height(overlay->archer_spell)))

	  cast_spell(UNIT_ARCHER);

     else if (x > 355 && x < (355 + al_get_bitmap_width(overlay->archer_spell)) &&
	 y > 10 && y < (10 + al_get_bitmap_height(overlay->archer_spell)))

	  cast_spell(UNIT_SOLDIER);

     else if (x > 470 && x < (470 + al_get_bitmap_width(overlay->archer_spell)) &&
	 y > 10 && y < (10 + al_get_bitmap_height(overlay->archer_spell)))

	  cast_spell(UNIT_WIZARD);


     /* local multiplayer mouse support */

     if (ENEMY_SOURCE == ENEMY_LOCAL_MULT) {

	  if (x > (SCREEN_W - 240 - al_get_bitmap_width(overlay->archer_spell)) &&
	      x < (SCREEN_W - 240) &&
	      y > 10 &&
	      y < (10 + al_get_bitmap_height(overlay->archer_spell)))

	       enemy_summon_unit(UNIT_ARCHER);

	  else if (x > (SCREEN_W - 355 - al_get_bitmap_width(overlay->soldier_spell)) &&
	      x < (SCREEN_W - 355) &&
	      y > 10 &&
	      y < (10 + al_get_bitmap_height(overlay->soldier_spell)))

	       enemy_summon_unit(UNIT_SOLDIER);

	  else if (x > (SCREEN_W - 470 - al_get_bitmap_width(overlay->wizard_spell)) &&
	      x < (SCREEN_W - 470) &&
	      y > 10 &&
	      y < (10 + al_get_bitmap_height(overlay->wizard_spell)))

	       enemy_summon_unit(UNIT_WIZARD);
	  
     }

     return;
     
}


/*
  draw the overlay

  TODO move allegro struct to local variables
  TODO make fps external
*/
void overlay_display(struct ALLEGRO_RESOURCES *container,
		     float fps)
{
     /*  draw frames per second (fps),
	 dynamic based on screen width ONLY IF
	 game mode is local multiplayer, may
	 be changed in future */
     int fps_xpos = 590;
     if (ENEMY_SOURCE == ENEMY_LOCAL_MULT)
	  fps_xpos = (SCREEN_W - al_get_text_width(container->font, "60.0")) / 2;
     al_draw_textf(container->font, al_map_rgb(0,0,0),
		   fps_xpos, 0, ALLEGRO_ALIGN_LEFT,
		   "%.1f", fps);

     // calculate health percent
     float health_percent;
     health_percent = player_get_current_health() /
	  player_get_max_health();
     
     // print out empty health bar,
     al_draw_bitmap(overlay->health_bar_empty, 10,
                    10, 0);
     
     // then print out current health over it
     al_draw_bitmap_region(overlay->health_bar_full, 0, 0,
                           206.0f *health_percent,
                           28, 10, 10, 0);

     // calculate mana percent
     float mana_percent;
     mana_percent = player_get_current_mana() /
	  player_get_max_mana();
     
     // print out empty mana bar
     al_draw_bitmap(overlay->mana_bar_empty, 10,
                    45, 0);
     // then print out current mana over it
     al_draw_bitmap_region(overlay->mana_bar_full, 0, 0,
                           (float) 206 * mana_percent,
                           28, 10, 45, 0);

     /* draw spells */

     // draw archer spell, tinting if on cooldown
     
     if (player_is_spell_on_cooldown(UNIT_ARCHER)) {

          al_draw_tinted_bitmap(overlay->archer_spell,
                                al_map_rgba_f(1, 1, 1, 0.2),
                                240, 10, 0);
          al_draw_textf(container->font, al_map_rgb(0,0,0),
                       310, 40, ALLEGRO_ALIGN_LEFT,
			"%.1f",
			player_get_cooldown_remaining(UNIT_ARCHER));
     }
     else {
          al_draw_bitmap(overlay->archer_spell,
                         240, 10, 0);
     }

     // draw key
     al_draw_text(container->font, al_map_rgb(0,0,0),
                  260, 40, ALLEGRO_ALIGN_LEFT,
                  "Q");

     // draw soldier spell, tinting if on cooldown
     
     if (player_is_spell_on_cooldown(UNIT_SOLDIER)) {

          al_draw_tinted_bitmap(overlay->soldier_spell,
                                al_map_rgba_f(1, 1, 1, 0.2),
                                355, 10, 0);
          al_draw_textf(container->font, al_map_rgb(0,0,0),
                       425, 40, ALLEGRO_ALIGN_LEFT,
			"%.1f",
			player_get_cooldown_remaining(UNIT_SOLDIER));
     }
     else {
          al_draw_bitmap(overlay->soldier_spell,
                         355, 10, 0);
     }

     // draw key
     al_draw_text(container->font, al_map_rgb(0,0,0),
                  375, 40, ALLEGRO_ALIGN_LEFT,
                  "W");

     // draw wizard spell, tinting if on cooldown
     
     if (player_is_spell_on_cooldown(UNIT_WIZARD)) {

          al_draw_tinted_bitmap(overlay->wizard_spell,
                                al_map_rgba_f(1, 1, 1, 0.2),
                                470, 10, 0);
          al_draw_textf(container->font, al_map_rgb(0,0,0),
                       540, 40, ALLEGRO_ALIGN_LEFT,
			"%.1f",
			player_get_cooldown_remaining(UNIT_WIZARD));
     }
     else {
          al_draw_bitmap(overlay->wizard_spell,
                         470, 10, 0);
     }

     // draw key
     al_draw_text(container->font, al_map_rgb(0,0,0),
                  490, 40, ALLEGRO_ALIGN_LEFT,
                  "E");

     // print out number of units on screen versus max units
     al_draw_textf(container->font, al_map_rgb(0,0,0),
		  600, 50, ALLEGRO_ALIGN_LEFT,
		   "%d/%d",
		   player_get_units_summoned(),
		   player_get_max_units_summoned());

     // print out wave number
     if (ENEMY_SOURCE == ENEMY_AI) {
	  al_draw_textf(container->font, al_map_rgb(0,0,0),
			10, 80, ALLEGRO_ALIGN_LEFT,
			"wave %d",
			wave_number - 1);
     }

     // print out spooky BONES collected,
     // BUT NOT IF game mode is multiplayer
     if (ENEMY_SOURCE != ENEMY_LOCAL_MULT &&
	 ENEMY_SOURCE != ENEMY_REMOTE_MULT) {

	  al_draw_textf(container->font, al_map_rgb(0,0,0),
			10, 120, ALLEGRO_ALIGN_LEFT,
			"bones %d", player_get_bones_to_spend());
     }

     if (ENEMY_SOURCE == ENEMY_LOCAL_MULT) {
	  // draw local enemy user interface
	  // FIXME this is a clone of the above

	  overlay_display_local_enemy(container);
     }

     return;
}


/*
  ENEMY OVERLAY FOR LOCAL MULTIPLAYER ONLY

*/
void overlay_display_local_enemy(struct ALLEGRO_RESOURCES *container)
{

     // calculate health percent
     float health_percent;
     health_percent = enemy_get_current_health() /
	  enemy_get_max_health();

     
     // print out empty health bar,
     al_draw_bitmap(overlay->health_bar_empty,
		    SCREEN_W - 10 - al_get_bitmap_width(overlay->health_bar_empty),
                    10, 0);
     
     // then print out current health over it
     al_draw_bitmap_region(overlay->health_bar_full, 0, 0,
                           206.0f *health_percent,
                           28,
			   SCREEN_W - 10 - al_get_bitmap_width(overlay->health_bar_empty),
			   10,
			   0);

     // calculate mana percent
     float mana_percent;
     mana_percent = enemy_get_current_mana() /
	  enemy_get_max_mana();
     
     // print out empty mana bar
     al_draw_bitmap(overlay->mana_bar_empty,
                    SCREEN_W - 10 - al_get_bitmap_width(overlay->mana_bar_empty),
		    45,
		    0);
     // then print out current mana over it
     al_draw_bitmap_region(overlay->mana_bar_full, 0, 0,
			   206.0f * mana_percent,
                           28,
			   SCREEN_W - 10 - al_get_bitmap_width(overlay->mana_bar_empty),
			   45,
			   0);


     /* draw spells */

     // draw archer spell, tinting if on cooldown
     
     if (enemy_is_spell_on_cooldown(UNIT_ARCHER)) {

          al_draw_tinted_bitmap(overlay->archer_spell,
                                al_map_rgba_f(1, 1, 1, 0.2),
                                SCREEN_W - 240 - al_get_bitmap_width(overlay->archer_spell),
				10, 0);
          al_draw_textf(container->font, al_map_rgb(0,0,0),
			SCREEN_W - 310 - al_get_text_width(container->font, ".0"),
			40, ALLEGRO_ALIGN_LEFT,
			"%.1f",
			enemy_get_cooldown_remaining(UNIT_ARCHER));
     }
     else {
          al_draw_bitmap(overlay->archer_spell,
                         SCREEN_W - 240 - al_get_bitmap_width(overlay->archer_spell),
			 10, 0);
     }

     // draw key
     al_draw_text(container->font, al_map_rgb(0,0,0),
                  SCREEN_W - 325 - al_get_text_width(container->font, "I"),
		  40, ALLEGRO_ALIGN_RIGHT,
                  "P");

     // draw soldier spell, tinting if on cooldown
     
     if (enemy_is_spell_on_cooldown(UNIT_SOLDIER)) {

          al_draw_tinted_bitmap(overlay->soldier_spell,
                                al_map_rgba_f(1, 1, 1, 0.2),
                                SCREEN_W - 355 - al_get_bitmap_width(overlay->soldier_spell),
				10, 0);
          al_draw_textf(container->font, al_map_rgb(0,0,0),
			SCREEN_W - 425 - al_get_text_width(container->font, ".0"),
			40, ALLEGRO_ALIGN_LEFT,
			"%.1f",
			enemy_get_cooldown_remaining(UNIT_SOLDIER));
     }
     else {
          al_draw_bitmap(overlay->soldier_spell,
                         SCREEN_W - 355 - al_get_bitmap_width(overlay->soldier_spell),
			 10, 0);
     }

     // draw key
     al_draw_text(container->font, al_map_rgb(0,0,0),
                  SCREEN_W - 425 - al_get_text_width(container->font, "O"),
		  40, ALLEGRO_ALIGN_RIGHT,
                  "O");

     // draw wizard spell, tinting if on cooldown
     
     if (enemy_is_spell_on_cooldown(UNIT_WIZARD)) {

          al_draw_tinted_bitmap(overlay->wizard_spell,
                                al_map_rgba_f(1, 1, 1, 0.2),
                                SCREEN_W - 470 - al_get_bitmap_width(overlay->wizard_spell),
				10, 0);
          al_draw_textf(container->font, al_map_rgb(0,0,0),
			SCREEN_W - 540 - al_get_text_width(container->font, ".0"),
			40, ALLEGRO_ALIGN_RIGHT,
			"%.1f",
			enemy_get_cooldown_remaining(UNIT_WIZARD));
     }
     else {
          al_draw_bitmap(overlay->wizard_spell,
			 SCREEN_W - 470 - al_get_bitmap_width(overlay->wizard_spell),
                         10, 0);
     }

     // draw key
     al_draw_text(container->font, al_map_rgb(0,0,0),
                  SCREEN_W - 525 - al_get_text_width(container->font, "P"),
		  40, ALLEGRO_ALIGN_RIGHT,
                  "I");

     // print out number of units on screen versus max units
     al_draw_textf(container->font, al_map_rgb(0,0,0),
		   SCREEN_W - 600,
		   50, ALLEGRO_ALIGN_RIGHT,
		   "%d/%d",
		   enemy_get_units_summoned(),
		   enemy_get_max_units_summoned());

     // print out round number
     // TODO
     al_draw_textf(container->font, al_map_rgb(0,0,0),
		   SCREEN_W - 10,
		   80, ALLEGRO_ALIGN_RIGHT,
		   "round %d",
		   wave_number - 1);

     // print out spooky BONES collected
     al_draw_textf(container->font, al_map_rgb(0,0,0),
		   SCREEN_W - 10,
		   120, ALLEGRO_ALIGN_RIGHT,
		   "bones %d", enemy_get_bones_to_spend());
}

/*
  function to load all images

  should probably call this during loading of game,
  but it probably won't take very long anyway
*/
int overlay_load(void)
{
     if (!overlay) {
	  overlay = malloc(sizeof(struct OVERLAY_RESOURCES));

	  if (!overlay) {
	       fprintf(stderr, "malloc error allocating overlay resources struct\n");

	       return 0;
	  }
     }

     overlay->health_bar_empty = NULL;
     overlay->mana_bar_empty = NULL;
     overlay->health_bar_empty = overlay->mana_bar_empty =
          al_load_bitmap("./data/assets/lifebars/EmptyBar.png");

     if (!overlay->health_bar_empty) {
          fprintf(stderr, "error loading EmptyBar.png\n");
          return 0;
     }

     overlay->health_bar_full = NULL;
     overlay->health_bar_full =
          al_load_bitmap("./data/assets/lifebars/RedBar.png");
     if (!overlay->health_bar_full) {
          fprintf(stderr, "error loading RedBar.png\n");
          return 0;
     }

     overlay->mana_bar_full = NULL;
     overlay->mana_bar_full =
          al_load_bitmap("./data/assets/lifebars/BlueBar.png");
     if (!overlay->mana_bar_full) {
          fprintf(stderr, "error loading BlueBar.png\n");
          return 0;
     }

     overlay->archer_spell = NULL;
     overlay->archer_spell =
          al_load_bitmap("./data/assets/spells/archer.png");
     if (!overlay->archer_spell) {
          fprintf(stderr, "error loading image archer.png\n");
          return 0;
     }

/*     player_set_spell_on_cooldown(UNIT_ARCHER);
       player_set_spell_cooldown(unit_values[UNIT_ARCHER].DEFAULT_COOLDOWN); */

     overlay->soldier_spell = NULL;
     overlay->soldier_spell =
          al_load_bitmap("./data/assets/spells/soldier.png");
     if (!overlay->soldier_spell) {
          fprintf(stderr, "error loading image soldier.png\n");
          return 0;
     }
     

/*     player_set_spell_on_cooldown(UNIT_SOLDIER);
       player_set_spell_cooldown(unit_values[UNIT_SOLDIER].DEFAULT_COOLDOWN); */

     overlay->wizard_spell = NULL;
     overlay->wizard_spell =
          al_load_bitmap("./data/assets/spells/wizard.png");
     if (!overlay->wizard_spell) {
          fprintf(stderr, "error loading image wizard.png\n");
          return 0;
     }

/*     player_set_spell_on_cooldown(UNIT_WIZARD);
       player_set_spell_cooldown(unit_values[UNIT_WIZARD].DEFAULT_COOLDOWN); */

     return 1;
}
