#include <stdio.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include "ui_builder/ui_builder.h"
#include "ui_builder/menus.h"
#include "ui_builder/elements.h"

#include "campaign.h"
#include "main.h"
#include "settings.h"
#include "audio.h"
#include "player.h"
#include "error.h"
#include "battle.h"
#include "enemy.h"
#include "inventory.h"
#include "items.h"
#include "shop.h"

static int campaign_init(void);
static int campaign_load_save_file(const char *savefile);

static void draw_location_connections(void);
static void draw_player_sprite(void);

static struct USER_INTERFACE *campaign_ui = NULL;
static struct LOCATION campaign_locations[NUM_LOCATIONS];

/* file-scoped variables */
static ALLEGRO_FONT *text_font = NULL, *hint_font = NULL;
static ALLEGRO_BITMAP *main_map_background_bmp = NULL,
     *location_bmp = NULL;

/*
   temporary ui for campaign mode, just experiments and stuff
*/
int start_campaign(const char *savefile)
{
     bool redraw = false;

     if (savefile) {
	  /* load savefile */
     }

     /* initialize initializations */

     
     if (campaign_init()) {
	  fprintf(stderr, "FATAL ERROR campaign menu failed to initialize\n");
	  return 1;
     }

     stop_background_music();

     /* main campaign loop */
     while (1) {

	  ALLEGRO_EVENT ev;
	  al_wait_for_event(container->event_queue, &ev);

	  switch (ev.type) {
	  case ALLEGRO_EVENT_TIMER:

	       redraw = true;

	       break;
	  case ALLEGRO_EVENT_DISPLAY_CLOSE:

	       ui_destroy_user_interface(campaign_ui);

	       return 0;

	  case ALLEGRO_EVENT_MOUSE_AXES:
	  case ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY:
	  case ALLEGRO_EVENT_MOUSE_BUTTON_UP:

	       ui_process_mouse_event(campaign_ui, &ev);

	       break;
	  }

	  if (redraw && al_is_event_queue_empty(
		   container->event_queue)) {

	       /* check and enforce proper background music */
	       if (audio_get_current_background_music_id() !=
		   CAMPAIGN_MENU_BG)
		    start_background_music(CAMPAIGN_MENU_BG);

	       redraw = false;

	       al_clear_to_color(al_map_rgb(0,0,0));

	       // draw user interface
	       ui_display_interface(campaign_ui);

	       draw_location_connections();

	       draw_player_sprite();

	       al_flip_display();
	       
	  }
     } ;

     return 0;
}

static void helper_print_debug(void *args)
{
     printf("i am BIZARRO debug\n");
}

/*
  trickery! create my own fake event that will
  return to main menu
*/
static void helper_return_to_main_menu(void *args)
{
     ALLEGRO_EVENT ev;
     ev.type = ALLEGRO_EVENT_DISPLAY_CLOSE;
     al_emit_user_event(&(container->event_source),
			&ev, NULL);
}

static void helper_initiate_battle(void *args)
{
     /*
       works by mapping the current player location,
	  available from player_get_current_location(),
	to the appropriate wave...

	TODO
     */

     play_game(ENEMY_CAMPAIGN_AI);
}

static void helper_move_player_to_location(void *args)
{
     int *target_id = ((int *)args);

     /* DEBUG 
     fprintf(stderr,"target_id: %d\t", *target_id);
     fprintf(stderr,"location_left_id: %d\t",
	    campaign_locations[player_get_current_location()].connected_location_ids[LEFT]);
     fprintf(stderr,"location_right_id: %d\t",
	    campaign_locations[player_get_current_location()].connected_location_ids[RIGHT]);
     */
          

     /* test if location is valid */
     if (campaign_locations[player_get_current_location()].connected_location_ids[LEFT] ==
	 *target_id ||
	 campaign_locations[player_get_current_location()].connected_location_ids[RIGHT] ==
	 *target_id) {

	  player_set_current_location(*target_id);
     }

     else
	  error_add_new_dialog("invalid location!");

     return;

}

static void helper_run_inventory(void *args)
{
     inventory_run();
}

static void helper_run_shop(void *args)
{
     shop_run();
}

/*
  initialize and load the campaign ui and
  other init functions necessary to polay
*/
static int campaign_init(void)
{
     /* reload everything except items */
     if (campaign_ui) {
	  /* TODO
	     reset state of user interface
	  */
	  ui_destroy_user_interface(campaign_ui);
	  campaign_ui = NULL;
     }

     else {
	  if (items_init()) {
	       fprintf(stderr, "failed to initialize items\n");
	       return 1;
	  }

	  init_player();
     }
     
     if (!al_init_primitives_addon()) {
	  fprintf(stderr, "failed to init primitives addon\n");
	  return 1;
     }

     /* load fonts */
     if (!text_font)
	  text_font = al_load_font("./data/fonts/ENDOR.ttf", 18, 0);
     if (!hint_font)
	  hint_font = al_load_font("./data/fonts/ENDOR.ttf", 14, 0);

     if (!text_font || !hint_font) {
	  fprintf(stderr, "failed to load data/fonts/ENDOR.ttf\n");
	  return 1;
     }

     /* create ui */
     int top_menu_y_offset = 80;
     campaign_ui = ui_create_interface(0,0,SCREEN_W,SCREEN_H,1);
     if (!campaign_ui) {
	  fprintf(stderr, "failed to create campaign ui interface struct\n");
	  return 1;
     }

     /* create top menu */
     struct UI_MENU *menu_campaign_top = ui_create_menu(0,0,SCREEN_W,top_menu_y_offset,
							1,true,NULL,0,LIST_HORIZONTAL,
							0,NULL);
     if (!menu_campaign_top) {
	  fprintf(stderr, "failed to create menu_campaign_top menu structure\n");
	  return 1;
     }

     /* create elements */
     /* these are place holders for now */
     struct UI_ELEMENT_FUNC func = {helper_return_to_main_menu, NULL};

     ui_create_action_element(0,0,"Back",text_font,
			      "Return to the main menu", hint_font,
			      &func, NULL, al_map_rgb(128,128,128),
			      menu_campaign_top);

     func.run = helper_run_shop; func.args = NULL;
     ui_create_action_element(0,0,"Shop",text_font,
			      "Browse and purchase items and upgrades", hint_font,
			      &func, NULL, al_map_rgb(128,128,128),
			      menu_campaign_top);

     func.run = helper_run_inventory; func.args = NULL;
     ui_create_action_element(0,0,"Inventory",text_font,
			      "Check out all your sweet items!",hint_font,
			      &func, NULL, al_map_rgb(128,128,128),
			      menu_campaign_top);

     func.run = helper_print_debug; func.args = NULL;
     ui_create_action_element(0,0,"Save",text_font,
			      "Save the game", hint_font,
			      &func, NULL, al_map_rgb(128,128,128),
			      menu_campaign_top);





     /* align top menu */
     ui_align_menu(menu_campaign_top);

     /* attach top menu to ui */
     ui_attach_menu_to_interface(menu_campaign_top, campaign_ui);


     /* create map ui, writing each element and location */

     /* temporary background */
     if (!main_map_background_bmp) {
	  main_map_background_bmp = al_create_bitmap(SCREEN_W, SCREEN_H - 80);
	  al_set_target_bitmap(main_map_background_bmp);
	  al_clear_to_color(al_map_rgb(0,255,0));
	  al_set_target_bitmap(al_get_backbuffer(container->display));
     }

     /* load location image */
     if (!location_bmp) {
	  location_bmp = al_load_bitmap("data/assets/ui/battle_location_marker.png");
	  if (!location_bmp) {
	       fprintf(stderr, "failed to load data/assets/ui/battle_location_marker.png\n");
	       return 1;
	  }
     }
     
     struct UI_MENU *menu_campaign_main_map = ui_create_menu(0,80,SCREEN_W, SCREEN_H - top_menu_y_offset,
							     1,true,main_map_background_bmp,0,0,0,NULL);
     if (!menu_campaign_main_map) {
	  fprintf(stderr, "failed to create menu_campaign_main_map menu struct\n");
	  return 1;
     }

     /* create FIGHT! button to launch into action */
     func.run = helper_initiate_battle; func.args = NULL;
     ui_create_action_element(10,10,"FIGHT!!!", text_font,
			      "Launch into the scenario!", hint_font,
			      &func, NULL, al_map_rgb(0,0,255),
			      menu_campaign_main_map);


     /* create locations and corresponding elements */
     func.run = helper_print_debug; func.args = NULL;
     ui_create_action_element(1*SCREEN_W/4,1*SCREEN_H/4,NULL,NULL,
			      "Starting location",hint_font,
			      &func,location_bmp,al_map_rgb(0,0,0),
			      menu_campaign_main_map);

     static int target_location_1 = FIRST_BATTLE_LOC;
     func.run = helper_move_player_to_location; func.args = &target_location_1;
     ui_create_action_element(2*SCREEN_W/4,1*SCREEN_H/4,NULL,NULL,
			      "First battle", hint_font,
			      &func,location_bmp,al_map_rgb(0,0,0),
			      menu_campaign_main_map);

     static int target_location_2 = SECOND_BATTLE_LOC;
     func.args = &target_location_2;
     ui_create_action_element(3*SCREEN_W/4,1*SCREEN_H/4,NULL,NULL,
			      "Second battle", hint_font,
			      &func,location_bmp,al_map_rgb(0,0,0),
			      menu_campaign_main_map);

     static int target_location_3 = FIRST_BOSS_LOC;
     func.args = &target_location_3;
     ui_create_action_element(2*SCREEN_W/4,2*SCREEN_H/4,NULL,NULL,
			      "First boss", hint_font,
			      &func,location_bmp,al_map_rgb(0,0,0),
			      menu_campaign_main_map);

     static int target_location_4 = FINAL_BOSS_LOC;
     func.args = &target_location_4;
     ui_create_action_element(1*SCREEN_W/4,2*SCREEN_H/4,NULL,NULL,
			      "Final boss", hint_font,
			      &func,location_bmp,al_map_rgb(0,0,0),
			      menu_campaign_main_map);

     /* construct location structure array */
     /* temporary for testing */
     campaign_locations[STARTING_LOC].id = STARTING_LOC;
     campaign_locations[STARTING_LOC].connected_location_ids[LEFT] = FIRST_BATTLE_LOC;
     campaign_locations[STARTING_LOC].connected_location_ids[RIGHT] = NO_LOCATION;
     campaign_locations[STARTING_LOC].x = 1*SCREEN_W/4 + al_get_bitmap_width(location_bmp) / 2;
     campaign_locations[STARTING_LOC].y = 1*SCREEN_H/4 + al_get_bitmap_height(location_bmp) / 2 +
	  top_menu_y_offset;
     campaign_locations[STARTING_LOC].name = "Starting location";
     campaign_locations[STARTING_LOC].description = NULL;
     
     campaign_locations[FIRST_BATTLE_LOC].id = FIRST_BATTLE_LOC;
     campaign_locations[FIRST_BATTLE_LOC].connected_location_ids[LEFT] = SECOND_BATTLE_LOC;
     campaign_locations[FIRST_BATTLE_LOC].connected_location_ids[RIGHT] = FIRST_BOSS_LOC;
     campaign_locations[FIRST_BATTLE_LOC].x = 2*SCREEN_W/4 + al_get_bitmap_width(location_bmp) / 2;
     campaign_locations[FIRST_BATTLE_LOC].y = 1*SCREEN_H/4 + al_get_bitmap_height(location_bmp) / 2 +
	  top_menu_y_offset;
     campaign_locations[FIRST_BATTLE_LOC].name = "First battle";
     campaign_locations[FIRST_BATTLE_LOC].description = NULL;

     campaign_locations[SECOND_BATTLE_LOC].id = SECOND_BATTLE_LOC;
     campaign_locations[SECOND_BATTLE_LOC].connected_location_ids[LEFT] = FIRST_BOSS_LOC;
     campaign_locations[SECOND_BATTLE_LOC].connected_location_ids[RIGHT] = NO_LOCATION;
     campaign_locations[SECOND_BATTLE_LOC].x = 3*SCREEN_W/4 + al_get_bitmap_width(location_bmp) / 2;
     campaign_locations[SECOND_BATTLE_LOC].y = 1*SCREEN_H/4 + al_get_bitmap_height(location_bmp) / 2 +
	  top_menu_y_offset;
     campaign_locations[SECOND_BATTLE_LOC].name = "Second battle";
     campaign_locations[SECOND_BATTLE_LOC].description = NULL;

     campaign_locations[FIRST_BOSS_LOC].id = FIRST_BOSS_LOC;
     campaign_locations[FIRST_BOSS_LOC].connected_location_ids[LEFT] = FINAL_BOSS_LOC;
     campaign_locations[FIRST_BOSS_LOC].connected_location_ids[RIGHT] = NO_LOCATION;
     campaign_locations[FIRST_BOSS_LOC].x = 2*SCREEN_W/4 + al_get_bitmap_width(location_bmp) / 2;
     campaign_locations[FIRST_BOSS_LOC].y = 2*SCREEN_H/4 + al_get_bitmap_height(location_bmp) / 2 +
	  top_menu_y_offset;
     campaign_locations[FIRST_BOSS_LOC].name = "First boss fight";
     campaign_locations[FIRST_BOSS_LOC].description = NULL;

     campaign_locations[FINAL_BOSS_LOC].id = FINAL_BOSS_LOC;
     campaign_locations[FINAL_BOSS_LOC].connected_location_ids[LEFT] = NO_LOCATION;
     campaign_locations[FINAL_BOSS_LOC].connected_location_ids[RIGHT] = NO_LOCATION;
     campaign_locations[FINAL_BOSS_LOC].x = 1*SCREEN_W/4 + al_get_bitmap_width(location_bmp) / 2;
     campaign_locations[FINAL_BOSS_LOC].y = 2*SCREEN_H/4 + al_get_bitmap_height(location_bmp) / 2 +
	  top_menu_y_offset;
     campaign_locations[FINAL_BOSS_LOC].name = "Final boss fight";
     campaign_locations[FINAL_BOSS_LOC].description = NULL;

     /* attach main map menu to ui */
     ui_attach_menu_to_interface(menu_campaign_main_map, campaign_ui);

     return 0;
}

/* draw a line between a location and its connections */
static void draw_location_connections(void)
{
     /* draw all connections */
     int i, j;

     for (i = 0; i < NUM_LOCATIONS; i++) {
	  for (j = 0; j < MAX_CONNECTIONS; j++) {
	       if (campaign_locations[i].connected_location_ids[j] > NO_LOCATION)
		    al_draw_line(campaign_locations[i].x,
				 campaign_locations[i].y,
				 campaign_locations[campaign_locations[i].connected_location_ids[j]].x,
				 campaign_locations[campaign_locations[i].connected_location_ids[j]].y,
				 al_map_rgb(255,0,0),
				 1.5f);

	  }
     }
     

     /* draw traveled connections */
     int *visited_location_ids = player_get_visited_location_ids();

     for (i = 0; i < (NUM_LOCATIONS - 1) * 2 && visited_location_ids[i] > NO_LOCATION; i += 2) {
	  al_draw_line(campaign_locations[visited_location_ids[i]].x,
		       campaign_locations[visited_location_ids[i]].y,
		       campaign_locations[visited_location_ids[i+1]].x,
		       campaign_locations[visited_location_ids[i+1]].y,
		       al_map_rgb(0,0,255),
		       2.0f);
     }

     return;
}

/*
  draw the player sprite, cycling through spritemap

  TODO change static sprite to dynamic
     maybe, it does look pretty good now
*/
static void draw_player_sprite(void)
{
     static int sprite_pos = 0, y_direction = 1;
     
     int loc = player_get_current_location();

     if (sprite_pos > 20)
	  y_direction = (-1);
     else if (sprite_pos < 2)
	  y_direction = 1;

     sprite_pos += y_direction * 2;
     
     al_draw_bitmap(player_get_sprite(),
		    campaign_locations[loc].x - (al_get_bitmap_width(player_get_sprite()) / 2),
		    campaign_locations[loc].y - al_get_bitmap_height(player_get_sprite()) -
		       sprite_pos,
		    ALLEGRO_FLIP_HORIZONTAL);

     return;
}
