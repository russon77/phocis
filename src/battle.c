/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>

#include "battle.h"
#include "player.h"
#include "overlay.h"
#include "environment.h"
#include "missiles.h"
#include "unit_behavior.h"
#include "waves.h"
#include "enemy.h"
#include "unit_values.h"
#include "menu_levelup.h"
#include "settings.h"
#include "audio.h"

int play_game(int enemy)
{
     bool refresh = false;
     float game_time = 0.0f;

     ENEMY_SOURCE = enemy;

     /* reset player values */
     reset_player_for_battle();

     /* initialize the background */
     if (init_background(container->display)) {
          fprintf(stderr, "FATAL ERROR failed to load background\n");
          return 1;
     }

     /* change font to overlay font */
     container->font = al_load_ttf_font(
          "data/fonts/ENDOR.ttf", 18, 0);
     if (!container->font) {
	  fprintf(stderr, "FATAL ERROR failed to load font\n");
	  return 1;
     }

     /* initialize overlay_resources */
     if (!overlay_load()) {
          fprintf(stderr, "FATAL ERROR: failed to load images\n");
          return 1;
     }

     /* get unit resources */
     if (unit_resources_load()) {
          fprintf(stderr, "FATAL ERROR: \
               failed to load unit resources\n");
          return 1;
     }

     /* init missile resources */
     if (missile_resources_load()) {
          fprintf(stderr, "FATAL ERROR: \
               failed to load missile resources\n");
          return 1;
     }

     /* initialize ai */
     if (enemy_load(enemy)) {
	  fprintf(stderr, "fatal error initiating enemy\n");
	  return 1;
     }

     /* start playing background music */
     start_background_music(BATTLE_BG);

     /* TODO FIXME
	make this cleaner/better/.refractorize */
     float fps = 0.0f;
     int frames = 0;
     ALLEGRO_EVENT_QUEUE *fps_queue = NULL;
     fps_queue = al_create_event_queue();
     if (!fps_queue) {
	  fprintf(stderr, "error creating fps event queue\n");
	  return 1;
     }

     ALLEGRO_TIMER *fps_timer = al_create_timer(1.0f);
     if (!fps_timer) {

	  fprintf(stderr, "failed to create fps timer\n");
	  return 1;
     }

     al_register_event_source(fps_queue, al_get_timer_event_source(fps_timer));

     al_start_timer(fps_timer);

     /* end of fps block */

     while (1) {

          ALLEGRO_EVENT ev;

          /* if refresh needs to happen, refresh screen */

          if (refresh && al_is_event_queue_empty(
                   container->event_queue)) {
               refresh = false;


               /* draw background */
               environment_display();

               /* draw overlay */
               overlay_display(container, fps);

               /* draw player
		  TODO */
	       

               /* draw soldiers on map */
               units_draw();

               /* draw missiles */
               missiles_draw();

               /* finally flip display */
               al_flip_display();
          }

	  /* check for fps updates */
	  if (al_get_next_event(fps_queue, &ev)) {
	       fps = (float) frames;
	       frames = 0;
	  }

	  /* process input / general events,

	     main game event handling */

          al_wait_for_event(container->event_queue, &ev);

          switch (ev.type) {
          case ALLEGRO_EVENT_TIMER:
               refresh = true;

	       /* frames per second counter */
	       frames++;

	       /* player cooldowns and mana */
	       player_lower_cooldowns(1.0f / 60.0f);
	       player_increase_mana(1.0 / 60.0);

	       /* local multiplayer cooldowns and mana */
	       if (ENEMY_SOURCE == ENEMY_LOCAL_MULT) {
		    enemy_lower_cooldowns(1.0f / 60.0f);
		    enemy_increase_mana(1.0f / 60.0f);
	       }
		    

	       /* check if wave is over */

	       if (wave_over) {

		    wave_over = false;

		    // skip level up if first wave
		    if (wave_number == 1) {
			 wave_number++;
			 continue;
		    }

		    game_paused = true;

		    if (menu_levelup(container)) {
			 fprintf(stderr, "FATAL ERROR exiting game...\n");
			 return 1;
		    }
		    
		    // restore mana
		    player_increase_mana(player_get_max_mana());

		    // reset cooldowns
		    player_lower_cooldowns_to_zero();

		    // destroy all units on screen in awesome explosion

		    // TODO
		    // display_explosion();

		    units_remove_all();

		    // destroy all missiles on screen
		    missiles_remove_all();

		    wave_number++;

		    game_paused = false;
	       }

	       // move units and process their attack functions
               units_move(game_time);

               units_attack();

               missiles_move();

	       // collision detection
               missiles_process_collisions();

               // run the garbage collectors, removing proper units/missiles
	       units_garbage_collector();

               missiles_garbage_collector();

	       // increase our game timer counter
	       game_time += (1.0 / FPS);
               
               break;

          case ALLEGRO_EVENT_DISPLAY_CLOSE:
	       al_destroy_timer(fps_timer);
	       al_destroy_event_queue(fps_queue);

               return 0;
          case ALLEGRO_EVENT_KEY_DOWN:
               switch(ev.keyboard.keycode) {
               case ALLEGRO_KEY_Q:
                    cast_spell(UNIT_ARCHER);

                    break;
               case ALLEGRO_KEY_W:
                    cast_spell(UNIT_SOLDIER);

                    break;
               case ALLEGRO_KEY_E:
                    cast_spell(UNIT_WIZARD);

                    break;
	       case ALLEGRO_KEY_P:

		    if (ENEMY_SOURCE == ENEMY_LOCAL_MULT)
			 enemy_summon_unit(UNIT_ARCHER);

		    break;
	       case ALLEGRO_KEY_O:

		    if (ENEMY_SOURCE == ENEMY_LOCAL_MULT)
			 enemy_summon_unit(UNIT_SOLDIER);

		    break;
	       case ALLEGRO_KEY_I:

		    if (ENEMY_SOURCE == ENEMY_LOCAL_MULT)
			 enemy_summon_unit(UNIT_WIZARD);

		    break;

               }


	  case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
	       overlay_process_mouse_click(ev.mouse.x, ev.mouse.y);
          }
     }

     return 0;
}

