/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <stdio.h>
#include <stdlib.h>

#include "waves.h"
#include "units.h"
#include "enemy.h"
#include "unit_values.h"
#include "player.h"
#include "campaign.h"

/* File-scoped functions */
static struct WAVE *waves_get_next_wave_from_file(void);
static struct WAVE *waves_get_next_wave_from_default_campaign(void);
static struct WAVE *waves_get_next_wave_from_custom_campaign(const char *);
     
/*
  File-scoped variables
*/
// default, starting wave number
int wave_number = 1;

// NOTE this is temporary, wave source should be specified
// in either command line option or gui settings
int wave_source = DEFAULT_WAVE_SOURCE;

/*
  Function definitions
*/

/*
  read the next wave source, construct and return wave structure
*/
struct WAVE *waves_get_next_wave(int source)
{
     switch (source) {
     case DEFAULT_WAVE_SOURCE:
	  return waves_get_next_wave_from_file();
     case CAMPAIGN_WAVE_SOURCE:
	  return waves_get_next_wave_from_default_campaign();
     }

     return NULL;
}

/*
  read next wave struct from the corresponding file

  TODO FIXME  only one file for this?
*/
static struct WAVE *waves_get_next_wave_from_file()
{

     // construct the file name
     // btw strings in C -- aka KILLMEPLZ
     char filename[sizeof("./data/levels/level999.txt")] = {0};

     sprintf(filename, "%s%03d.txt", "./data/levels/level", wave_number);

     // printf("%s\n", filename);
          
     // attempt to open file, catch failure
     FILE *fp = NULL;
     fp = fopen(filename, "r");

     if (!fp) {
	  fprintf(stderr, "error opening %s for reading wave\n", filename);

	  return NULL;
     }

     // construct wave: allocate wave structure
     struct WAVE *new_wave = NULL;
     new_wave = malloc(sizeof(struct WAVE));
     if (!new_wave) {
	  fprintf(stderr, "error allocating new wave structure in wave constructor\n");
	  return NULL;
     }
     

     // initial wave settings
     new_wave->units_array_capacity = 10;
     new_wave->units_array_size = 0;
     new_wave->units_array_pos = 0;

     // allocate integer units array
     new_wave->units_array = NULL;
     new_wave->units_array = malloc(new_wave->units_array_capacity * sizeof(int));
     if (!new_wave->units_array) {
	  fprintf(stderr, "error allocating units_array in wave constructor\n");
	  return NULL;
     }
     
     char tmp;
     int i = 0;

     // read in the unit information
     while ( fscanf(fp, "%c", &tmp) != EOF ) {
	  // implement dynamic array allocation
	  // if array is half filled, then double its capacity
	  if (i > (new_wave->units_array_capacity / 2)) {
	       new_wave->units_array_capacity *= 2;
	       new_wave->units_array = realloc(new_wave->units_array,
					       new_wave->units_array_capacity * sizeof(int));
	       if (!new_wave->units_array) {
		    fprintf(stderr, "error reallocating new_units_array in wave constructor\n");
		    return NULL;
	       }
	  }
	  // now enter unit into array
	  switch (tmp) {
	  case 'a':
	       // archer
	       new_wave->units_array[i++] = UNIT_ARCHER;
	       break;
	  case 'w':
	       // wizard
	       new_wave->units_array[i++] = UNIT_WIZARD;
	       break;
	  case 's':
	       // soldier
	       new_wave->units_array[i++] = UNIT_SOLDIER;
	       break;
	  }
     } ;

     // set the number of units in the array
     new_wave->units_array_size = i;

     // set up wave modifiers

     /* AI SUMMON TIME MODIFIER LOGIC */
     float tmp_stm = (1.0f) - ((float)(wave_number) * (0.1f));
     if (tmp_stm < AI_LOWEST_TIME_BETWEEN_SUMMONS_MODIFIER)
	  new_wave->wave_mods.ai_summon_time_modifier = AI_LOWEST_TIME_BETWEEN_SUMMONS_MODIFIER;
     else
	  new_wave->wave_mods.ai_summon_time_modifier = tmp_stm;

     // TODO implement other wave modifiers

     // finally close fp
     fclose(fp);

     return new_wave;

}

/*
  TODO map each campaign-location to appropriate wave
*/
static struct WAVE *waves_get_next_wave_from_default_campaign(void)
{
     static const char mappings[NUM_LOCATIONS][MAX_UNITS_PER_WAVE] =
     {
	  [FIRST_BATTLE_LOC] = "www",
	  [SECOND_BATTLE_LOC] = "aaa",
	  [FIRST_BOSS_LOC] = "sss",
	  [FINAL_BOSS_LOC] = "wawawa"
     } ;

     int loc = player_get_current_location();
     size_t wave_size = sizeof(mappings[loc]) / sizeof(mappings[loc][0]);

     // construct wave: allocate wave structure
     struct WAVE *new_wave = NULL;
     new_wave = malloc(sizeof(struct WAVE));
     if (!new_wave) {
	  fprintf(stderr, "error allocating new wave structure in wave constructor\n");
	  return NULL;
     }

     // initial wave settings
     new_wave->units_array_capacity = wave_size;
     new_wave->units_array_size = wave_size;
     new_wave->units_array_pos = 0;

     // allocate integer units array
     new_wave->units_array = NULL;
     new_wave->units_array = malloc(wave_size * sizeof(int));
     if (!new_wave->units_array) {
	  fprintf(stderr, "error allocating units_array in wave constructor\n");
	  return NULL;
     }

     int i;
     for (i = 0; i < wave_size; i++) {
	  switch (mappings[loc][i]) {
	  case 'a':
	       // archer
	       new_wave->units_array[i++] = UNIT_ARCHER;
	       break;
	  case 'w':
	       // wizard
	       new_wave->units_array[i++] = UNIT_WIZARD;
	       break;
	  case 's':
	       // soldier
	       new_wave->units_array[i++] = UNIT_SOLDIER;
	       break;
	  }
     }

     /* set up wave modifiers */

     /* AI SUMMON TIME MODIFIER LOGIC */
     float tmp_stm = (1.0f) - ((float)(wave_number) * (0.1f));
     if (tmp_stm < AI_LOWEST_TIME_BETWEEN_SUMMONS_MODIFIER)
	  new_wave->wave_mods.ai_summon_time_modifier = AI_LOWEST_TIME_BETWEEN_SUMMONS_MODIFIER;
     else
	  new_wave->wave_mods.ai_summon_time_modifier = tmp_stm;

/*     wave_number++; */

     return NULL;
}

/*
  reset the waves count,
   to be done at the start of every campaign/waves mode
*/

void waves_reset_count(void)
{
     wave_number = 1;
}

