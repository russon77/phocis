/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "units.h"
#include "unit_behavior.h"
#include "unit_values.h"
#include "player.h"
#include "enemy.h"
#include "settings.h"
#include "audio.h"

static void archer_fire(struct UNIT *);

static void wizard_fire(struct UNIT *);

static void soldier_attack(struct UNIT *);


void units_attack(void)
{

     struct UNIT *current = uh->head;

     while (current) {

          if (current->isaggrod &&
              (current->spritepos ==
               current->spritepos_attack_frame) &&
               !current->is_on_attack_cooldown) {

               current->is_on_attack_cooldown = true;

	       play_unit_sound(current, UNIT_ATTACK_SOUND);

               switch (current->unit_type) {
                    case UNIT_ARCHER:
                         archer_fire(current);
                         break;
                    case UNIT_SOLDIER:
                         soldier_attack(current);
                         break;
                    case UNIT_WIZARD:
                         wizard_fire(current);
                         break;
                    }
          }
          else if (current->spritepos !=
                   current->spritepos_attack_frame)
               current->is_on_attack_cooldown = false;
          
          current = current->next;
     }

     return;
}

static void archer_fire(struct UNIT *src)
{

     // find a target
     // TODO player can set archer behavior: closest target,
     //                                      furthest target,
     //                                      random target, etc.

     // archer should be within range if this is called,
     // otherwise segfault should occur if add_missile is
     // called with target NULL
     float closest_distance_yet = unit_values[UNIT_ARCHER].DEFAULT_AGGRO_RANGE;

     if (src->isplayerowned)
	  closest_distance_yet *= unit_values[UNIT_ARCHER].USER_AGGRO_RANGE_MOD;
     else
	  closest_distance_yet *= unit_values[UNIT_ARCHER].AI_AGGRO_RANGE_MOD;


     struct UNIT *closest_unit = NULL;

     struct UNIT *current = uh->head;

     while (current) {
	  // if `current` unit is closer than `closest-yet`,
	  // and `current` units are not on same `team`
          if (abs(current->xpos - src->xpos) <
              closest_distance_yet &&
              current->isplayerowned != src->isplayerowned) {

               closest_distance_yet =
                    abs(current->xpos - src->xpos);

               closest_unit = current;
          }

          current = current->next;
     }

     // attack closest unit,
     //        add arrow to missile list

     add_missile(MISSILE_ARROW, src->direction,
                 src->xpos, src->ypos,
                 closest_unit->xpos, closest_unit->ypos);


     return;
}

/*
  TODO
  FIXME
*/
static void wizard_fire(struct UNIT *src)
{
     // shoot in front
     // just for testing, definitely TODO
     // also TODO find highest density of targets

//     if (src->direction == DIRECTION_RIGHT)
          add_missile(MISSILE_SPELL, src->direction,
                      src->xpos, src->ypos,
                      640,
                      src->ypos);
/*   else
          add_missile(mh, mr, MISSILE_SPELL, src->direction,
                      src->xpos, src->ypos,
                      0.0f,
                      0.0f); */

     return;
}

static void soldier_attack(struct UNIT *src)
{
     // find closest unit (x coord)
     int closest_distance_yet = SCREEN_W;
     struct UNIT *closest_unit_previous = NULL;
     struct UNIT *closest_unit = NULL;

     struct UNIT *previous = NULL;
     struct UNIT *current = uh->head;

     while (current) {

          if (abs(current->xpos - src->xpos) <
              closest_distance_yet) {

               closest_distance_yet =
                    abs(current->xpos - src->xpos);

               closest_unit_previous = previous;
               closest_unit = current;
          }

          previous = current;
          current = current->next;
     }

     // attack closest unit,
     //        remove target unit if killed
     // TODO attack, death animations for struck targets

     if (closest_unit) {

	  float soldier_damage = unit_values[UNIT_SOLDIER].DEFAULT_DAMAGE;
	  if (src->isplayerowned)
	       soldier_damage *= unit_values[UNIT_SOLDIER].USER_DAMAGE_MOD;
	  else
	       soldier_damage *= unit_values[UNIT_SOLDIER].AI_DAMAGE_MOD;
	  
          if ((closest_unit->health -= unit_values[UNIT_SOLDIER].DEFAULT_DAMAGE) <
              0.0f) {

               if (closest_unit->isplayerowned)
		    player_lower_units_summoned();
               else
                    // lower ai count
                    enemy_lower_summoned_count();

               // delete `closest_unit`

               if (!closest_unit_previous) {
                    uh->head = uh->head->next;
                    free(closest_unit);
               }
               else {
                    closest_unit_previous->next =
                         closest_unit->next;
                    free(closest_unit);
               }

               // lower player summoned count

          }
     }
     
     return;
}
