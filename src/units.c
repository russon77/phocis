/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <time.h>
#include <allegro5/allegro.h>

#include "units.h"
#include "player.h"
#include "enemy.h"
#include "unit_values.h"
#include "settings.h"
#include "audio.h"

struct UNIT_RESOURCES *ur = NULL;
 struct UNIT_HEAD *uh = NULL;
 ALLEGRO_MUTEX *unit_summon_mutex = NULL;

/*
  UNIT constructor
*/

int add_unit(int owner, int unit_type)
{
//     printf("%s summoning unit... ", owner == AI_OWNED ? "ai" : "player");
     
     // lock the mutex
     al_lock_mutex(unit_summon_mutex);

     struct UNIT *new_unit = malloc(sizeof(struct UNIT));
     if (!new_unit) {
	  fprintf(stderr, "malloc error creating struct UNIT\n");
	  return 1;
     }

     // construct unit

     /**********************


       OWNERSHIP SPECIFIC VALUES


     ********************/
     if (owner == PLAYER_OWNED) {
          new_unit->xpos = -10.0f;
          new_unit->direction = DIRECTION_RIGHT;
          new_unit->isplayerowned = true;
          new_unit->sprites_mapping_array =
               &(ur->sprites_map_skeleton);

     }
     else {
          new_unit->xpos = SCREEN_W - 20.0f;
          new_unit->direction = DIRECTION_LEFT;
          new_unit->isplayerowned = false;
          new_unit->sprites_mapping_array =
               &(ur->sprites_map_human);

     }

     /**********************


       UNIT GENERAL VALUES


     ********************/

     // should be slightly random
     new_unit->ypos = (SCREEN_H * 45 / 100) +
	  rand()%100;

     new_unit->unit_type = unit_type;
     new_unit->isaggrod = false;


     new_unit->num_sprites_walking = 9;
     new_unit->num_sprites_total = 35;

     new_unit->spritepos = 0;
     new_unit->unit_timer = 0.0f;

     new_unit->is_on_attack_cooldown = false;

     switch (unit_type) {
	  /**********************


            UNIT SPECIFIC VALUES FOR SOLDIER


	  ********************/
     case UNIT_SOLDIER:

          new_unit->frame_time = 0.50f;
          
          new_unit->num_sprites_attack = 6;
          new_unit->num_sprites_attack_offset = 22;

          new_unit->spritepos_attack_frame = 25;

          if (new_unit->isplayerowned) {
               new_unit->spritesheet = 
                    ur->spritesheet_skeleton;
	       new_unit->health = 
		    new_unit->health_max = unit_values[UNIT_SOLDIER].DEFAULT_HEALTH *
		    unit_values[UNIT_SOLDIER].USER_HEALTH_MOD;

	  }
          else {
               new_unit->spritesheet =
                    ur->spritesheet_human;
	       new_unit->health = 
		    new_unit->health_max = unit_values[UNIT_SOLDIER].DEFAULT_HEALTH *
		    unit_values[UNIT_SOLDIER].AI_HEALTH_MOD;
	  }

          break;
	  /**********************


            UNIT SPECIFIC VALUES FOR ARCHER


	  ********************/
     case UNIT_ARCHER:

          new_unit->frame_time = 0.35;
          
          new_unit->num_sprites_attack = 13;
          new_unit->num_sprites_attack_offset = 9;

          new_unit->spritepos_attack_frame = 18;
          
          if (new_unit->isplayerowned) {
               new_unit->spritesheet =
                    ur->spritesheet_skeleton;
	       new_unit->health =
		    new_unit->health_max = unit_values[UNIT_ARCHER].DEFAULT_HEALTH *
		    unit_values[UNIT_ARCHER].USER_HEALTH_MOD;
	  }
          else {
               new_unit->spritesheet =
                    ur->spritesheet_human;
	       new_unit->health =
		    new_unit->health_max = unit_values[UNIT_ARCHER].DEFAULT_HEALTH *
		    unit_values[UNIT_ARCHER].AI_HEALTH_MOD;
	  }
          break;
	  /**********************


            UNIT SPECIFIC VALUES FOR WIZARD


	  ********************/
     case UNIT_WIZARD:

          new_unit->frame_time = 0.5f;
          
          new_unit->num_sprites_attack = 7;
          new_unit->num_sprites_attack_offset = 28;

          new_unit->spritepos_attack_frame = 32;
          
          if (new_unit->isplayerowned) {
               new_unit->spritesheet =
                    ur->spritesheet_skeleton;
	       new_unit->health =
		    new_unit->health_max = unit_values[UNIT_WIZARD].DEFAULT_HEALTH *
		    unit_values[UNIT_WIZARD].AI_HEALTH_MOD;
	  }
          else {
               new_unit->spritesheet =
                    ur->spritesheet_human;
	       new_unit->health =
		    new_unit->health_max = unit_values[UNIT_WIZARD].DEFAULT_HEALTH *
		    unit_values[UNIT_WIZARD].AI_HEALTH_MOD;
	  }

          break;
     }

     /*********

       Add unit to list in proper place

     ********/

     // edge case: list head is empty
     if (!uh->head) {

	  uh->head = new_unit;
	  uh->head->next = NULL;

     }
     else {
          // otherwise, add to proper place

	  // edge case: new unit goes before
	  // list head

	  if (uh->head->ypos > new_unit->ypos) {
	       // add in new unit before list head
	       new_unit->next = uh->head;
	       uh->head = new_unit;
	  }

	  else {
	       // otherwise need to step through list
	       // and find units place, or
	       // add to very end of list
	       
	       struct UNIT *current = uh->head;
	       while (current->next) {

		    if (current->next->ypos > new_unit->ypos) {
			 // unit belongs here
			 new_unit->next = current->next;
			 current->next = new_unit;


			 break;
		    }

		    current = current->next;
	       }

	       /* if end of list was reached, add to
		  end of list
	       */
	       if (!current->next && current->next != new_unit) {

		    current->next = new_unit;
		    new_unit->next = NULL;
	       }
	  }
     }

     // unlock the mutex
     al_unlock_mutex(unit_summon_mutex);

     return 0;
}

/*
  unit garbage collector

  find all units with no health and delete them
*/

void units_garbage_collector(void)
{
     struct UNIT *current = uh->head;
     struct UNIT *previous = NULL;

     while (current) {
          if (current->health < 0.0f) {
	       // play death sound
	       play_unit_sound(current, UNIT_DEATH_SOUND);

               // remove unit from list

	       // lower appropriate units summoned count
	       if (current->isplayerowned) {
		    player_lower_units_summoned();
	       }
	       else {
		    enemy_lower_summoned_count();
		    player_increase_bones();
	       }
               if (!previous) {
                    uh->head = current->next;
                    free(current);
                    current = uh->head;
                    continue;
               }
               else {
                    previous->next = current->next;
                    free(current);
                    current = previous->next;
                    continue;
               }
          }

          previous = current;
          current = current->next;
     }

     return;
}

/*
  movement routine: move each unit in DIRECTION,
  until in range of an enemy;
  then command UNIT to ATTACK  
*/
void units_move(float game_time)
{
     struct UNIT *current = uh->head;

     while (current) {

          current->unit_timer += (1.0 / 60.0);

          /* if there IS NOT an enemy in range, unit IS NOT aggrod,
             and should move forward, cycling through walking animation;
             otherwise unit IS AGGROD, and should cycle through
             attack animation
          */
          
          if (!(current->isaggrod =
                units_enemy_in_range(current))) {

	       float unit_movement = (10.0f)/(60.0f) *
		    unit_values[current->unit_type].MOVEMENT_MODIFIER;

               if (current->direction == DIRECTION_RIGHT) {
		    current->xpos += unit_movement;
	       }
               else { /* direction is left */
                    current->xpos -= unit_movement;
	       }

	       /* test for change of BACKGROUND MUSIC */

	       if (current->xpos > (SCREEN_W / 2) &&
		   current->direction == DIRECTION_RIGHT)

		    start_background_music(BATTLE_HALFWAY_GOOD_BG);

	       else if (current->xpos < (SCREEN_W / 2) &&
			current->direction == DIRECTION_LEFT)

		    start_background_music(BATTLE_HALFWAY_BAD_BG);

               // test for out of bounds
               if (current->xpos < -15.0f) {
                    // remove life from local player
		    player_lower_health(current->unit_type);
		    
                    // mark this unit to be deleted
                    current->health = (-1.0f);
               }
               else if (current->xpos > SCREEN_W) {
                    // remove life from ai
                    enemy_lower_health(current->unit_type);

                    // mark this unit to be deleted
		    current->health = (-1.0f);

		    // mark end of wave
		    force_new_wave = true;
               }

               current->spritepos =
                    (int)(current->unit_timer / current->frame_time) %
                    (current->num_sprites_walking - 1);
          }
          else { /* unit is AGGROD and should traverse attack animation */
               // set spritepos to next in attack animation

               current->spritepos =
                    (current->num_sprites_attack_offset) + (
                    (int)(current->unit_timer / current->frame_time) %
                    (current->num_sprites_attack));
          }
          
          // advance `current` pointer
          current = current->next;
     } ;

     return;
}

bool units_enemy_in_range(struct UNIT *unit)
{
     struct UNIT *current = uh->head;

     while (current) {
	  
          if (current->isplayerowned != unit->isplayerowned) {
	       if ( unit->isplayerowned && abs(unit->xpos - current->xpos) <
		    (unit_values[unit->unit_type].DEFAULT_AGGRO_RANGE *
			 unit_values[unit->unit_type].USER_AGGRO_RANGE_MOD))
		    return true;

	       else if ( !unit->isplayerowned && abs(unit->xpos - current->xpos) <
		    (unit_values[unit->unit_type].DEFAULT_AGGRO_RANGE *
			 unit_values[unit->unit_type].AI_AGGRO_RANGE_MOD))
		    return true;
          }

          // advance `current` pointer
          current = current->next;

     } ;

     return false;
}

/*
  drawing routine: draws individual health bars
*/

void units_draw_individual_health_bar(struct UNIT *unit)
{
     // don't print health bar if health is full
     if ((unit->health_max - unit->health) < 1.0f)
          return;
     
     // draw empty health bar
     al_draw_scaled_bitmap(ur->unit_healthbar_empty,
                           0, 0, 206, 28,
                           unit->xpos,
                           unit->ypos - 8,
                           50,
                           10,
                           0);

     // draw percent of full health bar

     float health_percent = (float)unit->health /
          (float)unit->health_max;
     al_draw_scaled_bitmap(ur->unit_healthbar_full,
                           0,0,
                           (float)206 * health_percent,
                           28,
                           unit->xpos,
                           unit->ypos - 8,
                           (float)50 * health_percent,
                           10,
                           0);
     
     return;
}

/*
  drawing routine: draws each unit in list to screen
*/
void units_draw(void)
{

     struct UNIT *current = uh->head;

     // for each unit
     while (current) {

          // draw each unit
          al_draw_bitmap_region(current->spritesheet,
                                (*current->sprites_mapping_array)[
                                     current->spritepos][
                                          SUB_SPRITE_X],
                                (*current->sprites_mapping_array)[
                                     current->spritepos][
                                          SUB_SPRITE_Y],
                                (*current->sprites_mapping_array)[
                                     current->spritepos][
                                          SUB_SPRITE_W],
                                (*current->sprites_mapping_array)[
                                     current->spritepos][
                                          SUB_SPRITE_H],
                                current->xpos,
                                current->ypos,
                                0);

          // draw unit health bar
          units_draw_individual_health_bar(current);
          
          // advance `current` pointer
          current = current->next;
     } ;

     return;
}

/*
  remove all units

  for use after each level, when game is over
*/
void units_remove_all(void)
{
     struct UNIT *current = uh->head;
     struct UNIT *prev = NULL;

     while (current) {
	  prev = current;
	  current = current->next;

	  free(prev);

	  prev = NULL;
     }

     uh->head = NULL;

     player_lower_units_summoned_to_zero();
     enemy_lower_units_summoned_to_zero();

     return;
}

/*
  load unit resources into struct and write sprite tables
*/
int unit_resources_load(void)
{
     ur = malloc(sizeof(struct UNIT_RESOURCES));
     if (!ur) {
          fprintf(stderr, "malloc unit resources\n");
          return 1;
     }

     uh = malloc(sizeof(struct UNIT_HEAD));
     if (!uh) {
	  fprintf(stderr, "malloc failed to allocate unit head\n");
	  return 1;
     }
     uh->head = NULL;

     /*
       initialize the mutex
     */
     unit_summon_mutex = al_create_mutex();
     if (!unit_summon_mutex) {
	  fprintf(stderr, "failed to create unit summon mutex\n");
	  return 1;
     }

     /* initiate random number generator */
     srand(time(NULL));
     
     /* write sprite map table */

     /* TODO make a loop for writing sprite map */

     ur->num_sprites_skeleton_walking = 9;
     ur->num_sprites_skeleton_archer_attack = 13;
     ur->num_sprites_skeleton_archer_attack_offset = 9;
     ur->num_sprites_skeleton_soldier_attack = 6;
     ur->num_sprites_skeleton_soldier_attack_offset = 22;
     ur->num_sprites_skeleton_wizard_attack = 7;
     ur->num_sprites_skeleton_wizard_attack_offset = 28;
     ur->num_sprites_skeleton_total = 35;

     /* SPRITE MAP FOR SKELETON WALKING */
     int i;
     for (i = 0; i < ur->num_sprites_skeleton_walking; i++) {
          ur->sprites_map_skeleton[i][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_skeleton[i][SUB_SPRITE_Y] = 192 + 4;
          ur->sprites_map_skeleton[i][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_skeleton[i][SUB_SPRITE_H] = 64 - 6;
     }


     /* SPRITE MAP FOR SKELETON ARCHER ATTACK */
     int j = ur->num_sprites_skeleton_archer_attack_offset;
     for (i = 0; i < ur->num_sprites_skeleton_archer_attack; i++) {
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_Y] = 1024 + 4;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_H] = 64 - 6;
     }
     /* SPRITE MAP FOR SKELETON SOLDIER ATTACK */
     j = ur->num_sprites_skeleton_soldier_attack_offset;
     for (i = 0; i < ur->num_sprites_skeleton_soldier_attack; i++) {
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_Y] = 448 + 4;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_H] = 64 - 6;
     }

     /* SPRITE MAP FOR SKELETON WIZARD ATTACK */
     j = ur->num_sprites_skeleton_wizard_attack_offset;
     for (i = 0; i < ur->num_sprites_skeleton_wizard_attack; i++) {
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_Y] = 704 + 4;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_skeleton[i+j][SUB_SPRITE_H] = 64 - 6;
     }
     

     /* load spritesheet */

     ur->spritesheet_skeleton = NULL;
     ur->spritesheet_skeleton =
          al_load_bitmap(
               "./data/assets/skeletons/skeleton_spritesheet.png");

     if (!ur->spritesheet_skeleton) {
          fprintf(stderr, "error loading skeleton_spritesheet.png\n");
          return 1;
     }

     /* HUMANS */

     ur->num_sprites_human_walking = 9;
     ur->num_sprites_human_archer_attack = 13;
     ur->num_sprites_human_archer_attack_offset = 9;
     ur->num_sprites_human_soldier_attack = 6;
     ur->num_sprites_human_soldier_attack_offset = 22;
     ur->num_sprites_human_wizard_attack = 7;
     ur->num_sprites_human_wizard_attack_offset = 28;
     ur->num_sprites_human_total = 35;

     /* SPRITE MAP FOR HUMAN WALKING */

     for (i = 0; i < ur->num_sprites_human_walking; i++) {
          ur->sprites_map_human[i][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_human[i][SUB_SPRITE_Y] = 576 + 4;
          ur->sprites_map_human[i][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_human[i][SUB_SPRITE_H] = 64 - 6;
     }

     /* SPRITE MAP FOR HUMAN ARCHER ATTACK */
     j = ur->num_sprites_human_archer_attack_offset;
     for (i = 0; i < ur->num_sprites_human_archer_attack; i++) {
          ur->sprites_map_human[i+j][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_human[i+j][SUB_SPRITE_Y] = 1088 + 4;
          ur->sprites_map_human[i+j][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_human[i+j][SUB_SPRITE_H] = 64 - 6;
     }

     /* SPRITE MAP FOR HUMAN SOLDIER ATTACK */
     j = ur->num_sprites_human_soldier_attack_offset;
     for (i = 0; i < ur->num_sprites_human_soldier_attack; i++) {
          ur->sprites_map_human[i+j][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_human[i+j][SUB_SPRITE_Y] = 832 + 4;
          ur->sprites_map_human[i+j][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_human[i+j][SUB_SPRITE_H] = 64 - 6;
     }

     /* SPRITE MAP FOR HUMAN WIZARD ATTACK */
     j = ur->num_sprites_human_wizard_attack_offset;
     for (i = 0; i < ur->num_sprites_human_wizard_attack; i++) {
          ur->sprites_map_human[i+j][SUB_SPRITE_X] = (64 * i) + 4;
          ur->sprites_map_human[i+j][SUB_SPRITE_Y] = 64 + 4;
          ur->sprites_map_human[i+j][SUB_SPRITE_W] = 64 - 6;
          ur->sprites_map_human[i+j][SUB_SPRITE_H] = 64 - 6;
     }
     

     /* load spritesheet */

     ur->spritesheet_human = NULL;
     ur->spritesheet_human =
          al_load_bitmap(
               "./data/assets/humans/human_spritesheet.png");

     if (!ur->spritesheet_human) {
          fprintf(stderr, "error loading human_spritesheet.png\n");
          return 1;
     }

     /* load unit healthbar bitmaps */

     ur->unit_healthbar_empty = NULL;
     ur->unit_healthbar_empty =
          al_load_bitmap("./data/assets/lifebars/EmptyBar.png");

     if (!ur->unit_healthbar_empty) {
          fprintf(stderr, "error loading emptybar.png\n");
          return 1;
     }

     ur->unit_healthbar_full = NULL;
     ur->unit_healthbar_full =
          al_load_bitmap("./data/assets/lifebars/RedBar.png");
     
     if (!ur->unit_healthbar_full) {
          fprintf(stderr, "error loading RedBar.png\n");
          return 0;
     }

     return 0;

}
