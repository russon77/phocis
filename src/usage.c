/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>

#include "usage.h"

/*
  print out usage for command line args

  should be called for '-h' or '--help',
    or if  arguments are invalid
*/
void usage(char *name)
{
     printf("usage: %s [arg] [value]...\n", name);

     printf("\n \
-a : turn off audio (on by default) (NOT IMPLEMENTED)\n \
-c : specify custom config file\n \
-d : set ai difficulty (NOT IMPLEMENTED)\n \
-h : print out this help dialog\n \
-l : show copying information\n \
-m : force multiplayer <l | local> | < <r | remote> <portno> | <host portno> >\n \
         this implicitly implements \"-s\" option\n \
-s : skip intro splash (NOT IMPLEMENTED)\n \
-v : print out current version of program\n \
-w : show warranty\n \
-x : set window width\n \
-y : set window height\n\n\n");
}

/*
  print out copying information
*/
void copying_info(void)
{
     printf("Copyright (C) 2014  Tristan Kernan \n \
    This program is free software: you can redistribute it and/or modify \n \
    it under the terms of the GNU General Public License as published by \n \
    the Free Software Foundation, either version 3 of the License, or \n \
    (at your option) any later version. \n\n");
     printf("This program is distributed in the hope that it will be useful, \n \
    but WITHOUT ANY WARRANTY; without even the implied warranty of \n \
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the \n \
    GNU General Public License for more details. \n\n \
    You should have received a copy of the GNU General Public License \n \
    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n\n");
     
     return;
}

/*
  print out warranty info
*/
void warranty_info(void)
{
     printf("THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED \n \
BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING \n \
THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM\n \
“AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,\n");
     printf("INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\n \
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. \n \
THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM\n \
IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, \n \
YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n\n");
     return;
}
