/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __linux__
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

#ifdef _WIN32
#include <winsock.h>
#endif

#include "multiplayer.h"

int remote_fd;

int mult_set_fd(int fd) {
     if (fd < 0)
	  return 1;
     
     remote_fd = fd;

     return 0;
}

/*
@start_server
@args: portno (int)
@return: file descriptor (int)
*/
int start_server(int portno)
{
     int sockfd, newsockfd;

     struct sockaddr_in serv_addr, cli_addr;

#ifdef __linux__
     socklen_t clilen;
#endif

#ifdef _WIN32
     size_t clilen;
     WSADATA wsaData;

     if (WSAStartup(MAKEWORD(2,0), &wsaData)) {
	  fprintf(stderr, "wsastartup() failed\n");
	  return -1;
     }
#endif

     sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

     if ( sockfd < 0 ) {
	  fprintf( stderr, "ERROR opening socket\n");
	  return -1;
     }

     memset(&serv_addr, 0, sizeof(serv_addr));

     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons( portno );

     if ( bind( sockfd, ( struct sockaddr * ) &serv_addr, sizeof( serv_addr ) ) < 0 ) {
	  fprintf( stderr, "ERROR on binding\n");
	  return -1;
     }

     listen(sockfd, 5);

     clilen = sizeof( cli_addr );

     printf("Waiting for someone to connect...\n");

     newsockfd = accept( sockfd, ( struct sockaddr * ) &cli_addr, &clilen );

     return newsockfd;
}


/*
@connect_to_host
@args: IPv4 destination (char *), portno (int)
@return: file descriptor (int)
*/
int connect_to_host(char *addr, int portno)
{
     int sockfd;
     struct sockaddr_in serv_addr;
     struct hostent *server;
     
#ifdef _WIN32
     WSADATA wsaData;
     if (WSAStartup(MAKEWORD(2,0), &wsaData)) {
	  fprintf(stderr, "wsastartup() failed\n");
	  return -1;
     }
#endif

     /* connect to server */
     sockfd = socket(AF_INET, SOCK_STREAM, 0);

     if ( sockfd < 0 ) {
	  fprintf( stderr, "ERROR opening socket\n");
	  return -1;
     }

     server = gethostbyname( addr );
     if ( server == NULL ) {
	  fprintf( stderr, "ERROR no such host\n");
	  return -1;
     }


     memset(&serv_addr, 0, sizeof(serv_addr));

     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = inet_addr(addr);
     serv_addr.sin_port = htons( portno );

     printf("Connecting to host...\n");

     if ( connect(sockfd, ( struct sockaddr * ) &serv_addr, sizeof( serv_addr ) ) < 0 ) {
	  fprintf( stderr, "ERROR connecting\n");
	  return -1;
     }

     printf("Connected!\n");

     return sockfd;
}
