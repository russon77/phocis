/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <string.h>
#include <allegro5/allegro.h>

#include "settings.h"
#include "unit_values.h"
#include "usage.h"
#include "enemy.h"
#include "multiplayer.h"

/*
  External variable initiation
*/
float FPS = 0.0f;
bool FULLSCREEN_TOGGLE = false;
int SCREEN_W = 0;
int SCREEN_H = 0;

/*
  Local variable initiation
*/

bool settings_override[6] = {false, false, false,
			     false, false, false};

/* Variables suffixed with 'or' specify
   override values */
  
char *config_file_or = NULL;
char *config_file_default = "./data/settings.cfg";

int screen_dimensions_or[2] = {0,0};

char *prog_name = NULL;


/*
  Function prototypes
*/

void load_default_settings(bool);
int load_command_line_arguments(int, char**);
int load_config_file_settings(ALLEGRO_CONFIG *);
ALLEGRO_CONFIG *create_new_config_file(void);

/*
  Function definitions
*/

/*
  Attempt to read settings from arguments,
   then attempt to read from config file,
   finally falling back to a default config
   internally stored
*/
int settings_load(int argc, char* argv[])
{
     if (argc > 1) {
	  prog_name = argv[0];
          if (load_command_line_arguments(argc - 1, &argv[1]))
	       return 1;
     }

     // load the configuration file
     ALLEGRO_CONFIG *cfg = NULL;
     if (settings_override[CONFIG_FILE])
	  cfg = al_load_config_file(config_file_or);
     else
	  cfg = al_load_config_file(config_file_default);

     if (cfg) {

	  if (load_config_file_settings(cfg)) {
	       fprintf(stderr, "Configuration file misconfigured. \
                       Please refer to the documentation for proper config file writing.\n");
	       fprintf(stderr, "Falling back to default config...\n");

	       load_default_settings(true);
	  }

	  else {
	       printf("Successfully loaded configuration file\n");
	  }

	  al_destroy_config(cfg);
     }

     else {
	  // fall back to most basic settings
	  load_default_settings(false);
     }

     // write in override variables

     if (settings_override[WIN_HEIGHT])
	  SCREEN_H = screen_dimensions_or[WIN_HEIGHT];

     if (settings_override[WIN_WIDTH])
	  SCREEN_W = screen_dimensions_or[WIN_WIDTH];

     /* to be implemented */

     if (settings_override[AI_DIFF])
	  ;

     if (settings_override[SKIP_INTRO])
	  ;

     if (settings_override[AUDIO_DISABLE])
	  ;
	

     return 0;
}

int load_config_file_settings(ALLEGRO_CONFIG *cfg)
{
     if (!cfg) {
	  fprintf(stderr, "error attempting to load config file from NULL\n");
	  return 1;
     }

     FPS = atof(al_get_config_value(cfg, "video", "fps"));
     if (FPS < 0.01f)
          return 1;
     SCREEN_W = atoi(al_get_config_value(cfg, "video", "width"));
     if (!SCREEN_W)
          return 1;
     SCREEN_H = atoi(al_get_config_value(cfg, "video", "height"));
     if (!SCREEN_H)
          return 1;

     // load other config values
     unit_values_load(cfg);
     
     return 0;
}

/*
  Recursively evaluate arguments list,
   appropriately resolving each
*/
int load_command_line_arguments(int argc, char *argv[])
{
     // base case
     if (argc <= 0)
	  return 0;

     // otherwise, attempt to parse first option

     if (!strcmp(argv[0], "-h")) {
	  usage(prog_name);

	  return 1;
     }

     if (!strcmp(argv[0], "-x")) {
	  if (argc < 2) {
	       usage(prog_name);
	       return 1;
	  }

	  settings_override[WIN_WIDTH] = true;
	  screen_dimensions_or[WIN_WIDTH] = atoi(argv[1]);

	  return load_command_line_arguments(argc - 2, &argv[2]);

     }

     if (!strcmp(argv[0], "-y")) {
	  if (argc < 2) {
	       usage(prog_name);
	       return 1;
	  }

	  settings_override[WIN_HEIGHT] = true;
	  screen_dimensions_or[WIN_HEIGHT] = atoi(argv[1]);

	  return load_command_line_arguments(argc - 2, &argv[2]);
     }

     if (!strcmp(argv[0], "-a")) {
	  settings_override[AUDIO_DISABLE] = true;

	  return load_command_line_arguments(argc - 1, &argv[1]);
     }

     if (!strcmp(argv[0], "-c")) {
	  if (argc < 2) {
	       usage(prog_name);
	       return 1;
	  }

	  settings_override[CONFIG_FILE] = true;
	  config_file_or = argv[1];

	  return load_command_line_arguments(argc - 2, &argv[2]);
     }

     /* handle multiplayer settings */
     if (!strcmp(argv[0], "-m")) {
	  if (argc < 2) {
	       usage(prog_name);
	       return 1;
	  }

	  // set skip introduction menu and jump right into game param
	  settings_override[SKIP_INTRO] = true;

	  if (!strcmp(argv[1], "l") || !strcmp(argv[1], "local")) {
	       ENEMY_SOURCE = ENEMY_LOCAL_MULT;
	  }

	  else if (!strcmp(argv[1], "r") || !strcmp(argv[1], "remote")) {
	       
	       if (argc == 3) {
		    // hosting a server locally

		    // start server (port)
		    if (mult_set_fd(start_server(atoi(argv[2])))) {
			 usage(prog_name);
			 return 1;
		    }

		    ENEMY_SOURCE = ENEMY_REMOTE_MULT;

		    return load_command_line_arguments(argc - 3, &argv[3]);

	       }

	       else {
		    // connecting to a remote server
		    if (mult_set_fd(connect_to_host(argv[2], atoi(argv[3])))) {
			 usage(prog_name);
			 return 1;
		    }

		    ENEMY_SOURCE = ENEMY_REMOTE_MULT;

		    return load_command_line_arguments(argc - 4, &argv[4]);
		    
	       }


	  }

	  return load_command_line_arguments(argc - 2, &argv[2]);
     }

     // TODO
     if (!strcmp(argv[0], "-d")) {
	  if (argc < 2) {
	       usage(prog_name);
	       return 1;
	  }

	  settings_override[AI_DIFF] = true;

	  return load_command_line_arguments(argc - 2, &argv[2]);
     }

     // TODO
     if (!strcmp(argv[0], "-l")) {
	  // print out copying information / licensing
	  copying_info();

	  return load_command_line_arguments(argc - 1, &argv[1]);
     }

     // TODO
     if (!strcmp(argv[0], "-s")) {
	  settings_override[SKIP_INTRO] = true;

	  return load_command_line_arguments(argc - 1, &argv[1]);
     }

     // TODO
     if (!strcmp(argv[0], "-v")) {
	  printf("phocis version 0.1\n");

	  return load_command_line_arguments(argc - 1, &argv[1]);
     }

     // TODO
     if (!strcmp(argv[0], "-w")) {
	  // print out warranty information
	  warranty_info();

	  return load_command_line_arguments(argc - 1, &argv[1]);
     }     

     // control reaches end, meaning option is invalid
     usage(prog_name);
     return 1;
}

void load_default_settings(bool config_file_exists)
{
     if (config_file_exists) {
	  // config file is in correct location but
	  // misconfigured; load default values
	  // and do not overwrite it
	  FPS = 60.0f;
	  SCREEN_W = 640;
	  SCREEN_H = 480;

	  unit_values_load(NULL);
     
     }
     else {
	  // config file does not exist,
	  // create it, save it, and load it
	  load_config_file_settings(create_new_config_file());
     }
     
     return;
}

ALLEGRO_CONFIG *create_new_config_file(void)
{
     ALLEGRO_CONFIG *cfg = al_create_config();
     if (!cfg) {
	  fprintf(stderr, "failed to create new allegro config\n");
	  return NULL;
     }

     fprintf(stdout, "creating new config file...\n");

     /*
       Not fun part where every value is written
     */
     
     al_set_config_value(cfg, "video", "fps", "60.0");
     al_set_config_value(cfg, "video", "height", "640.0");
     al_set_config_value(cfg, "video", "width", "480.0");

     /*********

       SOLDIER VALUES

     *********/ 
     
     al_set_config_value(cfg, "unit soldier", "cooldown default", "4.0");
     al_set_config_value(cfg, "unit soldier", "cooldown user mod", "1.0");
     al_set_config_value(cfg, "unit soldier", "cooldown ai mod", "1.0");

     al_set_config_value(cfg, "unit soldier", "mana default", "4.0");
     al_set_config_value(cfg, "unit soldier", "mana user mod", "1.0");
     al_set_config_value(cfg, "unit soldier", "mana ai mod", "1.0");

     al_set_config_value(cfg, "unit soldier", "health default", "1000.0");
     al_set_config_value(cfg, "unit soldier", "health user mod", "1.0");
     al_set_config_value(cfg, "unit soldier", "health ai mod", "1.0");

     al_set_config_value(cfg, "unit soldier", "damage default", "120.0");
     al_set_config_value(cfg, "unit soldier", "damage user mod", "1.0");
     al_set_config_value(cfg, "unit soldier", "damage ai mod", "1.0");

     al_set_config_value(cfg, "unit soldier", "aggro range default", "20.0");
     al_set_config_value(cfg, "unit soldier", "aggro range user mod", "1.0");
     al_set_config_value(cfg, "unit soldier", "aggro range ai mod", "1.0");

     al_set_config_value(cfg, "unit soldier", "defense default", "25.0");
     al_set_config_value(cfg, "unit soldier", "defense user mod", "1.0");
     al_set_config_value(cfg, "unit soldier", "defense ai mod", "1.0");

     al_set_config_value(cfg, "unit soldier", "movement mod", "1.0");

     /*********

       ARCHER VALUES

     *********/      

     al_set_config_value(cfg, "unit archer", "cooldown default", "7.0");
     al_set_config_value(cfg, "unit archer", "cooldown user mod", "1.0");
     al_set_config_value(cfg, "unit archer", "cooldown ai mod", "1.0");

     al_set_config_value(cfg, "unit archer", "mana default", "7.0");
     al_set_config_value(cfg, "unit archer", "mana user mod", "1.0");
     al_set_config_value(cfg, "unit archer", "mana ai mod", "1.0");

     al_set_config_value(cfg, "unit archer", "health default", "400.0");
     al_set_config_value(cfg, "unit archer", "health user mod", "1.0");
     al_set_config_value(cfg, "unit archer", "health ai mod", "1.0");

     al_set_config_value(cfg, "unit archer", "damage default", "80.0");
     al_set_config_value(cfg, "unit archer", "damage user mod", "1.0");
     al_set_config_value(cfg, "unit archer", "damage ai mod", "1.0");

     al_set_config_value(cfg, "unit archer", "aggro range default", "0.6667");
     al_set_config_value(cfg, "unit archer", "aggro range user mod", "1.0");
     al_set_config_value(cfg, "unit archer", "aggro range ai mod", "1.0");

     al_set_config_value(cfg, "unit archer", "defense default", "10.0");
     al_set_config_value(cfg, "unit archer", "defense user mod", "1.0");
     al_set_config_value(cfg, "unit archer", "defense ai mod", "1.0");

     al_set_config_value(cfg, "unit archer", "movement mod", "1.2");
     
     /*********

       WIZARD VALUES

     *********/      

     al_set_config_value(cfg, "unit wizard", "cooldown default", "10.0");
     al_set_config_value(cfg, "unit wizard", "cooldown user mod", "1.0");
     al_set_config_value(cfg, "unit wizard", "cooldown ai mod", "1.0");

     al_set_config_value(cfg, "unit wizard", "mana default", "10.0");
     al_set_config_value(cfg, "unit wizard", "mana user mod", "1.0");
     al_set_config_value(cfg, "unit wizard", "mana ai mod", "1.0");

     al_set_config_value(cfg, "unit wizard", "health default", "300.0");
     al_set_config_value(cfg, "unit wizard", "health user mod", "1.0");
     al_set_config_value(cfg, "unit wizard", "health ai mod", "1.0");

     al_set_config_value(cfg, "unit wizard", "damage default", "10.0");
     al_set_config_value(cfg, "unit wizard", "damage user mod", "1.0");
     al_set_config_value(cfg, "unit wizard", "damage ai mod", "1.0");

     al_set_config_value(cfg, "unit wizard", "aggro range default", "0.25");
     al_set_config_value(cfg, "unit wizard", "aggro range user mod", "1.0");
     al_set_config_value(cfg, "unit wizard", "aggro range ai mod", "1.0");

     al_set_config_value(cfg, "unit wizard", "defense default", "2.0");
     al_set_config_value(cfg, "unit wizard", "defense user mod", "1.0");
     al_set_config_value(cfg, "unit wizard", "defense ai mod", "1.0");

     al_set_config_value(cfg, "unit wizard", "movement mod", "0.9");     

     if (!al_save_config_file("./data/settings.cfg", cfg)) {
	  fprintf(stderr, "error writing new config file to disk\n");
	  return NULL;
     }

     return cfg;
}
