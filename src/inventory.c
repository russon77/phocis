/* inventory provides an interactive menu for players
   to inspect the items they have bought/found */

#include <stdio.h>
#include <allegro5/allegro.h>

#include "inventory.h"
#include "main.h"
#include "settings.h"
#include "audio.h"
#include "player.h"
#include "items.h"

#include "ui_builder/ui_builder.h"
#include "ui_builder/menus.h"
#include "ui_builder/elements.h"

static int inventory_init(void);

static struct USER_INTERFACE *inventory_ui = NULL;

int inventory_run(void)
{
     bool redraw = false;

     if (inventory_init()) {
	  fprintf(stderr, "inventory init failed!\n");
	  return 1;
     }

     while (1) {
	  ALLEGRO_EVENT ev;
	  al_wait_for_event(container->event_queue, &ev);

	  switch (ev.type) {
	  case ALLEGRO_EVENT_TIMER:

	       redraw = true;

	       break;
	  case ALLEGRO_EVENT_DISPLAY_CLOSE:

	       return 0;

	  case ALLEGRO_EVENT_MOUSE_AXES:
	  case ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY:
	  case ALLEGRO_EVENT_MOUSE_BUTTON_UP:

	       ui_process_mouse_event(inventory_ui, &ev);

	       break;
	  }

	  if (redraw && al_is_event_queue_empty(
		   container->event_queue)) {

	       /* check and enforce proper background music */
	       if (audio_get_current_background_music_id() !=
		   MAIN_MENU_BG)
		    start_background_music(INVENTORY_BG);

	       redraw = false;

	       al_clear_to_color(al_map_rgb(0,0,0));

	       /* draw user interface */
	       ui_display_interface(inventory_ui);

	       al_flip_display();
	  }
     } ;
     
     return 0;
}

static void helper_return(void *args)
{
     ALLEGRO_EVENT ev;
     ev.type = ALLEGRO_EVENT_DISPLAY_CLOSE;
     al_emit_user_event(&(container->event_source),
			&ev, NULL);
}

/*
  construct user interface, load default values into
     items array, and some other cool things i bet

  user interface will be dynamic!!! upon each load,
     check to see if an item was purchased and if it was,
     instantiate the corresponding element
*/
static int inventory_init(void)
{
     ALLEGRO_FONT *text_font, *hint_font;
     ALLEGRO_BITMAP *shelf_background;
     
     /* if inventory already exists and init is called again,
	destroy interface */
     if (inventory_ui) {
	  /* something maybe later*/
	  ui_destroy_user_interface(inventory_ui);
	  inventory_ui = NULL;
     }

     /* load fonts */
     text_font = al_load_font("./data/fonts/ENDOR.ttf", 24, 0);
     hint_font = al_load_font("./data/fonts/ENDOR.ttf", 18, 0);

     if (!text_font || !hint_font) {
	  fprintf(stderr, "failed to load data/fonts/ENDOR.ttf\n");
	  return 1;
     }

     /* draw the shelf background image */
     shelf_background = al_create_bitmap(SCREEN_W/2,100);
     al_set_target_bitmap(shelf_background);
     al_clear_to_color(al_map_rgb(160,82,45));
     al_set_target_bitmap(al_get_backbuffer(container->display));

     inventory_ui = ui_create_interface(0,0,SCREEN_W,SCREEN_H,1);
     if (!inventory_ui) {
	  fprintf(stderr, "failed to create inventory ui structure\n");
	  return 1;
     }

     /* holds only back button for now
	btw how that work o.O */
     struct UI_MENU *menu_operations = ui_create_menu(10,10,SCREEN_W/4,100,1,true,
						      NULL,0,0,0,NULL);

     if (!menu_operations) {
	  fprintf(stderr, "failed to create menu_operations menu structure\n");
	  return 1;
     }

     struct UI_MENU *menu_top_shelf = ui_create_menu(SCREEN_W/4,SCREEN_H/3,
						     SCREEN_W/2,100,1,true,
						     shelf_background,
						     0,LIST_HORIZONTAL,0,NULL);

     if (!menu_top_shelf) {
	  fprintf(stderr, "failed to create menu_top_shelf menu structure\n");
	  return 1;
     }

     struct UI_MENU *menu_bottom_shelf = ui_create_menu(SCREEN_W/4,SCREEN_H*2/3,
							SCREEN_W/2,100,1,true,
							shelf_background,
							0,LIST_HORIZONTAL,0,NULL);

     if (!menu_bottom_shelf) {
	  fprintf(stderr, "failed to create menu_bottom_shelf menu structure\n");
	  return 1;
     }

     struct UI_ELEMENT_FUNC runnable = {helper_return, NULL};

     /* create elements */
     /* each item will only be created here IFF player owns item */
     int *player_held_item_ids = player_get_held_items_id_list();
     int player_num_items = player_get_num_items();

     int i, num_items = items_get_num_items();
     struct ITEM *item;

     for (i = 0; i < player_num_items; i++) {
	  item = items_get_from_id(player_held_item_ids[i]);
	  if (!item) {
	       /* shouldn't happen except in testing,
		  but better to catch it anyway */
	       continue;
	  }

	  if (i > (num_items / 2)) {
	       /* draw on bottom shelf */
	       ui_create_simple_display_element(0,0,
						NULL,NULL,
						item->description, hint_font,
						item->image, al_map_rgb(255,0,0),
						menu_bottom_shelf);
	  }

	  else {
	       /* draw on top shelf */	       
	       ui_create_simple_display_element(0,0,
						NULL,NULL,
						item->description, hint_font,
						item->image, al_map_rgb(255,0,0),
						menu_top_shelf);

	  }
     }
     

     /* create operations elements : for now only back button */
     ui_create_action_element(0,0,"Back",text_font,
			      "Return to the campaign map", hint_font,
			      &runnable, NULL, al_map_rgb(128,128,128),
			      menu_operations);


     /* align menus */
     ui_align_menu(menu_top_shelf);
     ui_align_menu(menu_bottom_shelf);

     ui_attach_menu_to_interface(menu_top_shelf, inventory_ui);
     ui_attach_menu_to_interface(menu_bottom_shelf, inventory_ui);
     ui_attach_menu_to_interface(menu_operations, inventory_ui);
     

     return 0;
}
