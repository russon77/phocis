/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "main.h"
#include "player.h"
#include "main_menu.h"
#include "battle.h"
#include "settings.h"
#include "audio.h"
#include "enemy.h"

#include "ui_builder/ui_builder.h"

/*
  function prototypes
*/

static int main_helper_load_all(void);
static void main_helper_exit_all(void);

/*
  extern variable initiation/declaration
*/
struct ALLEGRO_RESOURCES *container;

/*
  main function

  @return: 0 upon success
*/
int main(int argc, char* argv[])
{

     // call all init functions
     // TODO move resource loading to here
     // TODO add a splash screen
     if (settings_load(argc, argv)) {
	  fprintf(stderr, "game unable to start! exiting...\n");
	  return 1;
     }

     if (main_helper_load_all()) {
          return 1;
     }

     if (ui_init_all(container->display)) {
	  fprintf(stderr, "failed to initiate ui builder\n");
	  return 1;
     }

     audio_load();

     al_clear_to_color(al_map_rgb(0,0,0));

     al_flip_display();

     al_start_timer(container->fps_timer);

     main_menu();

     main_helper_exit_all();

     return 0;
}

/*
  small function to run al_destroy_* functions in bulk,
  and run al_uninstall_* functions
*/
static void main_helper_exit_all(void)
{
     al_uninstall_keyboard();
     al_uninstall_mouse();
     al_uninstall_audio();

     al_shutdown_font_addon();
     al_shutdown_image_addon();

     al_destroy_user_event_source(&(container->event_source));
     al_destroy_timer(container->fps_timer);
     al_destroy_display(container->display);
     al_destroy_event_queue(container->event_queue);

     return;
}

/*
  small function to run initializion functions for
  allegro

  @return: 0 upon failure, 1 upon success
*/
static int main_helper_load_all(void)
{
     container = malloc(sizeof(struct ALLEGRO_RESOURCES));
     if (!container) {
	  fprintf(stderr, "fatal malloc failed struct al resources\n");
	  return 1;
     }
     if (!al_init()) {
          fprintf(stderr, "failed to initialize allegro!\n");
          return 1;
     }

     al_init_font_addon();
     al_init_ttf_addon();

     container->font = al_load_ttf_font(
          "data/fonts/SamuraiBlade.ttf", 36, 0);
     if (!container->font) {
          fprintf(stderr, "failed to load font: SamuraiBlade.ttf\n");
          return 1;
     }

     if (!al_init_image_addon()) {
          fprintf(stderr, "failed to initialize image addon\n");
          return 1;
     }

     if (!al_install_keyboard()) {
          fprintf(stderr, "failed to initialize keyboard!\n");
          return 1;
     }

     if (!al_install_mouse()) {
          fprintf(stderr, "failed to initialize the mouse!\n");
          return 1;
     }

     al_init_user_event_source(&(container->event_source));

     container->fps_timer = al_create_timer(1.0 / FPS);
     if (!container->fps_timer) {
          fprintf(stderr, "failed to initialize timer!\n");
          return 1;
     }

     if (FULLSCREEN_TOGGLE) {
	  ALLEGRO_DISPLAY_MODE disp_data;

	  al_get_display_mode(0, &disp_data);

	  al_set_new_display_flags(ALLEGRO_FULLSCREEN);

	  container->display = al_create_display(disp_data.width, disp_data.height);

	  SCREEN_W = disp_data.width;
	  SCREEN_H = disp_data.height;

	  if (!container->display) {
	       fprintf(stderr, "failed to create display!\n");
	       al_destroy_timer(container->fps_timer);
	       return 1;
	  }
     }

     else {
	  ALLEGRO_BITMAP *icon = al_load_bitmap("icon.png");

	  container->display = al_create_display(SCREEN_W, SCREEN_H);

	  if (!container->display) {
	       fprintf(stderr, "failed to create display!\n");
	       al_destroy_timer(container->fps_timer);
	       return 1;
	  }

	  if (icon) {
	       al_set_display_icon(container->display, icon);
	  }
	  else {
	       fprintf(stderr, "failed to load window icon\n");
	  }
	  
	  al_set_window_title(container->display, "Necromancy");

     }

     container->event_queue = al_create_event_queue();
     if (!container->event_queue) {
          fprintf(stderr, "failed to create event queue!\n");
          al_destroy_display(container->display);
          al_destroy_timer(container->fps_timer);
          return 1;
     }

     al_register_event_source(container->event_queue,
			      (&container->event_source));

     al_register_event_source(container->event_queue,
                              al_get_display_event_source(
                                   container->display));

     al_register_event_source(container->event_queue,
                              al_get_timer_event_source(
                                   container->fps_timer));

     al_register_event_source(container->event_queue,
                              al_get_mouse_event_source());

     al_register_event_source(container->event_queue,
                              al_get_keyboard_event_source());

     return 0;
     
}
