/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <stdio.h>

#ifdef __linux__
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#endif

#ifdef _WIN32
#include <winsock.h>
#endif

#include "player.h"
#include "overlay.h"
#include "units.h"
#include "unit_values.h"
#include "enemy.h"
#include "multiplayer.h"
#include "campaign.h"
#include "items.h"

/*
  File-scoped variables
*/
static struct PLAYER *player = NULL;

/*
  Function definitions
*/

/*
  initialize player structure to default values
    if player struct already exists,
      rewrite values to default values

  returns 1 on failure, 0 on success

  TODO loading values from save file
*/
int init_player(void)
{
     if (!player) {
	  player = malloc(sizeof(struct PLAYER));
	  if (!player) {
	       fprintf(stderr, "malloc failed allocating player struct\n");
	       return 1;
	  }
     }

     int i;

     /* generic, campaign related values */
     player->bones_to_spend = 5;
     player->current_location_id = STARTING_LOC;

     for (i = 0; i < MAX_INVENTORY; i++)
	  player->inventory[i] = ITEM_EMPTY;

     player->num_items = 0;

     for (i = 0; i < MAX_UPGRADES; i++)
	  player->upgrades[i] = UPGRADE_NONE;

     player->num_upgrades = 0;

     for (i = 0; i < (NUM_LOCATIONS - 1) * 2; i++)
	  player->visited_location_id_pairs[i] = NO_LOCATION;

     player->sprite =
	       al_load_bitmap("data/assets/player_sprites/tmp.png");
     if (!player->sprite) {
	  fprintf(stderr, "Failed to load player sprite\n");
	  return 1;
     }

     /* set initial battle-related values */
     player->health = 
	  player->health_max = HEALTH_MAX;
     player->mana = 
	  player->mana_max = MANA_MAX;
     player->num_units_summoned = 0;

     player->spell_on_cooldown[UNIT_SOLDIER] =
	  player->spell_on_cooldown[UNIT_WIZARD] =
	  player->spell_on_cooldown[UNIT_ARCHER] = false;

     player->spell_cooldowns[UNIT_SOLDIER] =
	  unit_values[UNIT_SOLDIER].DEFAULT_COOLDOWN;
     player->spell_cooldowns[UNIT_ARCHER] =
	  unit_values[UNIT_ARCHER].DEFAULT_COOLDOWN;
     player->spell_cooldowns[UNIT_WIZARD] =
	  unit_values[UNIT_WIZARD].DEFAULT_COOLDOWN;

     /* TODO */
     /*
     player->player_defined_keys[SPELL_ARCHER] = ALLEGRO_KEY_Q;
     player->player_defined_keys[SPELL_WIZARD] = ALLEGRO_KEY_W;
     player->player_defined_keys[SPELL_SOLDIER] = ALLEGRO_KEY_E;
     */

     return 0;
}

/*
  to be called at the beginning of every battle

  reset all battle-related values to default
*/
void reset_player_for_battle(void)
{
     player->health = 
	  player->health_max = HEALTH_MAX;
     player->mana = 
	  player->mana_max = MANA_MAX;
     player->num_units_summoned = 0;

     player->spell_on_cooldown[UNIT_SOLDIER] =
	  player->spell_on_cooldown[UNIT_WIZARD] =
	  player->spell_on_cooldown[UNIT_ARCHER] = false;

     player->spell_cooldowns[UNIT_SOLDIER] =
	  unit_values[UNIT_SOLDIER].DEFAULT_COOLDOWN;
     player->spell_cooldowns[UNIT_ARCHER] =
	  unit_values[UNIT_ARCHER].DEFAULT_COOLDOWN;
     player->spell_cooldowns[UNIT_WIZARD] =
	  unit_values[UNIT_WIZARD].DEFAULT_COOLDOWN;

     return;
}

/*
  cast spell, raising given skeleton type from the ground

  checks if player mana is high enough to cast spell

  add unit to linked list of units
*/

void cast_spell(int unit_type)
{
     float unit_mana_cost = unit_values[unit_type].DEFAULT_MANA_COST *
	  unit_values[unit_type].USER_MANA_COST_MOD;

     if (!player_is_spell_on_cooldown(unit_type) &&
	 player_get_current_mana() > (unit_mana_cost) &&
	 player_get_units_summoned() < player_get_max_units_summoned()) {

	  player_increase_units_summoned();
	  player_lower_mana(unit_mana_cost);

	  player_set_spell_on_cooldown(unit_type);

	  if (add_unit(PLAYER_OWNED, unit_type)) {
	       fprintf(stderr, "failed to add player unit\n");
	       return;
	  }

	  if (ENEMY_SOURCE == ENEMY_REMOTE_MULT) {
	       char buffer[32];
	       snprintf(buffer, sizeof(buffer), "summon %d", unit_type);
	       send(remote_fd, buffer, sizeof(buffer), 0);

	       printf("Sent data: %s to remote host\n", buffer);
	  }
     }

     return;
}

/******************
  Get Functions
*******************/
float player_get_current_health(void)
{
     return player->health;
}

float player_get_max_health(void)
{
     return player->health_max;
}

float player_get_current_mana(void)
{
     return player->mana;
}

float player_get_max_mana(void)
{
     return player->mana_max;
}

int player_get_units_summoned(void)
{
     return player->num_units_summoned;
}

int player_get_max_units_summoned(void)
{
     return MAX_UNITS;
}

bool player_is_spell_on_cooldown(int unit)
{
     return player->spell_on_cooldown[unit];
}

float player_get_cooldown_remaining(int unit)
{
     return player->spell_cooldowns[unit];
}

int player_get_bones_to_spend(void)
{
     return player->bones_to_spend;
}

int player_get_current_location(void)
{
     return player->current_location_id;
}

ALLEGRO_BITMAP *player_get_sprite(void)
{
     return player->sprite;
}

int *player_get_visited_location_ids(void)
{
     return player->visited_location_id_pairs;
}

int player_get_num_items(void)
{
     return player->num_items;
}

int *player_get_held_items_id_list(void)
{
     return player->inventory;
}

/******************
  Setter Functions
*******************/

void player_set_spell_on_cooldown(int unit)
{
     player->spell_on_cooldown[unit] = true;
     
     player->spell_cooldowns[unit] = unit_values[unit].DEFAULT_COOLDOWN /
	  unit_values[unit].USER_COOLDOWN_MOD;

     return;
}

void player_set_current_location(int target)
{
     int i;

     for (i = 0; i < (NUM_LOCATIONS - 1) * 2; i++) {
	  if (player->visited_location_id_pairs[i] == NO_LOCATION) {
	       /* insert here */
	       player->visited_location_id_pairs[i] = player_get_current_location();
	       player->visited_location_id_pairs[i + 1] = target;

	       printf("start: %d stop: %d\n", player->visited_location_id_pairs[i],
		      player->visited_location_id_pairs[i+1]);

	       break;
	  }
     }

     player->current_location_id = target;

     return;
}

void player_add_item(int id)
{
     int i;
     for (i = 0; i < items_get_num_items(); i++) {
	  if (player->inventory[i] == ITEM_EMPTY) {
	       player->inventory[i] = id;
	       player->num_items++;
	       return;
	  }
     }
}

/******************
  Lower Functions
*******************/

void player_lower_units_summoned(void)
{
     if (player->num_units_summoned > 0)
	  player->num_units_summoned--;

     return;
}

void player_lower_units_summoned_to_zero(void)
{
     player->num_units_summoned = 0;

     return;
}

void player_lower_health(int unit_type)
{
     player->health -= unit_type * (3.0f);

     return;
}

void player_lower_mana(float amount)
{
     if ((player->mana - amount) >= 0.0f)
	  player->mana -= amount;

     return;

}

void player_lower_bones(void)
{
     if (player->bones_to_spend > 0)
	  player->bones_to_spend -= 1;

     return;
}

void player_lower_cooldowns(float amount)
{
     if (player_is_spell_on_cooldown(UNIT_SOLDIER)) {
	  if ((player->spell_cooldowns[UNIT_SOLDIER] - amount) > 0.0f)
	       player->spell_cooldowns[UNIT_SOLDIER] -= amount;
	  else {
	       player->spell_cooldowns[UNIT_SOLDIER] = 0.0f;
	       player->spell_on_cooldown[UNIT_SOLDIER] = false;
	  }
     }

     if (player_is_spell_on_cooldown(UNIT_ARCHER)) {
	  if ((player->spell_cooldowns[UNIT_ARCHER] - amount) > 0.0f)
	       player->spell_cooldowns[UNIT_ARCHER] -= amount;
	  else {
	       player->spell_cooldowns[UNIT_ARCHER] = 0.0f;
	       player->spell_on_cooldown[UNIT_ARCHER] = false;
	  }
     }


     if (player_is_spell_on_cooldown(UNIT_WIZARD)) {
	  if ((player->spell_cooldowns[UNIT_WIZARD] - amount) > 0.0f)
	       player->spell_cooldowns[UNIT_WIZARD] -= amount;
	  else {
	       player->spell_cooldowns[UNIT_WIZARD] = 0.0f;
	       player->spell_on_cooldown[UNIT_WIZARD] = false;
	  }
     }

}

void player_lower_cooldowns_to_zero(void)
{
     player->spell_cooldowns[UNIT_SOLDIER] = 0.0f;
     player->spell_on_cooldown[UNIT_SOLDIER] = false;

     player->spell_cooldowns[UNIT_ARCHER] = 0.0f;
     player->spell_on_cooldown[UNIT_ARCHER] = false;

     player->spell_cooldowns[UNIT_WIZARD] = 0.0f;
     player->spell_on_cooldown[UNIT_WIZARD] = false;

     return;
}

void player_lower_bones_by_amount(int amount)
{
     if (player->bones_to_spend >= amount)
	  player->bones_to_spend -= amount;

     return;
}

/******************
  Increase Functions
*******************/

void player_increase_mana(float amount)
{
     if ((player->mana + amount) < player->mana_max)
	  player->mana += amount;

     else
	  player->mana = player->mana_max;

     return;
}

void player_increase_health(float amount)
{
     if ((player->health + amount) < player->health_max)
	  player->health += amount;
     else
	  player->health = player->health_max;

     return;
}

void player_increase_units_summoned(void)
{
     if (player->num_units_summoned > MAX_UNITS)
	  return;

     player->num_units_summoned++;

     return;
}

void player_increase_bones(void)
{
     player->bones_to_spend++;

     return;
}

