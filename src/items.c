/*
  load all items from config file
*/

#include <stdio.h>
#include <allegro5/allegro.h>

#include "items.h"

static int num_items = 0;

static struct ITEM *items_list = NULL;
static int *items_id_list = NULL;

/*
  load each item into the array
*/
int items_init(void)
{
     items_list = malloc(sizeof(struct ITEM) * 2);
     if (!items_list) {
	  fprintf(stderr, "Failed to allocate items_list array\n");
	  return 1;
     }

     items_id_list = malloc(sizeof(int) * 2);
     if (!items_id_list) {
	  fprintf(stderr, "failed to allocate items_id_list array\n");
	  return 1;
     }

     /****** ITEM ONE *****/

     items_list[0].id = 101; items_list[0].cost = 2;
     /* does this have to be manually NULL-terminated??? */
     strncpy(items_list[0].name, "Jar of Magic", sizeof(items_list[0].name));
     strncpy(items_list[0].description, "Replenish your mana",
	     sizeof(items_list[0].description));
     strncpy(items_list[0].shopkeeper_comment, "It'll get your spells a runnin' agin",
	     sizeof(items_list[0].shopkeeper_comment));
     items_list[0].image = al_load_bitmap("./data/assets/items/jar_of_magic.png");

     if (!items_list[0].image) {
	  fprintf(stderr, "failed to load ./data/assets/items/jar_of_magic.png\n");
	  return 1;
     }

     items_id_list[0] = items_list[0].id;

     num_items++;

     /***** ITEM TWO ****/
     items_list[1].id = 102; items_list[1].cost = 3;
     /* does this have to be manually NULL-terminated??? */
     strncpy(items_list[1].name, "Bone of Regrowth", sizeof(items_list[1].name));
     strncpy(items_list[1].description, "Heal all skeletons on screen",
	     sizeof(items_list[1].description));
     strncpy(items_list[1].shopkeeper_comment, "It'll get your skellies runnin' agin",
	     sizeof(items_list[1].shopkeeper_comment));
     items_list[1].image = al_load_bitmap("./data/assets/items/bone_of_regrowth.png");

     if (!items_list[1].image) {
	  fprintf(stderr, "failed to load ./data/assets/items/bone_of_regrowth.png\n");
	  return 1;
     }

     items_id_list[1] = items_list[1].id;

     num_items++;

     fprintf(stderr, "ran items init\n");

     return 0;
}

/*
  return appropriate item, if it exists,
*/
struct ITEM *items_get_from_id(int target_id)
{
     int i;
     for (i = 0; i < num_items; i++) {
	  if (items_list[i].id == target_id)
	       return &(items_list[i]);
     }

     return NULL;
}

/*
  return the id list
*/
int *items_get_id_list(void)
{
     return items_id_list;
}

/*
  return number of items in game
*/
int items_get_num_items(void)
{
     return num_items;
}
