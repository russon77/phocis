/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>

#include "menu_levelup.h"
#include "player.h"
#include "units.h"
#include "unit_values.h"
#include "settings.h"

/*
  Structure definitions
*/

struct UNIT_VALUES new_unit_values[3];

/*
  File-specific scoped variables
*/
struct MENU_LEVELUP_RESOURCES *mlur = NULL;

// initialize to default
int UNIT_SELECTED = UNIT_SOLDIER;

bool unit_type_selected[3] = {false, false, false};
bool unit_type_hover_over[3] = {false, false, false};

bool continue_button_hover = false;

enum BUTTON_LIST {
     ATTACK_MOD_BUTTON_MINUS, ATTACK_MOD_BUTTON_PLUS,
     DEFENSE_MOD_BUTTON_MINUS, DEFENSE_MOD_BUTTON_PLUS,
     COOLDOWN_MOD_BUTTON_MINUS, COOLDOWN_MOD_BUTTON_PLUS
} ;

enum LEVELUP_MOUSE_EVENT_RESULTS {
     EVENT_LEAVE, EVENT_CONTINUE
} ;

enum LOCATION_SUB {
     XBEGIN, XEND, YBEGIN, YEND
} ;

bool button_hover_over[6] = {false, false, false, false, false, false};
bool button_selected[6] = {false, false, false, false, false, false};

bool is_finished = false;

#define DEFAULT_CHANGE_PER_UPGRADE (0.02f);


// window-location variables
const int menu_width = 400;
const int menu_height = 400;

const int xoffset = 9;
const int yoffset = 20;

const int text_height = 36;

int xbegin, ybegin, xend, yend;
 
/*
  Function prototypes
*/

int menu_levelup_load(void);
void display_menu(struct ALLEGRO_RESOURCES *);
void set_state_from_mouse_event(ALLEGRO_EVENT *);
void process_user_click(int);
void finalize_write_unit_values(void);

/*
  Function definitions
*/

/*
  display the menu, processing user (mouse) input
  
  upon completion, write changes to appropriate places
       i.e. player, unit_values
*/
int menu_levelup(struct ALLEGRO_RESOURCES *container)
{
     // set our position variables
     xbegin = (SCREEN_W - menu_width) / 2;
     ybegin = (SCREEN_H - menu_height) / 2;
     xend = (SCREEN_W + menu_width) / 2;
     yend = (SCREEN_H + menu_height) / 2;

     // set our new structure to its initial values
     new_unit_values[0] = unit_values[0];
     new_unit_values[1] = unit_values[1];
     new_unit_values[2] = unit_values[2];

     // set finished
     is_finished = false;
     

     // load the menu
     if (!mlur) {
	  if (menu_levelup_load()) {
	       fprintf(stderr, "menu_levelup_load() failed\n");
	       return 1;
	  }
     }

     bool redraw = false;

     while(!is_finished) {
	  ALLEGRO_EVENT ev;
	  al_wait_for_event(container->event_queue, &ev);

	  if (ev.type == ALLEGRO_EVENT_TIMER)
	       redraw = true;

	  else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
	       return 1;

	  else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
                  ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY ||
		  ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)

	       set_state_from_mouse_event(&ev);

	  if (redraw && al_is_event_queue_empty(container->event_queue)) {
	       redraw = false;

	       // TODO this should instead display proper textures
	       al_clear_to_color(al_map_rgb(0,0,0));

	       // print everything
	       display_menu(container);

	       al_flip_display();
	  }
     } ;

     finalize_write_unit_values();
     
     return 0;
}

void set_state_from_mouse_event(ALLEGRO_EVENT *ev)
{
     // TODO set proper state from mouse DOWN and mouse UP

     int xmouse = ev->mouse.x;
     int ymouse = ev->mouse.y;
     int i;

     // reset every boolean to false
     for (i = 0; i < 3; i++) {
	  unit_type_selected[i] = false;
	  unit_type_hover_over[i] = false;
     }

     for (i = 0; i < 6; i++) {
	  button_hover_over[i] = false;
	  button_selected[i] = false;
     }

     continue_button_hover = false;
     
     // reset unit type selected
     unit_type_selected[UNIT_SELECTED] = true;

     // set proper boolean to true
     // have to test every button until proper one is found!!! woo

     for (i = 0; i < 3; i++) {
	  if (xmouse > mlur->spell_locations[i][XBEGIN] &&
	       xmouse < mlur->spell_locations[i][XEND] &&
	       ymouse > mlur->spell_locations[i][YBEGIN] &&
	      ymouse < mlur->spell_locations[i][YEND]) {

	       unit_type_hover_over[i] = true;

	       if (ev->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
		    UNIT_SELECTED = i;
	       }
	  }
     }

     for (i = 0; i < 6; i++) {
	  if (xmouse > mlur->buttons_locations[i][XBEGIN] &&
	       xmouse < mlur->buttons_locations[i][XEND] &&
	       ymouse > mlur->buttons_locations[i][YBEGIN] &&
	      ymouse < mlur->buttons_locations[i][YEND]) {

	       button_hover_over[i] = true;

	       if (ev->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
		    button_selected[i] = true;
		    process_user_click(i);
	       }
	  }
     }

     if (xmouse > mlur->button_continue_locations[XBEGIN] &&
	  xmouse < mlur->button_continue_locations[XEND] &&
	  ymouse > mlur->button_continue_locations[YBEGIN] &&
	 ymouse < mlur->button_continue_locations[YEND]) {

	  continue_button_hover = true;

	  if (ev->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
	       // leave the levelup screen
	       is_finished = true;
	  }
     }

     return;
}


void process_user_click(int button)
{
     // do proper action based on mouse input

     if (button == ATTACK_MOD_BUTTON_MINUS) {
	  if (new_unit_values[UNIT_SELECTED].USER_DAMAGE_MOD >
	      unit_values[UNIT_SELECTED].USER_DAMAGE_MOD) {

	       new_unit_values[UNIT_SELECTED].USER_DAMAGE_MOD -= DEFAULT_CHANGE_PER_UPGRADE;

	       player_increase_bones();
	  }
     }

     else if (button == ATTACK_MOD_BUTTON_PLUS) {
	  if (player_get_bones_to_spend()) {
	       new_unit_values[UNIT_SELECTED].USER_DAMAGE_MOD += DEFAULT_CHANGE_PER_UPGRADE;

	       player_lower_bones();
	  }
     }

     if (button == DEFENSE_MOD_BUTTON_MINUS) {
	  if (new_unit_values[UNIT_SELECTED].USER_DEFENSE_MOD >
	      unit_values[UNIT_SELECTED].USER_DEFENSE_MOD) {
	       new_unit_values[UNIT_SELECTED].USER_DEFENSE_MOD -= DEFAULT_CHANGE_PER_UPGRADE;
	       new_unit_values[UNIT_SELECTED].USER_HEALTH_MOD -= DEFAULT_CHANGE_PER_UPGRADE;

	       player_increase_bones();
	  }
     }

     else if (button == DEFENSE_MOD_BUTTON_PLUS) {
	  if (player_get_bones_to_spend()) {
	       new_unit_values[UNIT_SELECTED].USER_DEFENSE_MOD += DEFAULT_CHANGE_PER_UPGRADE;
	       new_unit_values[UNIT_SELECTED].USER_HEALTH_MOD += DEFAULT_CHANGE_PER_UPGRADE;

	       player_lower_bones();
	  }
     }

     if (button == COOLDOWN_MOD_BUTTON_MINUS) {
	  if (new_unit_values[UNIT_SELECTED].USER_COOLDOWN_MOD >
	      unit_values[UNIT_SELECTED].USER_COOLDOWN_MOD) {
	       new_unit_values[UNIT_SELECTED].USER_COOLDOWN_MOD -= DEFAULT_CHANGE_PER_UPGRADE;

	       player_increase_bones();
	  }
     }

     else if (button == COOLDOWN_MOD_BUTTON_PLUS) {
	  if (player_get_bones_to_spend()) {
	       new_unit_values[UNIT_SELECTED].USER_COOLDOWN_MOD += DEFAULT_CHANGE_PER_UPGRADE;

	       player_lower_bones();
	  }
     }
     
     return;
}

void finalize_write_unit_values(void)
{
     unit_values[0] = new_unit_values[0];
     unit_values[1] = new_unit_values[1];
     unit_values[2] = new_unit_values[2];
 
     return;
}

/*
  Print the entire menu
*/
void display_menu(struct ALLEGRO_RESOURCES *container)
{

     // draw top left corner of menu
     al_draw_bitmap_region(mlur->borders,
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_X],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_Y],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_W],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_H],
			   xbegin, ybegin, 0);

     // draw top right corner of menu
     al_draw_bitmap_region(mlur->borders,
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_X],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_Y],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_W],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_H],
			   xend - mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_W],
			   ybegin,
			   ALLEGRO_FLIP_HORIZONTAL);

     // draw bottom left corner of menu
     al_draw_bitmap_region(mlur->borders,
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_X],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_Y],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_W],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_H],
			   xbegin,
			   yend - mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_H],
			   ALLEGRO_FLIP_VERTICAL);


     // draw bottom right corner of menu
     al_draw_bitmap_region(mlur->borders,
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_X],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_Y],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_W],
			   mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_H],
			   xend - mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_W],
			   yend - mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_H],
			   ALLEGRO_FLIP_VERTICAL | ALLEGRO_FLIP_HORIZONTAL);

     // TODO draw in horizontal/vertical flat pieces between corners

     // draw in 3 spells

     // TODO draw depends on HOVER

     float archer_picture_tint = 1.0f;
     if (UNIT_SELECTED == UNIT_ARCHER)
	  archer_picture_tint = 0.0f;
     else if (unit_type_hover_over[UNIT_ARCHER])
	  archer_picture_tint = 0.5f;
     
     al_draw_tinted_bitmap(mlur->archer_spell,
			   al_map_rgba_f(archer_picture_tint,archer_picture_tint,1,1),
			   xbegin + xoffset, ybegin + yoffset,
			   0);
     
     float soldier_picture_tint = 1.0f;
     if (UNIT_SELECTED == UNIT_SOLDIER)
	  soldier_picture_tint = 0.0f;
     else if (unit_type_hover_over[UNIT_SOLDIER])
	  soldier_picture_tint = 0.5f;

     al_draw_tinted_bitmap(mlur->soldier_spell,
			   al_map_rgba_f(soldier_picture_tint, soldier_picture_tint,1,1),
			   xbegin + xoffset + menu_width*1/3, ybegin + yoffset,
			   0);

     float wizard_picture_tint = 1.0f;
     if (UNIT_SELECTED == UNIT_WIZARD)
	  wizard_picture_tint = 0.0f;
     else if (unit_type_hover_over[UNIT_WIZARD])
	  wizard_picture_tint = 0.5f;

     al_draw_tinted_bitmap(mlur->wizard_spell,
			   al_map_rgba_f(wizard_picture_tint, wizard_picture_tint, 1,1),
			   xbegin + xoffset + menu_width*2/3, ybegin + yoffset,
			   0);

     // draw in continue text and button

     int text_tint = 0;
     if (continue_button_hover) {
	  text_tint = 255;
     }
     
     al_draw_text(container->font, al_map_rgb(255,text_tint,0),
		  xbegin + menu_width*7/8, ybegin + menu_height*4/8, ALLEGRO_ALIGN_CENTRE,
		  "Continue");

     al_draw_bitmap(mlur->button_continue,
		    xbegin + menu_width*9/12, ybegin + menu_height*5/8,
		    0);

     // print out UNIT SELECTED text
     // for user clarity sake
     // TODO
     
     // print text
     al_draw_text(container->font, al_map_rgb(255,255,255),
		  xbegin + menu_width/2, ybegin + menu_height*1/4, ALLEGRO_ALIGN_CENTRE,
		  "Attack Modifier");

     al_draw_text(container->font, al_map_rgb(255,255,255),
		  xbegin + menu_width/2, ybegin + menu_height*2/4, ALLEGRO_ALIGN_CENTRE,
		  "Defense Modifier");

     al_draw_text(container->font, al_map_rgb(255,255,255),
		  xbegin + menu_width/2, ybegin + menu_height*3/4, ALLEGRO_ALIGN_CENTRE,
		  "Cooldown Modifier");

     // draw in remaining points text

     al_draw_text(container->font, al_map_rgb(255,0,0),
		  xbegin + xoffset, ybegin + menu_height*2/4, ALLEGRO_ALIGN_LEFT,
		  "To Spend");
     // TODO Implement
     al_draw_textf(container->font, al_map_rgb(255,255,255),
		   xbegin + menu_width*3/32, ybegin + menu_height*5/8, ALLEGRO_ALIGN_LEFT,
		   "%d", player_get_bones_to_spend());

     // print values for modifiers
     al_draw_textf(container->font, al_map_rgb(255,255,255),
		  xbegin + menu_width/2, ybegin + menu_height*3/8, ALLEGRO_ALIGN_CENTRE,
		  "%.2f", new_unit_values[UNIT_SELECTED].USER_DAMAGE_MOD);

     al_draw_textf(container->font, al_map_rgb(255,255,255),
		  xbegin + menu_width/2, ybegin + menu_height*5/8, ALLEGRO_ALIGN_CENTRE,
		  "%.2f", new_unit_values[UNIT_SELECTED].USER_DEFENSE_MOD);

     al_draw_textf(container->font, al_map_rgb(255,255,255),
		  xbegin + menu_width/2, ybegin + menu_height*7/8, ALLEGRO_ALIGN_CENTRE,
		  "%.2f", new_unit_values[UNIT_SELECTED].USER_COOLDOWN_MOD);

     // draw in buttons


     int i;

     // draw in attack modifier button down and button up

     for (i = 0; i < 3; i++) {
	  al_draw_bitmap_region(mlur->buttons,
				mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_X],
				mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_Y],
				mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_W],
				mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_H],
				xbegin + menu_width/4,
				ybegin + menu_height*(i+1)/4 + text_height,
				0);

	  al_draw_bitmap_region(mlur->buttons,
				mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_X],
				mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_Y],
				mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_W],
				mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_H],
				xbegin + menu_width*3/4 -
			             mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_W],
				ybegin + menu_height*(i+1)/4 + text_height,
				0);
     }

     return;
}

/*
  initialize and load the resources structure for this menu
*/
int menu_levelup_load(void)
{

     // in case resources are attempted to load twice
     // may want to enable this in case of hot-reloading of menu graphx
     if (mlur) {
	  return 1;
     }

     mlur = malloc(sizeof(struct MENU_LEVELUP_RESOURCES));
     if (!mlur) {
	  fprintf(stderr, "error allocating menu levelup resources\n");
	  return 1;
     }

     // load ui elements
     mlur->borders = al_load_bitmap("./data/assets/ui/borders.png");
     if (!mlur->borders) {
	  fprintf(stderr, "error loading bitmap: borders.png\n");
	  return 1;
     }

     mlur->buttons = al_load_bitmap("./data/assets/ui/buttons.png");
     if (!mlur->buttons) {
	  fprintf(stderr, "error loading bitmap: buttons.png\n");
	  return 1;
     }

     mlur->checkmarks = al_load_bitmap("./data/assets/ui/checkmarks.png");
     if (!mlur->checkmarks) {
	  fprintf(stderr, "error loading bitmap: checkmarks.png\n");
	  return 1;
     }

     // load spell bitmaps
     mlur->archer_spell = al_load_bitmap("./data/assets/spells/archer.png");
     if (!mlur->archer_spell) {
	  fprintf(stderr, "error loading bitmap archer.png\n");
	  return 1;
     }

     mlur->soldier_spell = al_load_bitmap("./data/assets/spells/soldier.png");
     if (!mlur->soldier_spell) {
	  fprintf(stderr, "error loading bitmap soldier.png\n");
	  return 1;
     }

     mlur->wizard_spell = al_load_bitmap("./data/assets/spells/wizard.png");
     if (!mlur->wizard_spell) {
	  fprintf(stderr, "error loading bitmap wizard.png\n");
	  return 1;
     }

     mlur->button_continue = al_load_bitmap("./data/assets/ui/arrow_right.png");
     if (!mlur->button_continue) {
	  fprintf(stderr, "error loading arrow_right.png\n");
	  return 1;
     }

     // load 'specials' bitmaps
     // TODO

     // write spritemap tables
     // TODO find a million bugs here and eventually
     // have these values be loaded from a config file

     // write in borders spritemapping
     mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_X] =
	  mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_Y] = 0;
     mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_W] = 
	  mlur->borders_spritemap[TOP_LEFT_CORNER][SUB_SPRITE_H] = 25;

     mlur->borders_spritemap[TOP_RIGHT_CORNER][SUB_SPRITE_X] = 123;
     mlur->borders_spritemap[TOP_RIGHT_CORNER][SUB_SPRITE_Y] = 0;
     mlur->borders_spritemap[TOP_RIGHT_CORNER][SUB_SPRITE_W] = 
	  mlur->borders_spritemap[TOP_RIGHT_CORNER][SUB_SPRITE_H] = 25;

     mlur->borders_spritemap[FLAT_HORIZONTAL][SUB_SPRITE_X] = 38;
     mlur->borders_spritemap[FLAT_HORIZONTAL][SUB_SPRITE_Y] = 0;
     mlur->borders_spritemap[FLAT_HORIZONTAL][SUB_SPRITE_W] = 80;
     mlur->borders_spritemap[FLAT_HORIZONTAL][SUB_SPRITE_H] = 25;

     // write in buttons spritemapping
     mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_X] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_Y] = 0;

     mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_SELECTED][SUB_SPRITE_X] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_HOVERED][SUB_SPRITE_X] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_INVALID][SUB_SPRITE_X] = 0;

     mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_X] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_SELECTED][SUB_SPRITE_X] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_HOVERED][SUB_SPRITE_X] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_INVALID][SUB_SPRITE_X] = 37;
	  

     mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_SELECTED][SUB_SPRITE_Y] = 48;
     mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_HOVERED][SUB_SPRITE_Y] = 96;
     mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_INVALID][SUB_SPRITE_Y] = 144;
     mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_Y] = 0;
     mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_SELECTED][SUB_SPRITE_Y] = 48;
     mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_HOVERED][SUB_SPRITE_Y] = 96;
     mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_INVALID][SUB_SPRITE_Y] = 144;


     mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_W] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_SELECTED][SUB_SPRITE_W] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_HOVERED][SUB_SPRITE_W] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_INVALID][SUB_SPRITE_W] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_W] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_SELECTED][SUB_SPRITE_W] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_HOVERED][SUB_SPRITE_W] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_INVALID][SUB_SPRITE_W] = 36;

     mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_H] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_SELECTED][SUB_SPRITE_H] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_HOVERED][SUB_SPRITE_H] =
	  mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_INVALID][SUB_SPRITE_H] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_NORMAL][SUB_SPRITE_H] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_SELECTED][SUB_SPRITE_H] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_HOVERED][SUB_SPRITE_H] =
	  mlur->buttons_spritemap[BUTTON_INCREASE][BUTTON_INVALID][SUB_SPRITE_H] = 48;
     

     // write in checkmark box spritemapping
     mlur->checkmark_spritemap[UNCHECKED][CHECK_NORMAL][SUB_SPRITE_X] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_NORMAL][SUB_SPRITE_X] = 0;

     mlur->checkmark_spritemap[UNCHECKED][CHECK_HOVERED][SUB_SPRITE_X] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_HOVERED][SUB_SPRITE_X] = 42;

     mlur->checkmark_spritemap[UNCHECKED][CHECK_INVALID][SUB_SPRITE_X] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_INVALID][SUB_SPRITE_X] = 84;

     mlur->checkmark_spritemap[UNCHECKED][CHECK_NORMAL][SUB_SPRITE_Y] =
	  mlur->checkmark_spritemap[UNCHECKED][CHECK_HOVERED][SUB_SPRITE_Y] =
	  mlur->checkmark_spritemap[UNCHECKED][CHECK_INVALID][SUB_SPRITE_Y] = 0;

     mlur->checkmark_spritemap[CHECKED][CHECK_NORMAL][SUB_SPRITE_Y] = 
	  mlur->checkmark_spritemap[CHECKED][CHECK_HOVERED][SUB_SPRITE_Y] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_INVALID][SUB_SPRITE_Y] = 39;

     mlur->checkmark_spritemap[UNCHECKED][CHECK_NORMAL][SUB_SPRITE_W] =
	  mlur->checkmark_spritemap[UNCHECKED][CHECK_HOVERED][SUB_SPRITE_W] =
	  mlur->checkmark_spritemap[UNCHECKED][CHECK_INVALID][SUB_SPRITE_W] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_NORMAL][SUB_SPRITE_W] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_HOVERED][SUB_SPRITE_W] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_INVALID][SUB_SPRITE_W] = 42;

     mlur->checkmark_spritemap[UNCHECKED][CHECK_NORMAL][SUB_SPRITE_H] =
	  mlur->checkmark_spritemap[UNCHECKED][CHECK_HOVERED][SUB_SPRITE_H] =
	  mlur->checkmark_spritemap[UNCHECKED][CHECK_INVALID][SUB_SPRITE_H] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_NORMAL][SUB_SPRITE_H] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_HOVERED][SUB_SPRITE_H] =
	  mlur->checkmark_spritemap[CHECKED][CHECK_INVALID][SUB_SPRITE_H] = 39;

     // write in our location tables
     // for ease of drawing and for ease of reading user input

     // write buttons table
     int button_width = mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_W];
     int button_height = mlur->buttons_spritemap[BUTTON_DECREASE][BUTTON_NORMAL][SUB_SPRITE_H];
     mlur->buttons_locations[ATTACK_MOD_BUTTON_MINUS][XBEGIN] = xbegin + menu_width/4;
     mlur->buttons_locations[ATTACK_MOD_BUTTON_MINUS][XEND] = xbegin + menu_width/4 + button_width;
     mlur->buttons_locations[ATTACK_MOD_BUTTON_MINUS][YBEGIN] = ybegin + menu_height/4 +  text_height;
     mlur->buttons_locations[ATTACK_MOD_BUTTON_MINUS][YEND] = ybegin + menu_height/4 +
	  text_height + button_height;

     mlur->buttons_locations[ATTACK_MOD_BUTTON_PLUS][XBEGIN] = xbegin + menu_width*3/4 - button_width;
     mlur->buttons_locations[ATTACK_MOD_BUTTON_PLUS][XEND] = xbegin + menu_width*3/4;
     mlur->buttons_locations[ATTACK_MOD_BUTTON_PLUS][YBEGIN] = ybegin + menu_height/4 + text_height;
     mlur->buttons_locations[ATTACK_MOD_BUTTON_PLUS][YEND] = ybegin + menu_height/4 +
	  text_height + button_height;

     mlur->buttons_locations[DEFENSE_MOD_BUTTON_MINUS][XBEGIN] = xbegin + menu_width/4;
     mlur->buttons_locations[DEFENSE_MOD_BUTTON_MINUS][XEND] = xbegin + menu_width / 4 + button_width;
     mlur->buttons_locations[DEFENSE_MOD_BUTTON_MINUS][YBEGIN] = ybegin + menu_height*2/4 + text_height;
     mlur->buttons_locations[DEFENSE_MOD_BUTTON_MINUS][YEND] = ybegin + menu_height*2/4 +
	  text_height + button_height;

     mlur->buttons_locations[DEFENSE_MOD_BUTTON_PLUS][XBEGIN] = xbegin + menu_width*3/4 - button_width;
     mlur->buttons_locations[DEFENSE_MOD_BUTTON_PLUS][XEND] = xbegin + menu_width*3/4;
     mlur->buttons_locations[DEFENSE_MOD_BUTTON_PLUS][YBEGIN] = ybegin + menu_width*2/4 + text_height;
     mlur->buttons_locations[DEFENSE_MOD_BUTTON_PLUS][YEND] = ybegin + menu_width*2/4 +
	  text_height + button_height;

     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_MINUS][XBEGIN] = xbegin + menu_width/4;
     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_MINUS][XEND] = xbegin + menu_width/4 + button_width;
     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_MINUS][YBEGIN] = ybegin + menu_height*3/4 + text_height;
     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_MINUS][YEND] = ybegin + menu_height*3/4 +
	  text_height + button_height;

     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_PLUS][XBEGIN] = xbegin + menu_width*3/4 - button_width;
     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_PLUS][XEND] = xbegin + menu_width*3/4;
     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_PLUS][YBEGIN] = ybegin + menu_height*3/4 + text_height;
     mlur->buttons_locations[COOLDOWN_MOD_BUTTON_PLUS][YEND] = ybegin + menu_height*3/4 +
	  text_height + button_height;

     // write spell table
     mlur->spell_locations[UNIT_SOLDIER][XBEGIN] = xbegin + xoffset +
	  menu_width*1/3;
     mlur->spell_locations[UNIT_SOLDIER][XEND] = xbegin + xoffset +
	  menu_width*1/3 + al_get_bitmap_width(mlur->soldier_spell);
     mlur->spell_locations[UNIT_SOLDIER][YBEGIN] = ybegin + yoffset;
     mlur->spell_locations[UNIT_SOLDIER][YEND] = ybegin + yoffset +
	  al_get_bitmap_height(mlur->soldier_spell);

     mlur->spell_locations[UNIT_ARCHER][XBEGIN] = xbegin + xoffset;
     mlur->spell_locations[UNIT_ARCHER][XEND] = xbegin + xoffset +
	  al_get_bitmap_width(mlur->archer_spell);
     mlur->spell_locations[UNIT_ARCHER][YBEGIN] = ybegin + yoffset;
     mlur->spell_locations[UNIT_ARCHER][YEND] = ybegin + yoffset +
	  al_get_bitmap_height(mlur->archer_spell);

     mlur->spell_locations[UNIT_WIZARD][XBEGIN] = xbegin + xoffset +
	  menu_width*2/3;
     mlur->spell_locations[UNIT_WIZARD][XEND] = xbegin + xoffset +
	  menu_width*2/3 + al_get_bitmap_width(mlur->wizard_spell);
     mlur->spell_locations[UNIT_WIZARD][YBEGIN] = ybegin + yoffset;
     mlur->spell_locations[UNIT_WIZARD][YEND] = ybegin + yoffset +
	  al_get_bitmap_height(mlur->wizard_spell);

     // write continue button table
     mlur->button_continue_locations[XBEGIN] = xbegin + menu_width*9/12;
     mlur->button_continue_locations[XEND] = xbegin + menu_width*9/12 +
	  al_get_bitmap_width(mlur->button_continue);
     // testing a new ybegin for continue button
     mlur->button_continue_locations[YBEGIN] = ybegin + menu_height*4/8;
//     mlur->button_continue_locations[YBEGIN] = ybegin + menu_height*5/8;
     mlur->button_continue_locations[YEND] = ybegin + menu_height*5/8 +
	  al_get_bitmap_height(mlur->button_continue);

     // that's all folks

     return 0;
}
