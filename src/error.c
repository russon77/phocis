#include <stdio.h>

#include "error.h"

int error_add_new_dialog(const char *message)
{
     fprintf(stderr, "ERROR: %s\n", message);

     return 0;
}
