/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "missiles.h"
#include "units.h"
#include "settings.h"
#include "unit_values.h"

static struct MISSILE_RESOURCES *mr = NULL;
static struct MISSILE_HEAD *mh = NULL;
static ALLEGRO_MUTEX *missile_create_mutex = NULL;

enum AXES {
     x, y
};

/*
  Function prototypes
*/
static void missiles_process_individual_collision(struct UNIT *unit, struct MISSILE *missile);

/*
  Function definitions
*/

/*
  returns 1 on error
*/

int missile_resources_load(void)
{
     /* allocate missile head */
     mh = malloc(sizeof(struct MISSILE_HEAD));
     if (!mh) {
	  fprintf(stderr, "malloc missile head\n");
	  return 1;
     }
     mh->head = NULL;
       
     /* allocate struct */
     mr = malloc(sizeof(struct MISSILE_RESOURCES));
     if (!mr) {
          fprintf(stderr, "malloc missile resources\n");
          return 1;
     }

     // load the local mutex
     missile_create_mutex = al_create_mutex();
     if (!missile_create_mutex) {
	  fprintf(stderr, "failed to create missile create mutex\n");
	  return 1;
     }
     
     /* load arrow spritesheet and sprite map */
     mr->arrow_spritesheet =
          al_load_bitmap("./data/assets/missiles/arrow_spritesheet.png");

     if (!mr->arrow_spritesheet) {
          fprintf(stderr, "error loading arrow_spritesheet.png\n");
          return 1;
     }

     mr->arrow_mappings[SUB_SPRITE_X] = 790;
     mr->arrow_mappings[SUB_SPRITE_Y] = 228;
     mr->arrow_mappings[SUB_SPRITE_W] = 42;
     mr->arrow_mappings[SUB_SPRITE_H] = 28;

     /* load spellshot for skeleton */
     mr->spellshot_spritesheet_skeleton =
          al_load_bitmap(
               "./data/assets/missiles/spell_spritesheet_skeleton.png");

     if (!mr->spellshot_spritesheet_skeleton) {
          fprintf(stderr, "error loading spell_spritesheet_skely.png\n");
          return 1;
     }

     /* load spellshot for human */
     mr->spellshot_spritesheet_human =
          al_load_bitmap(
               "./data/assets/missiles/spell_spritesheet_human.png");

     if (!mr->spellshot_spritesheet_human) {
          fprintf(stderr, "error loading spell_spritesheet_human.png\n");
          return 1;
     }

     return 0;
}

/*
  MISSILE constructor
*/
int add_missile(int type, int direction,
                float xsrc, float ysrc,
                float xdest, float ydest)
{
     // lock the mutex
     al_lock_mutex(missile_create_mutex);

     
     // edge case: missile head is null
     if (!mh->head) {
          mh->head = (struct MISSILE *)
               malloc(sizeof(struct MISSILE));
          if (!mh->head) {
               fprintf(stderr, "malloc failed creating missile\n");
               return 0;
          }
          mh->head->next = NULL;
     }
     else {
          // otherwise, add to front
          struct MISSILE *tmp =
               (struct MISSILE *)malloc(sizeof(struct MISSILE));
          if (!tmp) {
               fprintf(stderr, "malloc failed creating missile\n");
               return 0;
          }

          tmp->next = mh->head;
          mh->head = tmp;
     }

     // TODO fix positioning for humans
     if (type == MISSILE_ARROW) {
          mh->head->width = 42;
          mh->head->height = 28;
          if (direction == DIRECTION_RIGHT) {
               mh->head->xsrc = xsrc + 30;
               mh->head->ysrc = ysrc + 27;
          }
          else {
               mh->head->xsrc = xsrc - 30;
               mh->head->ysrc = ysrc + 27;               
          }
     }
     else { // type is missile_spell
          mh->head->width = 35;
          mh->head->height = 30;
          if (direction == DIRECTION_RIGHT) {
               mh->head->xsrc = xsrc;
               mh->head->ysrc = ysrc + 10;
          }
          else {
               mh->head->xsrc = xsrc - 100;
               mh->head->ysrc = ysrc + 10;               
          }
     }
          
     mh->head->time_since_launch = 0.0f;
     mh->head->ydest = ydest;
     mh->head->xdest = xdest;
     mh->head->type = type;
     mh->head->direction = direction;
     mh->head->to_be_removed = false;


     // TODO calculate initial y and x velocity
     float time_to_hit;

     switch (type) {
     case MISSILE_ARROW:
	  // calculate y velocity using MATH
	  time_to_hit = abs(xsrc - xdest) / MISSILE_ARROW_DEFAULT_X_VELOCITY;

          if (ysrc > ydest)
               mh->head->yvelocity = (-1) * abs(ysrc - ydest) / time_to_hit;
          else
               mh->head->yvelocity = abs(ysrc - ydest) / time_to_hit;

	  // set default x velocity
	  mh->head->xvelocity = MISSILE_ARROW_DEFAULT_X_VELOCITY;
	  
          mh->head->spritesheet = mr->arrow_spritesheet;

          mh->head->arrow_mappings = &mr->arrow_mappings;

          break;
     case MISSILE_SPELL:
	  // calculate y velocity
	  time_to_hit = abs(xsrc - xdest) / MISSILE_SPELL_DEFAULT_X_VELOCITY;

	  mh->head->yvelocity = abs(ysrc - ydest) / time_to_hit;

	  // set default x velocity
          mh->head->xvelocity = MISSILE_SPELL_DEFAULT_X_VELOCITY;

	  // add proper spritesheet based on direction aka allegiance
          if (direction == DIRECTION_RIGHT)
               mh->head->spritesheet =
                    mr->spellshot_spritesheet_skeleton;
          else
               mh->head->spritesheet =
                    mr->spellshot_spritesheet_human;
               
          break;
     }

     al_unlock_mutex(missile_create_mutex);
     
     return 1;
}

/*
  move missiles, checking if out of bounds, and if so, deleting
*/
void missiles_move(void)
{
     struct MISSILE *current = mh->head;

     while (current) {
          // TODO implement 2d physics

          current->time_since_launch += (1.0 / 60.0);


	  if (current->type == MISSILE_SPELL) {
	       // proc spell time out deletion
	       if (current->time_since_launch > MISSILE_SPELL_TIME_TO_LIVE) {
		    current->to_be_removed = true;
	       }
	       // 
	       else {
		    current->xvelocity += (15.0f)/(60.0f);
	       }
	  }

	  // process movement in proper direction
	  // movement is based on 2d physics

	  // calc new x position
          if (current->direction == DIRECTION_RIGHT) 
               current->xpos = current->xsrc +
                    (current->time_since_launch *
                     current->xvelocity);
          else { /* direction is left */
               current->xpos = current->xsrc -
                    (current->time_since_launch *
                     current->xvelocity);
	  }

	  // calc new y position
          current->ypos = current->ysrc + (current->time_since_launch * current->yvelocity);

          if (current->xpos < -100.0f || current->xpos > (SCREEN_W + 100))
               // delete this missile
               current->to_be_removed = true;

          current = current->next;
     } ;

     return;
}

void missiles_process_collisions(void)
{
     struct UNIT *current_unit = uh->head;
     struct MISSILE *current_missile;

     // for each UNIT on screen
     while (current_unit) {

	  if (current_unit->health > 0.0f) {

	       // for each MISSILE on screen
	       current_missile = mh->head;
	       while (current_missile) {

		    // NOTE abstracted collision detection and processing
		    // to another function
		    if (!current_missile->to_be_removed)
			 missiles_process_individual_collision(current_unit, current_missile);
		    
		    current_missile = current_missile->next;
	       } ;
	  }

               current_unit = current_unit->next;
     } ;

     return;
}

void missiles_garbage_collector(void)
{
     struct MISSILE *current = mh->head;
     struct MISSILE *previous = NULL;

     while (current) {

          if (current->to_be_removed) {
               // remove this missile from list
               if (!previous) {
                    mh->head = current->next;
                    free(current);
                    current = mh->head;
                    continue;
               }
               else {
                    previous->next = current->next;
                    free(current);
                    current = previous->next;
                    continue;
               }
          }

          previous = current;
          current = current->next;
     }

     return;
}

void missiles_draw(void)
{
     struct MISSILE *current = mh->head;

     while (current) {

          if (current->type == MISSILE_ARROW)
               al_draw_bitmap_region(current->spritesheet,
                            (*current->arrow_mappings)[SUB_SPRITE_X],
                            (*current->arrow_mappings)[SUB_SPRITE_Y],
                            (*current->arrow_mappings)[SUB_SPRITE_W],
                            (*current->arrow_mappings)[SUB_SPRITE_H],
                            current->xpos,
                            current->ypos,
             current->direction == DIRECTION_LEFT ?
                                ALLEGRO_FLIP_HORIZONTAL : 0);

          else
               al_draw_bitmap(current->spritesheet,
                              current->xpos,
                              current->ypos,
             current->direction == DIRECTION_LEFT ?
                                ALLEGRO_FLIP_HORIZONTAL : 0);
                              

          current = current->next;
     } ;

     return;
}

/*
  helper function to abstract ugly missile detection logic
  away from list traversal

  function should detect if there is an overlap
  between UNIT and MISSILE: tests for intersections

  points are described as X,Y pairs in location arrays

  rectangle points are stored in arrays in pattern:
           0           2
	   1           3
*/

// TODO create an enum to properly describe the point intersections
// TODO wizard spell doesn't damage

static void missiles_process_individual_collision(struct UNIT *unit, struct MISSILE *missile)
{
     float missile_location[4][2] = {{missile->xpos, missile->ypos},
				   {missile->xpos, missile->ypos + missile->height},
				   {missile->xpos + missile->width, missile->ypos},
				   {missile->xpos + missile->width, missile->ypos + missile->height}};

     float this_unit_height = (*unit->sprites_mapping_array)[unit->spritepos][SUB_SPRITE_H];
     float this_unit_width = (*unit->sprites_mapping_array)[unit->spritepos][SUB_SPRITE_H];
     float unit_location[4][2] = {{unit->xpos, unit->ypos},
				{unit->xpos, unit->ypos + this_unit_height},
				{unit->xpos + this_unit_width, unit->ypos},
				{unit->xpos + this_unit_width, unit->ypos + this_unit_height}};

     if (unit->direction != missile->direction) {
	  int i;
	  for (i = 0; i < 4; i++) {
	       if (missile_location[i][x] > unit_location[0][x] &&
		   missile_location[i][x] < unit_location[2][x]) {

		    // meets within x boundaries
		    // now test y boundaries
		    if (missile_location[i][y] > unit_location[0][y] &&
			missile_location[i][y] < unit_location[3][y]) {

			 // meets within x and y boundaries
			 // damage unit and remove missile
			 if (missile->type == MISSILE_ARROW) {
			      // unit takes damage; if unit health falls below 0.0f, then
			      // unit will be removed by unit garbage collector
			      float damage_to_take = unit_values[UNIT_ARCHER].DEFAULT_DAMAGE;
			      if (unit->isplayerowned) { // take ai damage mod
				   damage_to_take *= unit_values[UNIT_ARCHER].AI_DAMAGE_MOD;
			      }
			      else {
				   damage_to_take *= unit_values[UNIT_ARCHER].USER_DAMAGE_MOD;
			      }

			      unit->health -= damage_to_take;
			      missile->to_be_removed = true;
			 }
			 else { // else missile type is wizard's spell

			      if (unit->isplayerowned) { // take ai damage mod
				   unit->health -= unit_values[UNIT_WIZARD].DEFAULT_DAMAGE *
					unit_values[UNIT_WIZARD].AI_DAMAGE_MOD;
			      }
			      else {
				   unit->health -= unit_values[UNIT_WIZARD].DEFAULT_DAMAGE *
					unit_values[UNIT_WIZARD].USER_DAMAGE_MOD;				   
			      }

			      // don't delete wizard spells
			 }

		    
		    }
	       }

	       // edge case: unit is fully inside a wizard spell
	       // TODO doesn't work
	       if (unit_location[i][x] > missile_location[0][x] &&
		   unit_location[i][x] < missile_location[2][x]) {

		    // meets within x boundaries
		    // now test y boundaries
		    if (unit_location[i][y] > missile_location[0][y] &&
			unit_location[i][y] < missile_location[3][y]) {

			 // meets within x and y boundaries
			 // damage unit and remove missile
			 if (missile->type == MISSILE_ARROW) {
			      // unit takes damage; if unit health falls below 0.0f, then
			      // unit will be removed by unit garbage collector
			      unit->health -= MISSILE_ARROW_DAMAGE;
			      missile->to_be_removed = true;
			 }
			 else { // else missile type is wizard's spell
			      unit->health -= MISSILE_SPELL_DAMAGE;
			      // don't delete wizard spells
			 }

		    
		    }
	       }


	  }
     }

     return;
}

/*
  remove all missiles from the list
*/
void missiles_remove_all(void)
{
     struct MISSILE *current = mh->head;
     struct MISSILE *prev = NULL;

     while (current) {
	  prev = current;
	  current = current->next;

	  free (prev);

	  prev = NULL;
	  
     }

     mh->head = NULL;

     return;
}
