/**
   Phocis : a real time strategy game, where YOU become a necromancer!
    Copyright (C) 2014 Tristan Kernan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <stdio.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "audio.h"
#include "settings.h"
#include "player.h"
#include "unit_values.h"

#define MAX_SAMPLES (32)

static ALLEGRO_SAMPLE *unit_sounds[2][3][3];

static ALLEGRO_SAMPLE *background_music[NUM_BACKGROUND_MUSIC];
static ALLEGRO_SAMPLE_ID background_music_id[NUM_BACKGROUND_MUSIC];
static const char *background_music_file_name[] =
{[NONE] = NULL,
 [BATTLE_BG] = "data/assets/sounds/background/battle_main.wav",
 [BATTLE_HALFWAY_GOOD_BG] = "data/assets/sounds/background/battle_good.wav",
 [BATTLE_HALFWAY_BAD_BG] = NULL,
 [BATTLE_VICTORY] = NULL,
 [BATTLE_DEFEAT] = NULL,
 [MAIN_MENU_BG] = "data/assets/sounds/background/main_menu.wav",
 [MAIN_MENU_WAITING_BG] = NULL,
 [CAMPAIGN_MENU_BG] = NULL,
 [LOADING_BG] = NULL
};


static int background_state = NONE;

void audio_load(void)
{
     /* If audio has been disabled globally */
     if (settings_override[AUDIO_DISABLE])
	  return;

     /* Otherwise initialize all audio subroutines */
     if (!al_install_audio()) {
	  fprintf(stderr, "failed to initialize audio!\n");
	  return;
     }

     if (!al_init_acodec_addon()) {
	  fprintf(stderr, "failed to initialize audio codec addon!\n");
	  return;
     }

     if (!al_reserve_samples(MAX_SAMPLES)) {
	  fprintf(stderr, "failed to reserve samples!\n");
	  return;
     }

     /* And load all audio files */

     /* load all background sound files */

     int i;
     for (i = 0; i < NUM_BACKGROUND_MUSIC; i++) {
	  if (background_music_file_name[i]) {
	       background_music[i] =
		    al_load_sample(background_music_file_name[i]);
	       if (!background_music[i]) {
		    fprintf(stderr, "failed to load %s\n", background_music_file_name[i]);
	       }
	  }
     }

     /* load all unit sound files */

     unit_sounds[PLAYER_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND] =
	  al_load_sample("./data/assets/sounds/archer_attack.wav");
     if (!unit_sounds[PLAYER_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND]) {
	  fprintf(stderr, "failed to load archer attack sound\n");
     }

     unit_sounds[AI_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND] =
	  unit_sounds[PLAYER_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND];

     unit_sounds[PLAYER_OWNED][UNIT_SOLDIER][UNIT_ATTACK_SOUND] =
	  al_load_sample("./data/assets/sounds/soldier_attack.wav");
     if (!unit_sounds[PLAYER_OWNED][UNIT_SOLDIER][UNIT_ATTACK_SOUND]) {
	  fprintf(stderr, "failed to load soldier attack sound\n");
     }

     unit_sounds[AI_OWNED][UNIT_SOLDIER][UNIT_ATTACK_SOUND] =
	  unit_sounds[PLAYER_OWNED][UNIT_SOLDIER][UNIT_ATTACK_SOUND];

     unit_sounds[PLAYER_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND] =
	  al_load_sample("./data/assets/sounds/archer_attack.wav");
     if (!unit_sounds[PLAYER_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND]) {
	  fprintf(stderr, "failed to load archer attack sound\n");
     }

     unit_sounds[AI_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND] =
	  unit_sounds[PLAYER_OWNED][UNIT_ARCHER][UNIT_ATTACK_SOUND];     

}

void play_unit_sound(struct UNIT *unit, int sound_type)
{
     if (settings_override[AUDIO_DISABLE])
	  return;
     
     if (!unit_sounds[unit->isplayerowned][unit->unit_type][sound_type])
	  return;
     

     if (!al_play_sample(unit_sounds[unit->isplayerowned][unit->unit_type][sound_type], 1.0, 0.0, 1.0,
			 ALLEGRO_PLAYMODE_ONCE, NULL)) {
		    
	  fprintf(stderr,
		  "error playing audio... may be due to all reserved \
samples instances currently being in use. try increasing reserved sample instances\n");
	       
     }

     return;
 
}

/*
  Stop the currently playing background music and start another
*/
void start_background_music(int type)
{
     
     if (settings_override[AUDIO_DISABLE])
	  return;

     if (background_state != NONE) {
	  al_stop_sample(&background_music_id[background_state]);
     }

     if (!background_music[type])
	  return;

     if (!al_play_sample(background_music[type], 1.0, 0.0, 1.0,
			 ALLEGRO_PLAYMODE_LOOP, &background_music_id[type])) {

	  fprintf(stderr, "error playing background audio\n");
     }

     /* set the new background state */
     background_state = type;

     return;
}

/*
  stop the currently playing background music
*/
void stop_background_music(void)
{
     if (background_state != NONE)
	  al_stop_sample(&background_music_id[background_state]);

     return;
}

/*
  return currently playing background id
*/
int audio_get_current_background_music_id(void)
{
     return background_state;
}
